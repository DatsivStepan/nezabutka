<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Product;
use app\models\Category;
use app\models\RegForm;
use app\models\Productcategory;
use app\models\Productimages;
use app\models\Favorite;
use app\models\Productviews;
use app\models\Profile;
use app\models\Message;
use app\models\Messagegroup;
use app\models\SendEmailForm;
use app\models\ResetPasswordForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\AccountActivation;
use yii\db\Query;

error_reporting( E_ERROR );

class ProductController extends Controller
{


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {

    }

    public function actionAddproduct()
    {
        if (!\Yii::$app->user->isGuest) {
            $newModelProduct = new Product();
            $newModelProduct->scenario = 'add_product';
            $modelCategories = Category::find()->where(['parent_id' => 0])->all();
            $categoryArray = ArrayHelper::map($modelCategories, 'id', 'name');
            if($_POST){
                if($_POST['Product']['category_id_l'] == 0){
                    $category_id = $_POST['Product']['category_id_f'];                    
                }else{
                    $category_id = $_POST['Product']['category_id_l'];
                }
                if($_POST['Product']['category_id_f'] == 0){
                    $category_id = $_POST['Product']['category_id'];                    
                }
                $categoryArrayId = [];
                if($category_id){
                    $modelCategory = Category::find()->where(['id' => $category_id])->one();
                    $categoryArrayId[] = $category_id;
                    if($modelCategory->parent_id != 0){
                        $categoryArrayId[] = $modelCategory->parent_id;
                        $modelParentCategory = Category::find()->where(['id' => $modelCategory->parent_id])->one();
                        if(($modelParentCategory->parent_id != 0)){
                            $categoryArrayId[] = $modelParentCategory->parent_id;
                        }                    
                    }
                }
                if($newModelProduct->load(Yii::$app->request->post())){
                    $newModelProduct->category_id = $category_id;
                    if($newModelProduct->save()){
                        Yii::$app->session->setFlash('add_product');
                        foreach($categoryArrayId as $c_id){
                            $newModelProductCategory = new Productcategory();
                            $newModelProductCategory->scenario = 'add_product_category';
                            $newModelProductCategory->category_id = $c_id;
                            $newModelProductCategory->product_id = $newModelProduct->id;
                            $newModelProductCategory->save();
                        }
                        $product_image = json_decode($_POST['Product']['image']);
                        
                        if($product_image){
                            foreach($product_image as $key => $image_src){
                                if($key == 0){
                                    $newModelProduct->scenario = 'update_image';
                                    $newModelProduct->img_src = $image_src;
                                    $newModelProduct->save();
                                }else{
                                    $newModelProductImage = new Productimages();
                                    $newModelProductImage->scenario = 'add_product_image';
                                    $newModelProductImage->img_src = $image_src;
                                    $newModelProductImage->product_id = $newModelProduct->id;
                                    $newModelProductImage->status = 0;
                                    $newModelProductImage->save();
                                }
                            }
                        }

                        return $this->redirect('/profile/'.\Yii::$app->user->id.'/sold');
                    }else{
                        Yii::$app->session->setFlash('not_add_category');
                    }
                }
            }
            $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->asArray()->one();
            return $this->render('addproduct', [
                'newModelProduct' => $newModelProduct,
                'modelCategories' => $categoryArray,
                'modelUser' => $modelUser,
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
       
    public function actionEditproduct($product_id)
    {
        if (!\Yii::$app->user->isGuest) {
            
            $modelProduct = Product::find()->where(['id' => $product_id])->one();
            $modelProduct->scenario = 'add_product';
            if(($modelProduct) && ($modelProduct->user_id == \Yii::$app->user->id)){
                
                $modelCategory = Category::find()->where(['id' => $modelProduct->category_id])->one();
                $arrayCategory['category'] = $modelCategory;                
                if($modelCategory->parent_id != 0){
                    $arrayCategory['category_array'] = ArrayHelper::map(Category::find()->where(['parent_id' => $modelCategory->parent_id])->all(), 'id', 'name');
                    
                    $modelParentCategory = Category::find()->where(['id' => $modelCategory->parent_id])->one();
                    if($modelParentCategory){
                        $arrayCategory['parent'] = $modelParentCategory;
                        if($modelParentCategory->parent_id != 0){
                            $arrayCategory['parent_array'] = ArrayHelper::map(Category::find()->where(['parent_id' => $modelParentCategory->parent_id])->all(), 'id', 'name');
                            
                            $modelParentParentCategory = Category::find()->where(['id' => $modelParentCategory->parent_id])->one();
                            if($modelParentParentCategory){
                                $arrayCategory['parent_parent'] = $modelParentParentCategory;
                                $arrayCategory['parent_parent_array'] = ArrayHelper::map(Category::find()->where(['parent_id' => 0])->all(), 'id', 'name');
                            }                            
                        }else{
                            $arrayCategory['parent_parent_array'] = ArrayHelper::map(Category::find()->where(['parent_id' => $modelCategory->id])->all(), 'id', 'name');
                            $arrayCategory['parent_array'] = ArrayHelper::map(Category::find()->where(['parent_id' => 0])->all(), 'id', 'name');
                        }
                    }
                }else{
                    $arrayCategory['parent_array'] = ArrayHelper::map(Category::find()->where(['parent_id' => $modelCategory->id])->all(), 'id', 'name');                    
                    $arrayCategory['category_array'] = ArrayHelper::map(Category::find()->where(['parent_id' => 0])->all(), 'id', 'name');                    
                }
                
                if($_POST){
                        if($_POST['Product']['category_id_l'] == 0){
                            $category_id = $_POST['Product']['category_id_f'];                    
                        }else{
                            $category_id = $_POST['Product']['category_id_l'];
                        }
                        if($_POST['Product']['category_id_f'] == 0){
                            $category_id = $_POST['Product']['category_id'];                    
                        }
                        $categoryArrayId = [];
                        if($category_id){
                            $modelCategory = Category::find()->where(['id' => $category_id])->one();
                            $categoryArrayId[] = $category_id;
                            if($modelCategory->parent_id != 0){
                                $categoryArrayId[] = $modelCategory->parent_id;
                                $modelParentCategory = Category::find()->where(['id' => $modelCategory->parent_id])->one();
                                if(($modelParentCategory->parent_id != 0)){
                                    $categoryArrayId[] = $modelParentCategory->parent_id;
                                }                    
                            }
                        }
                        if($modelProduct->load(Yii::$app->request->post())){
                            $modelProduct->category_id = $category_id;
                            if($modelProduct->save()){
                                Yii::$app->session->setFlash('product_update');
                                Productcategory::deleteAll(['product_id' => $modelProduct->id]);

                                foreach($categoryArrayId as $c_id){
                                    $newModelProductCategory = new Productcategory();
                                    $newModelProductCategory->scenario = 'add_product_category';
                                    $newModelProductCategory->category_id = $c_id;
                                    $newModelProductCategory->product_id = $modelProduct->id;
                                    $newModelProductCategory->save();
                                }

                                Productimages::deleteAll(['product_id' => $modelProduct->id]);
                                $product_image = json_decode($_POST['Product']['image']);
                                if($product_image){
                                    foreach($product_image as $key => $image_src){
                                        if($key == 0){
                                            $modelProduct->scenario = 'update_image';
                                            $modelProduct->img_src = $image_src;
                                            $modelProduct->save();
                                        }else{
                                            $newModelProductImage = new Productimages();
                                            $newModelProductImage->scenario = 'add_product_image';
                                            $newModelProductImage->img_src = $image_src;
                                            $newModelProductImage->product_id = $modelProduct->id;
                                            $newModelProductImage->status = 0;
                                            $newModelProductImage->save();
                                        }
                                    }
                                }else{
                                    $modelProduct->scenario = 'update_image';
                                    $modelProduct->img_src = '';
                                    $modelProduct->save();
                                }

                                return $this->redirect('/product/'.$modelProduct->id);
                            }else{
                                Yii::$app->session->setFlash('not_add_category');
                            }

                        }                    
                }
                
                $modelProductImage = Productimages::find()->where(['product_id' => $product_id])->asArray()->all();
                $stringProductImage = '';
                if($modelProduct->img_src != ''){
                    $stringProductImage = '"'.$modelProduct->img_src.'"';
                    foreach($modelProductImage as $image){
                        $stringProductImage = $stringProductImage.',"'.(string)$image['img_src'].'"';
                    }
                }
                $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->asArray()->one();
                $arrayMetro = [];
                if(($modelProduct->city_id != '') && ($modelProduct->city_id != null)){
                    $arrayMetro = ArrayHelper::map(\app\models\Metro::find()->where(['city_id' => $modelProduct->city_id])->all(), 'id', 'name');                    
                }
                return $this->render('editproduct', [
                    'stringProductImage' => '['.$stringProductImage.']',
                    'modelProductImage' => $modelProductImage,
                    'arrayCategory' => $arrayCategory,
                    'modelProduct' => $modelProduct,
                    'arrayMetro' => $arrayMetro,
                    'modelUser' => $modelUser,
                ]);
            }else{
                throw new \yii\web\NotFoundHttpException();
            }
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    
    public function actionGetproducts()
    {
        $category_id = $_POST['category_id'];
        $filter_price_to = $_POST['filter_price_to'];
        $filter_price_from = $_POST['filter_price_from'];
        $filter_sort = $_POST['filter_sort'];
        $city_id = $_POST['city_id'];
        $filter_date_to = $_POST['filter_date_to'];
        $search_key = $_POST['search_key'];
        $limit = $_POST['limit'];
        $offset = $_POST['offset'];
        $metro_id = $_POST['metro_id'];
        
        $metrosArray = json_decode($metro_id);
        
        
        if($category_id != ''){
            $query = new Query();
            $query->select(['*', 'id' => '{{product}}.id', 'date_create' => '{{product}}.date_create', 'img_src' => '{{product}}.img_src'])
                        ->from('{{product_category}}')
                        ->join('LEFT JOIN',
                                        '{{product}}',
                                        '{{product}}.id = {{product_category}}.product_id')
                        ->join('LEFT JOIN',
                                        '{{category}}',
                                        '{{category}}.id = {{product}}.category_id');
            $query->where(['{{product_category}}.category_id' => $category_id, '{{product}}.status' => '1']);
        }else{
            $query = new Query();
            $query->select(['*', 'id' => '{{product}}.id', 'date_create' => '{{product}}.date_create', 'img_src' => '{{product}}.img_src'])
                    ->from('{{product}}')
                        ->join('LEFT JOIN',
                                        '{{category}}',
                                        '{{category}}.id = {{product}}.category_id');
            $query->where(['{{product}}.status' => '1']);
        }
        
        if($search_key != ''){
            $query->andWhere(['LIKE','title',$search_key]);
        }
        
        if($filter_price_to != ''){
            $query->andWhere("{{product}}.price <= '".$filter_price_to."' ");
        }
        if($filter_price_from != ''){
            $query->andWhere("{{product}}.price >= '".$filter_price_from."' ");
        }
        if($city_id != ''){
            $query->andWhere(["{{product}}.city_id" => $city_id]);
        }
//        var_dump($metrosArray);
        if(!empty($metrosArray)){
            $query->andWhere(["{{product}}.metro_id" => $metrosArray]);
        }
        
        if(($filter_date_to != '') && ($filter_date_to != 'all')){
            $date_end = date('Y-m-d H:i:s', strtotime('-'.$filter_date_to.' days')); 
            $query->andWhere("{{product}}.date_create >  '".$date_end."' ");
        }
        
        if($filter_sort == 'date'){
            $query->orderBy('{{product}}.date_create ASC');
        }elseif($filter_sort == 'price'){
            $query->orderBy('{{product}}.price ASC');
        }
        $query->limit($limit);
        $query->offset($offset);
            
        $command = $query->createCommand();
        $modelProducts = $command->queryAll();
        
        echo json_encode($modelProducts);
    }
    
    public function actionProduct($id=null)
    {
        $modelProduct = Product::find()->where(['id' => $id])->one();
        $modelNewMessage = new Message();
        $modelNewMessage->scenario = 'add_message';
        if($modelProduct != null){
            if($_POST){
                if(isset($_POST['Frozen']['type'])){
                    if($_POST['Frozen']['type'] == 'defrost'){
                        $modelProduct->scenario = 'update_status';
                        $modelProduct->status = 1;
                        $modelProduct->save();
                    }elseif($_POST['Frozen']['type'] == 'frozen'){
                        $modelProduct->scenario = 'update_status';
                        $modelProduct->status = 4;
                        $modelProduct->save();                        
                    }
                }
                if(isset($_POST['Message'])){
                    if($modelNewMessage->load(\Yii::$app->request->post())){
                        $modelNewMessageGroup = new Messagegroup();
                        $modelNewMessageGroup->scenario = 'add_group_message';
                        $modelNewMessageGroup->sender_id = \Yii::$app->user->id;
                        $modelNewMessageGroup->recipient_id = $modelNewMessage->recipient_id;
                        $modelNewMessageGroup->title = $_POST['Message']['title'];
                        if($modelNewMessageGroup->save()){
                            $modelNewMessage->group_id = $modelNewMessageGroup->id;
                            $modelNewMessage->sender_id = $modelNewMessageGroup->sender_id;
                            $modelNewMessage->status = 0;
                            if($modelNewMessage->save()){
                                \Yii::$app->session->setFlash('message_send');
                            }else{
                                \Yii::$app->session->setFlash('message_not_send');                                
                            }
                        }
                    }
                }
            }
            
            if(($modelProduct->status == 3) || ($modelProduct->status == 4)){
                if($modelProduct->user_id != \Yii::$app->user->id){
                    throw new \yii\web\NotFoundHttpException();
                }
            }
//            if($modelProduct->user_id != \Yii::$app->user->id){
                $user_ip = Yii::$app->request->userIP;
                $modeProductViews = Productviews::find()->where(['product_id' => $modelProduct->id, 'user_ip' => $user_ip])->one();
                if(!$modeProductViews){
                    $modeNewProductViews = new Productviews();
                    $modeNewProductViews->scenario = 'add_views';
                    $modeNewProductViews->product_id = $modelProduct->id;
                    $modeNewProductViews->user_ip = $user_ip;
                    $modeNewProductViews->save();
                }
  //          }
            
            $modelCategory = Category::find()->where(['id' => $modelProduct->category_id])->one();
            $arrayCategory['category'] = $modelCategory;
            if($modelCategory->parent_id != 0){
                $modelParentCategory = Category::find()->where(['id' => $modelCategory->parent_id])->one();
                $arrayCategory['parent'] = $modelParentCategory;
                
                if($modelParentCategory->parent_id){
                    $modelParentParentCategory = Category::find()->where(['id' => $modelParentCategory->parent_id])->one();
                    $arrayCategory['parent_parent'] = $modelParentParentCategory;
                }
            }
                    
            $productViewsCount = Productviews::find()->where(['product_id' => $modelProduct->id])->count();
            $today = date('Y-m-d');
            $productViewsCountToday = Productviews::find()->where(['product_id' => $modelProduct->id])->andWhere('date_create LIKE "'.$today.'%"')->count();
            $productCount = $productViewsCount.'<span>('.$productViewsCountToday.' cегодня)<span>';
            
            $modelUser = User::find()->where(['id' => $modelProduct->user_id])->one();
            
            $modelUserProducts = Product::find()->where(['user_id' => $modelUser->id,'status' => '1'])->andWhere(['!=', 'id', $id])->asArray()->all();
            $productImage = Productimages::find()->where(['product_id' => $id])->asArray()->all();
            $favoriteStatus = Favorite::find()->where(['product_id' => $modelProduct->id, 'user_id' => \Yii::$app->user->id])->one();
            
            return $this->render('product', [
                'modelNewMessage' => $modelNewMessage,
                'arrayCategory' => $arrayCategory,
                'productCount' => $productCount,
                'modelUserProducts' => $modelUserProducts,
                'modelCategory' => $modelCategory,
                'modelProduct' => $modelProduct,
                'modelUser' => $modelUser,
                'productImage' => $productImage,
                'favoriteStatus' => $favoriteStatus,
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionSaveproductimg(){
        $ds          = DIRECTORY_SEPARATOR;
        $storeFolder = \Yii::getAlias('@webroot').'/img/product/';

        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
                mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            $tempFile = $_FILES['file']['tmp_name'];
            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;

            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.jpg';

            move_uploaded_file($tempFile,$targetFile);
            return \Yii::$app->user->id.'/'.$for_name.'_'.$randNumber.'.jpg';
        }
    }
    
    public function actionProductfavorite(){
        $product_id = $_POST['product_id'];
        $favoriteStatus = $_POST['status'];
        $result = [];
        if($favoriteStatus == 'add'){
            $newFavorite = new Favorite();
            $newFavorite->scenario = 'add_to_favorite';
            $newFavorite->product_id = $product_id;
            $newFavorite->user_id = \Yii::$app->user->id;
            if($newFavorite->save()){
                $result['status'] = 'success';
            }else{
                $result['status'] = 'error';
            }
        }else{
            $modelFavorite = Favorite::find()->where(['product_id' => $product_id, 'user_id' => \Yii::$app->user->id])->one();
            if($modelFavorite){
                if($modelFavorite->delete()){
                    $result['status'] = 'success';
                }else{
                    $result['status'] = 'error';
                }                
            }else{
                $result['status'] = 'error';
            }
        }
        echo json_encode($result);
    }
    
    public function actionDeleteproduct(){
        $product_id = $_POST['product_id'];
        $status = $_POST['status'];
        $result = [];
        $modelProduct = Product::find()->where(['id' => $product_id])->one();
        if($modelProduct){
            switch ($status) {
                case 'sold':
                        $modelProduct->scenario = 'update_status';
                        $modelProduct->status = '2';
                        if($modelProduct->save()){
                            $result['status'] = 'success';                    
                        }else{
                            $result['status'] = 'error';                    
                        }
                    break;                
                case 'soldout':
                        $modelProduct->scenario = 'update_status';
                        $modelProduct->status = '3';
                        if($modelProduct->save()){
                            $result['status'] = 'success';                    
                        }else{
                            $result['status'] = 'error';                    
                        }
                    break;                
                case 'frozen':
                        $modelProduct->scenario = 'update_status';
                        $modelProduct->status = '1';
                        if($modelProduct->save()){
                            $result['status'] = 'success';                    
                        }else{
                            $result['status'] = 'error';                    
                        }
                    break;                
                case 'archive':
                        if($modelProduct->delete()){
                            $result['status'] = 'success';                    
                        }else{
                            $result['status'] = 'error';                    
                        }
                    break;                
                default:
                    $result['status'] = 'error';                    
            }
        }else{
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }
    
}
