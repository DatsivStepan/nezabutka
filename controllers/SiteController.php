<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\User;
use app\models\Product;
use app\models\City;
use app\models\Category;
use app\models\RegForm;
use app\models\Profile;
use app\models\Metro;
use app\models\SendEmailForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\AccountActivation;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;

error_reporting( E_ERROR );

class SiteController extends Controller
{


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        //$
        //$queryProduct = Product::find()->where(['status' => '1']);
        //$modelProduct = new ActiveDataProvider(['query' => $queryProduct, 'pagination' => ['pageSize' => 10]]);
        return $this->render('index',[
//            'modelProduct' => $modelProduct->getModels(),
//            'pagination' => $modelProduct->pagination,
//            'count' => $modelProduct->pagination->totalCount,
        ]);
}

    public function actionLogin(){
        $model = new LoginForm();
        $result = [];
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
    
    
    public function actionCity(){
        $cityArray = City::find()->where('name LIKE "а%"')->asArray()->all();
        
        return $this->render('city',[
            'cityArray' => $cityArray
        ]);
    }
    
    public function actionGetcity()
    {
        $search_key = $_POST['search_key'];
        $modelCity = City::find()->where(['LIKE','name',$search_key])->asArray()->all();
        echo json_encode($modelCity);
    }
    
    public function actionGetmetro()
    {
        $city_id = $_POST['city_id'];
        $modelMetro = \app\models\Metro::find()->where(['city_id' => $city_id])->asArray()->all();
        echo json_encode($modelMetro);
    }
    
    public function actionGetcitylettername()
    {
        $letter = $_POST['letter'];
        $result = [];
        $modelCity = City::find()->where('name LIKE "'.$letter.'%"')->asArray()->all();
        if($modelCity){
            $result['status'] = 'success';
            $result['cityArray'] = $modelCity;
        }else{
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }
    
    public function actionGetcityname()
    {
        $city_id = $_POST['city_id'];
        $result = [];
        $modelCity = City::find()->where(['city_id' => $city_id])->asArray()->one();
        
        $arrayMetro = ArrayHelper::map(Metro::find()->where(['city_id' => $city_id])->all(), 'id', 'name');
        
        if($modelCity){
            $result['status'] = 'success';
            $result['city_name'] = $modelCity['name'];
            $result['metro'] = $arrayMetro;
        }else{
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }
    
    public function actionIfcityinbase()
    {
        $city_name = $_POST['city_name'];
        $result = [];
        $modelCity = City::find()->where(['like','name',$city_name])->asArray()->one();
        if($modelCity){
            $result['status'] = 'success';
            $result['city_model'] = $modelCity;
        }else{
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }
    
    
    public function actionContact()
    {
        $mailContent = '
            <table>
                <tr>
                    <td>Имя</td>
                    <td>'.$_POST['userName'].'</td>
                </tr>
                <tr>
                    <td>E-mail</td>
                    <td>'.$_POST['e-mail'].'</td>
                </tr>
                <tr>
                    <td>Сообщение</td>
                    <td>'.$_POST['massege'].'</td>
                </tr>
            </table>
        ';
        $request = Yii::$app->request;
        $adminUsers = User:: find()->where(['type' => 'admin'])->one();
        $adminEmail = $adminUsers->email;
        if(mail($adminEmail, 'Сообщение администратору', $mailContent)){
            echo json_encode('OK');
        }else {
            echo json_encode('ERROR');
        }
    }
    
    public function actionSignup(){
        $model = new User();
        
        $request = Yii::$app->request;
        $postedForm = $request->post('ContactForm');
        
        $model->scenario = 'registration';
        if ($model->load($request->post())){
            $model->password_hash = \Yii::$app->security->generatePasswordHash($model->password);
            $model->auth_key = 'key';
            if ($model->save()) {
                    if (Yii::$app->getUser()->login($model)) {
                            return $this->redirect('/addproduct');
                    }
            }
	}
        
        return $this->render('signup', [
            'newModel' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionActivateAccount($key)
    {
        try {
            $user = new AccountActivation($key);
        }
        catch(InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if($user->activateAccount()):
            Yii::$app->session->setFlash('success', 'Активация прошла успешно. <strong>'.Html::encode($user->username).'</strong> вы теперь с phpNT!!!');
        else:
            Yii::$app->session->setFlash('error', 'Ошибка активации.');
            Yii::error('Ошибка при активации.');
        endif;
        return $this->redirect(Url::to(['/main/login']));
    }

    public function actionReg()
    {
        $emailActivation = Yii::$app->params['emailActivation'];
        $model = $emailActivation ? new RegForm(['scenario' => 'emailActivation']) : new RegForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()):
            if ($user = $model->reg()):
                if ($user->status === User::STATUS_ACTIVE):
                    if (Yii::$app->getUser()->login($user)):
                        return $this->redirect('/addproduct');
                    endif;
                else:
                    if($model->sendActivationEmail($user)):
                        Yii::$app->session->setFlash('success', 'Письмо с активацией отправлено на емайл <strong>'.Html::encode($user->email).'</strong> (проверьте папку спам).');
                    else:
                        Yii::$app->session->setFlash('error', 'Ошибка. Письмо не отправлено.');
                        Yii::error('Ошибка отправки письма.');
                    endif;
                    return $this->refresh();
                endif;
            else:
                Yii::$app->session->setFlash('error', 'Возникла ошибка при регистрации.');
                Yii::error('Ошибка при регистрации');
                return $this->refresh();
            endif;
        endif;
        return $this->render(
            'reg',
            [
                'model' => $model
            ]
        );
    }

    public function actionGetchildcategory()
    {
        $category_id = $_POST['category_id'];
        $modelCategories = Category::find()->where(['parent_id' => $category_id])->orderBy('position ASC')->asArray()->all();
        $categoryArray = [];
        foreach($modelCategories as $category){
            $categoryArray[$category['id']]['info'] = $category;
            $categoryArray[$category['id']]['child'] = Category::find()->where(['parent_id' => $category['id']])->count();
        }
        echo json_encode($categoryArray);
    }
    
    public function actionAddnew()
    {
        return $this->render('addnew');
    }

    public function actionSendEmail()
    {
        $model = new SendEmailForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if($model->sendEmail()):
                    Yii::$app->getSession()->setFlash('warning', 'Проверьте емайл.');
                    return $this->goHome();
                else:
                    Yii::$app->getSession()->setFlash('error', 'Нельзя сбросить пароль.');
                endif;
            }
        }
        return $this->render('sendEmail', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
            exit;
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    public function actionRegistrationuser()
    {
        $result = [];
        if(isset($_POST['email'])){
            $login = User::generateLogin();
            $password = User::generatePassword();
            $email = User::verifyEmail($_POST['email']);
            $contactname = $_POST['contactname'];
            if($email){
                $model = new User();
                $model->username = $login;
                $model->password = $password;
                $model->email = $email;
                $model->contactname = $contactname;
                $model->password_hash = \Yii::$app->security->generatePasswordHash($password);
                $model->auth_key = 'key';
                $model->status = '10';
                if($model->save()) {
                                        
                    
                    \Yii::$app->mailer->compose()
                        ->setFrom('from@domain.com')->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                        ->setTo($email)
                        ->setSubject('Регистрация на "Незабудка"')
                        ->setHtmlBody('<div style="margin:10px;">'.
                                '<img src="http://nezabutka.roketdev.pro/img/logo_all.png">'.
                            '</div>'.
                            '<h2 style="margin:0px;font-weight:400;">Здравствуйте!</h2>'.
                            '<h4 style="font-weight:400;margin-top:0px;">Вы успешно зарегистрированы на сайте "Незабудка"<h4>'.
                            '<div style="font-weight:400;">'.
                                'Ваши данные:'.
                            '</div>'.
                            '<div style="margin:10px;">'.
                                '<div><b>Ваш Email</b> - <span style="font-weight:400;">'.$email.'</span></div>'.
                                '<div><b>Ваш Пароль</b> - <span style="font-weight:400;">'.$password.'</span></div>'.
                            '</div>'.
                            '<div>'.
                            'Пожалуйста сохраните єто сообщение и не роспостраняйте вышеизложенную информацию'.
                                '<a href="http://nezabutka.roketdev.pro/">Перейти на сайт</a>'.
                            '</div>')
                        ->send();
                    
                    
                    $result['status'] = 'success';
                }else{
                    $result['status'] = 'error';
                }
            }else{
                $result['status'] = 'error_email';
            }
        }else{
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }
    
    public function actionPage($urlname)
    {
        $modelPage = \app\models\Pages::find()->where(['slug' => $urlname])->one();
        
        return $this->render('page', [
            'modelPage' => $modelPage,
        ]);
    }
    
    public function actionHelpsend()
    {
        $email = $_POST['email'];
        $text = $_POST['text'];
            \Yii::$app->mailer->compose()
                ->setFrom('from@domain.com')->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                ->setTo(\Yii::$app->params['supportEmail'])
                ->setSubject('Помощь онлайн')
                ->setHtmlBody('<div style="margin:10px;">'.
                            '<img src="http://nezabutka.roketdev.pro/img/logo_all.png">'.
                        '</div>'.
                        '<h2 style="margin:0px;font-weight:400;">Помощь онлайн</h2>'.
                        '<div style="margin:10px;">'.
                            '<div><b>Email пользователя</b> - <span style="font-weight:400;">'.$email.'</span></div>'.
                            '<div><b>Вопрос от пользователя</b> - <span style="font-weight:400;">'.$text.'</span></div>'.
                        '</div>')
                ->send(); 
            
        echo json_encode(['status' => 'success']);
    }
    
    public function actionReklama()
    {
        $email = $_POST['email'];
        $telefon = $_POST['telefon'];
        $comment = $_POST['comment'];
            \Yii::$app->mailer->compose()
                ->setFrom('from@domain.com')->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                ->setTo(\Yii::$app->params['supportEmail'])
                ->setSubject('Заказать рекламу на сайте')
                ->setHtmlBody('<div style="margin:10px;">'.
                                '<img src="http://nezabutka.roketdev.pro/img/logo_all.png">'.
                            '</div>'.
                            '<h2 style="margin:0px;font-weight:400;">Заказать рекламу на сайте</h2>'.
                            '<div style="margin:10px;">'.
                                '<div><b>Email пользователя</b> - <span style="font-weight:400;">'.$email.'</span></div>'.
                                '<div><b>Телефон пользователя</b> - <span style="font-weight:400;">'.$telefon.'</span></div>'.
                                '<div><b>Комментарий пользователя</b> - <span style="font-weight:400;">'.$comment.'</span></div>'.
                            '</div>')
                ->send(); 
            
        echo json_encode(['status' => 'success']);
    }

}
