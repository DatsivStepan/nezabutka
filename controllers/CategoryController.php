<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Category;
use app\models\Product;
use app\models\RegForm;
use app\models\Attribute;
use app\models\Profile;
use app\models\Productcategory;
use app\models\SendEmailForm;
use app\models\ResetPasswordForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\AccountActivation;
use yii\data\ActiveDataProvider;
use yii\db\Query;

class CategoryController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($category_slug)
    {
        $modelParentCategory = Category::find()->where(['slug' => $category_slug])->one();
        $modelCategory = Category::find()->where(['parent_id' => $modelParentCategory->id])->orderBy('position ASC')->asArray()->all();
        $categoryArray = [];
        foreach($modelCategory as $category){
            $modelChildCategory = Category::find()->where(['parent_id' => $category['id']])->asArray()->all();
            $categoryArray[] = ['category_info' => $category, 'category_child' => $modelChildCategory];            
        }
        
        if($modelParentCategory != null){
            return $this->render('index',[
                'categoryArray' => $categoryArray,
                'modelParentCategory' => $modelParentCategory,
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionCategory($category_slug)
    {
        $modelCategory = Category::find()->where(['slug' => $category_slug])->one();

        $arrayCategory = [];
        $arrayCategory['category'] = $modelCategory;
        if($modelCategory){
            if($modelCategory->parent_id != 0){
                $modelParentCategory = Category::find()->where(['id' => $modelCategory->parent_id])->one();
                $arrayCategory['parent'] = $modelParentCategory;
                
                if($modelParentCategory->parent_id){
                    $modelParentParentCategory = Category::find()->where(['id' => $modelParentCategory->parent_id])->one();
                    $arrayCategory['parent_parent'] = $modelParentParentCategory;
                }
            }
            $modelChildCategory = Category::find()->where(['parent_id' => $modelCategory->id])->orderBy('position ASC')->all();
            
//            $query = new Query();
//            $query->select(['*', 'id' => '{{%product}}.id'])
//                        ->from('{{product_category}}')
//                        ->join('LEFT JOIN',
//                                        '{{%product}}',
//                                        '{{%product}}.id = {{%product_category}}.product_id'
//                                )->where(['{{%product_category}}.category_id' => $modelCategory->id]);
//                        
//            $modelProduct = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);
            return $this->render('category',[
                'arrayCategory' => $arrayCategory,
                'modelChildCategory' => $modelChildCategory,
                //'productCount' => $productCount,
//                'modelProduct' => $modelProduct->getModels(),
//                'pagination' => $modelProduct->pagination,
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionSubcategory($category_slug)
    {
        $modelCategory = Category::find()->where(['slug' => $category_slug])->one();
        $query = Product::find()->where(['category_id' => $modelCategory['id']]);
        $productCount = $query->count();
        $modelProduct = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);
        exit;
        if($modelCategory != null){
            return $this->render('subcategory',[
                'productCount' => $productCount,
                'modelProduct' => $modelProduct->getModels(),
                'pagination' => $modelProduct->pagination,
                'modelCategory' => $modelCategory,
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionGetchildcategory()
    {
        $category_id = $_POST['category_id'];
        $modelCategories = Category::find()->where(['parent_id' => $category_id])->orderBy('position ASC')->asArray()->all();
        if($modelCategories != null){
            $result['data'] = ArrayHelper::map($modelCategories,'id','name');
            $result['type'] = 'child_category';
            $result['status'] = 'success';
        }else{
            $modelAttribute = Attribute::find()->where(['category_id' => $category_id])->asArray()->all();
            if($modelAttribute != null){
                $result['data'] = $modelAttribute;
                $result['type'] = 'attribute';
                $result['status'] = 'success';
            }else{
                $result['status'] = 'error';
            }
        }
        
        echo json_encode($result);
    }
}
