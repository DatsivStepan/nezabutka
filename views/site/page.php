<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
$this->registerJsFile('/js/socket.io-1.3.5.js');
$this->registerJsFile('/js/userOnline.js');

$this->title = 'Незабудка';

?>
<div class="main-content">
    <h3 style="font: 30px 'Lato',sans-serif;"><b><?= $modelPage->name; ?></b></h3>
    <div style="text-align: left">
        <?= $modelPage->content; ?>
    </div>
</div>
