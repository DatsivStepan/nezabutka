<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\RegForm */
/* @var $form ActiveForm */
?>
<div class="header-bottom header-bottom-reg">
    <h1>Регистрация нового пользователя</h1>
    <p>Зарегистируйтеся и воспользуйтесь всеми преимуществами нашего сервиса</p>
</div>
<div class="main-reg" style="margin:0px;">
<div class="reg-option">

    <?php $form = ActiveForm::begin(); ?>
            <?php $this->registerJs("
                                     $('#refresh-captcha').on('click', function(e){e.preventDefault();
                                     $('#my-captcha-image').yiiCaptcha('refresh');})
                                    "); ?>
            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'password2')->passwordInput() ?>
            <?php echo $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                'imageOptions' => [
                    'id' => 'my-captcha-image'
                ]
            ]); ?>
                          <?php echo Html::button('', ['class' => 'reg-option-button', 'id' => 'refresh-captcha']);?>
            <div class="form-group">
                <?= Html::submitButton('Зарегистрироваться', ['class' => 'reg-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>





    <?php
    if($model->scenario === 'emailActivation'):
        ?>
        <i>*На указанный емайл будет отправлено письмо для активации аккаунта.</i>
        <?php
    endif;
    ?>
</div><!-- reg-option -->
    <div class="fteep">
        <h1>Шаг 1</h1>
        <p>Зарегистируйтеся и перейдите в ваш личный кабинет</p>
        <h1>Шаг 2</h1>
        <p>Разместите ваше обявление</p>
    </div>
</div><!-- main-reg -->