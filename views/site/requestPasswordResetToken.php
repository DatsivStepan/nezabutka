<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="border_reset">
<div class="site-request-password-reset text_reset">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Введите свой зарегистрированный адрес эл.почты, на который будут высланы инструкции</p>

    <div class="row">
        <div class="col-lg-5 text_reset_mail">
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                    <?= $form->field($model, 'email')->textInput(['class' => 'fornm_email  form-control'])?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => ' button_reset']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
    </div>
