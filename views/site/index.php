<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\assets\ProductsAsset;
ProductsAsset::register( $this );

$this->registerJsFile('/js/socket.io-1.3.5.js');
$this->registerJsFile('/js/userOnline.js');

$this->title = 'Незабудка';

?>
<div class="main-content main_content_pos">
        <div>
            <a class="show_product_filter hidden-sm hidden-md hidden-lg ">Развернуть фильтры поиска</a>
            <a class="hide_product_filter hidden-sm hidden-md hidden-lg " style="display:none;">Свернуть фильтры поиска</a>
            <div class="row no_margin FilterBlock ">
                <input type="hidden" name="filter_category_id" value="">
                <div class="app_filter-price">
                    <div class="filter-title">Цена:</div>
                    <div class="form_group col-md-5 no_padding">
                        <input type="text" name="filter_price_from" placeholder="От">
                    </div>
                    <div class="form_group col-md-5 no_padding">
                        <input type="text" name="filter_price_to" placeholder="До">
                    </div>
                    <div class="form_group col-md-2 no_padding">
                        <span>руб.</span>
                    </div>
                </div>
                <div class="app_filter-time">
                    <div class="filter-title">Срок размещения:</div>
                    <div class="form_group col-md-3 no_padding">
                        <input type="radio" id="f_24" name="filter_date_to" value="1">
                        <label for="f_24" class="radio-inline"> за 24 часа</label>
                    </div>
                    <div class="form_group col-md-3 no_padding">
                        <input type="radio" id="f_7" name="filter_date_to" value="7">
                        <label for="f_7" class="radio-inline"> за 7 дней</label>
                    </div>
                    <div class="form_group col-md-3 no_padding">
                        <input type="radio" id="f_30" name="filter_date_to" value="30">
                        <label for="f_30" class="radio-inline"> за 30 дней</label>
                    </div>
                    <div class="form_group col-md-3 no_padding">
                        <input type="radio" id="f_all" name="filter_date_to" value="all" checked="checked">
                        <label for="f_all" class="radio-inline"> за все время</label>
                    </div>
                </div>
                <div class="app_filter-sort">
                    <div class="filter-title">Сортировка:</div>
                    <div class="form_group col-md-7 no_padding">
                        <input type="radio" id="f_date" name="filter_sort" value="date">
                        <label for="f_date" class="radio-inline"> по дате публикации</label>
                    </div>
                    <div class="form_group col-md-5 no_padding">
                        <input type="radio" id="f_price" name="filter_sort" value="price" checked="checked">
                        <label for="f_price" class="radio-inline"> по стоимости</label>
                    </div>
                </div>
            </div>
        </div>
    
    <div style="padding-top:18px;text-align:right;padding-right:8px;">
            <span class="filter-title">Показать объявления</span>
            <div class="gridStyle" style="display:inline-block;">
                <img src="/img/show_grid.png" style="width:16px;" />
            </div>
            <div class="listStyle" style="display:inline-block;">
                <img src="/img/show_list.png"  style="width:16px;" />                
            </div>
    </div>
    
    <div class="main-catalog col-sm-12 productsBlock" style="padding-left: 0px;padding-right:0px;">
    </div>
    <div style="text-align: center">
        <a class="show_more_product_btn">Больше товаров</a>        
    </div>
</div>
