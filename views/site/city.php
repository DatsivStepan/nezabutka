<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;

$this->title = 'Незабудка';

?>
<div class="col-sm-12 up_menu_personsl" style="margin-top:20px; margin-bottom: 50px;right: 27px;">
    <a href="<?= Url::home(); ?>"">Главная</a> <img src="/web/img/arrow-right.png">
    <a >Города</a>
</div>
<div class="title_city_mob">
    <a>Города</a>
</div>
<div class="title_city">
    <a>Города распределены по первым буквам для удобного поиска</a>
</div>
<div class="main-content">
        
    <div class="row letterBlock">
        <div class="letter active">А</div>
        <div class="letter">Б</div>
        <div class="letter">В</div>
        <div class="letter">Г</div>
        <div class="letter">Д</div>
        <div class="letter">Е</div>
        <div class="letter">Ж</div>
        <div class="letter">З</div>
        <div class="letter">И</div>
        <div class="letter">Й</div>
        <div class="letter">К</div>
        <div class="letter">Л</div>
        <div class="letter">М</div>
        <div class="letter">Н</div>
        <div class="letter">О</div>
        <div class="letter">П</div>
        <div class="letter">Р</div>
        <div class="letter">С</div>
        <div class="letter">Т</div>
        <div class="letter">У</div>
        <div class="letter">ф</div>
        <div class="letter">Х</div>
        <div class="letter">Ц</div>
        <div class="letter">Ч</div>
        <div class="letter">Ш</div>
        <div class="letter">Щ</div>
        <div class="letter">Э</div>
        <div class="letter">Ю</div>
        <div class="letter">Я</div>
    </div>
    <div class="row " style="margin-top:20px;">
        <h3 class="letterChoisen">A</h3>
        <div class="column col-xs-12 ">
            <?php foreach($cityArray as $city){ ?>
                <div class="city " data-city_id="<?= $city['city_id']; ?>"><?= $city['name']; ?></div><br>
            <?php } ?>
        </div>
    </div>
</div>
