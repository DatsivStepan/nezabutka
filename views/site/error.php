<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
?>
<div class=" row border_error">
<div class="site-error col-xs-12">
    <div class=" col-xs-12  col-md-6 number_error" >
        404
    </div>
    <div class="col-xs-12 col-md-6 text_site_error">
        <h1>Страница не найдена</h1>
        <h4>Причины, которые могли привести к ошибке:</h4>
        <ul>
            <li>1. Адрес был набран неправильно.Проверьте правильность адреса </li>
            <li>2. Возможно документ перемещен и находится по другому адресу.</li>
            <li>3. Возможно вы использовали неправильные символы в адресе.</li>
            </ul>
        <div class="button_error_cl">
        <a href="<?= Url::home(); ?>" class="btn_per button_error">Вернуться на главную</a></div>
    </div>
    </div>
</div>
