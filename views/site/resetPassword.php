<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = 'Восстановление доступа';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="border_reset">
<div class="site-reset-password text_reset">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Введите новый пароль в поле ниже</p>

    <div class="row">
        <div class="col-lg-5 text_reset_mail">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
                <?= $form->field($model, 'password')->passwordInput()->textInput(['class' => 'fornm_email  form-control']) ?>
                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'button_reset']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    </div>
</div>
