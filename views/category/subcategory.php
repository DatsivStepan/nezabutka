<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>

	<div class="search-result clearfix">
		<div class="container">
			<span class="search-result_title">Найдено <?= $productCount; ?> обьявления</span>
			<?php foreach($modelProduct as $product){ ?>
                            <div class="result-item">
                                    <div class="item-img">
                                        <?php if($product->img_src != ''){ ?>
                                                <img src="<?= Url::home().'img/product/'.$product->img_src; ?>" alt="">
                                        <?php }else{ ?>
                                                <img src="<?= Url::home(); ?>img/DefaultFotoProduct.png" alt="">
                                        <?php } ?>
                                    </div>
                                    <div class="item-head">
                                            <?= HTML::a($product->title,Url::home().'product/'.$product->id); ?>
                                            <span class="date-item"><?= $product->date_create; ?></span>
                                    </div>
                                    <div class="item-main">
                                            <p><?= $product->about; ?></p>
                                    </div>
                                    <div class="item-foot">
                                            <!--<div class="release-year">Год выпуска:<span>2013</span></div>
                                            <div class="mileage">Состояние:<span>После кап. ремонта</span></div>-->
                                            <div class="price-item"><?= $product->price; ?>руб</div>
                                    </div>
                            </div>
                        <?php } ?>
			
			<div class="pagination">
                            <?= LinkPager::widget(['pagination'=>$pagination]); ?>
			</div>
		</div>
        </div>