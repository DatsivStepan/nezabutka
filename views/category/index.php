<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="select-cat">
        <div class="container">
                <div class="sel-cat-list clearfix">
                        <div class="cat-avto_title mobile">
                                <h1>Категория <?= $modelParentCategory->name; ?></h1>
                        </div>
                        <div class="select-cat_title">
                                <span>Выбор по категориям</span>
                        </div>
                        <ul>
                                    <?php foreach($categoryArray as $category){ ?>
                                        <li>
                                        <?= HTML::a($category['category_info']['name'], '#'); ?>
                                        </li>
                                    <?php } ?>
                        </ul>
                </div>

                <?php foreach($categoryArray as $category){ ?>
                    <div class="select-cat_item">
                        <div class="cat-item_img">
                            <?php if($category['category_info']['img_src'] == ''){ ?>
                                <img src="<?= Url::home().'img/category/default_img.png' ?>" alt="" style="max-width: 50px;margin-bottom: 5px;max-height: 50px;">
                            <?php }else{ ?>
                                <img src="<?= Url::home().'img/category/'.$category['category_info']['name']; ?>" alt="" style="max-width: 50px;margin-bottom: 5px;max-height: 50px;">
                            <?php } ?>
                                <?= HTML::a($category['category_info']['name'], Url::home().'category/'.$category['category_info']['slug']); ?>
                        </div>
                        <div class="cat-item_list">
                                <ul>
                                    <?php foreach($category['category_child'] as $child_category){ ?>
                                        <li>
                                            <?= HTML::a($child_category['name'], Url::home().'subcategory/'.$child_category['slug']); ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                <?php } ?>

        </div>
</div>