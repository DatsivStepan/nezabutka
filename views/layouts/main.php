<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\Alert;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\models\Category;
use app\models\Pages;
use app\models\City;
use app\models\User;
use app\assets\AppAsset;
use app\modules\administration\models\Settings;
use app\widgets\CategoryrightmenuWidget;
use app\widgets\ProductcountWidget;
use app\widgets\HelpWidget;


AppAsset::register($this);
//$modelCity = City::find()->asArray()->limit(20)->all();
$modelCategory = Category::find()->where(['parent_id' => 0])->all();
$modelSettings = Settings::find()->asArray()->all();

    $settingsArr = [];
    foreach($modelSettings as  $value){
        $settingsArr += [$value['key'] => $value['value']];
    }

$modelRandomCategory = Category::find()->where([' != ','parent_id', 0])->orderBy(new yii\db\Expression('rand()'))->one();
$modelPages = Pages::find()->asArray()->all();


?>
<?php $this->beginPage() ?>


<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=460, initial-scale=0.6">
    <link rel="shortcut icon" href="<?= Url::home(); ?>img/favicon/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?= Url::home(); ?>img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= Url::home(); ?>img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= Url::home(); ?>img/favicon/apple-touch-icon-114x114.png">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>
    
    
    <div style='display:none;'>
        <?php 
            NavBar::begin([
            ]); 
            NavBar::end([
            ]); 
        ?>
    </div>

<div class="header">
    <input type="hidden" id="user_id" value="<?=(!\Yii::$app->user->isGuest)?\Yii::$app->user->id:'';?>">

    <div class="account_mobile_header style-mob-header">
        <div class="style-padding-cab">
            <a class="pull-right account_mob-link style-text-none">Личный кабинет</a>
            <div class="clear-both"></div>
        </div>
        <div class="form-account_top style-top-acc">
            <?php if (!\Yii::$app->user->isGuest) {?>
                <div class="container style-container">
                    <div class="logout_mob_input_container">
                        <div class="form-account_top">
                                <?= HTML::a('Личный кабинет', Url::home().'profile/'.\Yii::$app->user->id, ['class' => 'signin signin_mob']); ?>
                        </div>
                        <div class="form-account_top">
                                <?= Html::beginForm(['/site/logout'], 'post',['class'=>'exit']) ?>
                                    <?= Html::submitButton('Выход',['class' => 'exit_box_mob fl logout_mob','style' => 'border:1px solid transparent']); ?>
                                <?= Html::endForm(); ?>
                        </div>
                    </div>
                </div>
            <?php }else{ ?>
                <div class="container style-container">
                    <div class="login_mob_input_container">
                        <span class="style-span-h">Вход в личный кабинет</span>
                        <input type="text" name="username" placeholder="Ваш Email">
                        <input class="style-input" type="password" name="password" placeholder="Ваш пароль">
                        <a id="btn-in_mob" class="btn-login">Войти</a>
                        <a href="<?= Url::home(); ?>site/request-password-reset" class="remember-pass">Напомнить пароль</a>
                        <a class="signin btn-signin_mob" id="btn-signin_mob">Регистрация</a>
                    </div>
                    <div class="loader-acc"></div>
                    <div class="hidd-acc">
                        <i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="header-top_line style-top-header-line">
        <div class="container">
<!--            <div style="float:left;color:white;line-height:35px;">
            </div>-->
            <ul class="header-top_menu style-top-mune-header">
                <li class="pull-left">
                    <span class="style-span-oholosh">Объявлений  на Незабудке:
                    <?= ProductcountWidget::widget(); ?></span>
                </li>
                <li class="pull-right style-pull-right">
                    <?php if (!\Yii::$app->user->isGuest) {?>
                    <a href="#" class="account-link">
                        <?php  if((\Yii::$app->user->identity->contactname != '') && (\Yii::$app->user->identity->contactname != null)){ ?>
                            <?= \Yii::$app->user->identity->contactname; ?>
                        <?php  }else{ ?>
                            <?= \Yii::$app->user->identity->email; ?>
                        <?php  } ?>
                        
                    </a>
                        <div class="form-account form-account_exit style-exit-form">                    
                            <div class="form-account_top style-acc-top-form" style="">
                                <span>
                                    <?= HTML::a('Личный кабинет', Url::home().'profile/'.\Yii::$app->user->id, ['class' => 'signin','style' => 'margin-top:2px;line-height:45px;']); ?>
                                </span>
                            </div>
                            <div class="form-account_top">
                                <span>
                                <?= Html::beginForm(['/site/logout'], 'post',['class'=>'exit', 'style' => 'margin-top:2px;']) ?>
                                    <?= Html::submitButton('Выход',['class' => 'exit_box_mob fl','style' => 'outline: none;margin: 8px 0 0 0;border: 1px solid transparent;background-color: transparent;']); ?>
                                <?= Html::endForm(); ?>
                                </span>
                            </div>
                        </div>

                    <?php }else{ ?>
                        <a href="#" class="account-link">Личный кабинет</a>
                        <div class="form-account style-acc-form">
                            <div class="form-account_top">
                                <span class="pull-right fa fa-times-circle loginFormClose" style="color:grey;font-size: 22px;cursor:pointer;"></span>
                                <span class="style-span-vhod" style="">Вход в личный кабинет</span>
                                <input type="text" name="username" placeholder="Ваш Email">
                                <input class="style-input-pass" type="password" name="password" placeholder="Ваш пароль">
                                <span id="btn-signin" >Войти</span>
                                <a href="<?= Url::home(); ?>site/request-password-reset" class="remember-pass style-pass-remeber">Напомнить пароль</a>
                            </div>
                            <a class="btn-signup">Регистрация</a>
                            <div class="loader-acc"></div>
                            <div class="hidd-acc">
                                <i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i>
                            </div>
                        </div>
                    <?php } ?>
                </li>
                <li class="pull-right style-pull-right">
                    <a href="#" class="area-link linkCityHeader">Город</a>
                    <div class="form-area">
                        <span class="pull-right fa fa-times-circle cityChoiseClose"></span>
                        <div class="yourCityBlock" style="display:none;">
                            <p style="padding-top:5px;">
                                <span  style="font-size:14px;display:inline-block;font-family: 'Lato';"> Ваш город - </span>
                                <span  style="display:inline-block;font-size:18px;font-weight: bold;font-family: 'Lato';" class="yourCity"></span>
                            </p>
                            <a class="yourCityConfirm" data-city_id="" >Да</a>
                            <a class="choiseOtherCity" >Выбрать другой город</a>
                        </div>
                        <div class="otherCityChoisen" style="margin-top:5px;">
                            <span class="style-city-choose">Выбрать город</span>
                            <div style="position:relative;">
                                <input type="hidden" name="cityChoiseId" value="0">
                                <input type="text" class="choiseCity" style="padding-right:30px;" placeholder="Введите название своего города">
                                <i class="glyphicon glyphicon-remove deleteChoiseCity" ></i>
                            </div>
                            <div class="style-city-view">
                                <div class="style-city-pre">
                                    <ul class="cityPreview">
                                    </ul>
                                </div>
                            </div>
                            <div class="metroBlock" style="display:none;">
                                <span class="style-city-choose">Выбрать станцию метро:</span>
                                <select name="choiseMetro" id="choiseMetro" style="width:100%;padding:10px;margin-top:10px;"  multiple="multiple">
                                    <option>Выберите станцию метро</option>
                                </select>
                            </div>
                            <a class="btn-save saveCity"  data-button-type="comp" style="font-weight: bold;cursor:pointer;">Готово</a>
                        </div>
                    </div>
                </li>
                <li class="pull-right style-pull-right"><span class="header-phone style-header-phone">Горячая линия:<?php echo ($settingsArr['phone']);?></span></li>
                <li class="pull-right style-pull-right"><a href="#" class="style-a-href">Магазины</a></li>
            </ul>
        </div>

        <audio id="newMSG" style="display: none;">
          <source src="/audio/new-msg.mp3" type="audio/mpeg">
        </audio>
    </div>


    <!-- registration modal begin-->
    <div class="row reklamaBlock style-row-rek-block">
        <div class="reklamashow style-row-rek-show">
            <div class="style-mar-top-bottom">
                <form id="reklamaForm" class="registrationq">
                    <h1>Заказать рекламу на сайте</h1>
                    <input type="email" required="required" name="reklamaEmail"  class="emailform_registration form_reklam reklam_text" placeholder="Введите ваш e-mail:">
                    <input type="text" required="required" name="reklamaTelefon"  class="emailform_registration form_reklam reklam_text " placeholder="Введите номер телефона:">
                    <textarea required="required" name="reklamaComment"  class="emailform_registration form_reklam_padding reklam_text"  rows="10" placeholder="Комментарии:"></textarea>
                    <span class="text_registration">Мы свяжемся с вами в ближайшие время</span>
                    <button class=send_reklama>Отправить</button>
                    <div class="img_redit"><img  src="/web/img/regissration-.png"></div>
                </form>
                <div class="loader-rek"></div>
                <div class="hidd-rek">
                    <i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i>
                </div>
            </div>
        </div>
    </div>
    <!-- registration modal end -->


    <!-- registration modal begin-->
    <div class="row signinBlock style-row-sigin-block">
        <div class="registrationshow style-row-sigin-show">
            <div class="style-mar-top-bottom">
                <form id="registrationForm" class="registration">
                    <h1>Регистрация</h1>
                    <input type="text" required="required" min="3" name="registrationContactname"  class="emailform_registration" placeholder="Введите ваше Имя:">
                    <input type="email" required="required" name="registrationEmail"  class="emailform_registration" placeholder="Введите ваш e-mail:">
                    <span class="text_registration" style="display:block;">Мы пришлем вам пароль</span>
                    <button class="btn_registration button_registration">Получить пароль</button>
                    <div class="img_redit"><img  src="/img/regissration-.png"></div>
                </form>
                <div class="loader-email"></div>
                <div class="hidd-loader">
                    <i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i>
                </div>
            </div>
        </div>
    </div>
    <!-- registration modal end -->
    <div class="header-body style-header-body">
        <div class="nav-mobile clearfix">
            <div class="container style-container-pos">
                <span  id="navmobile-btn" class="menu_img hidei style-span-nav-mob"><img  class="img_mob_menue" src="<?= Url::home(); ?>img/mobile-menu-btn.png" alt=""></span>

                <div class="style-empty-block">
                    <div class="productCountBlockmodal style-product-coun-block">Обьявлений  на незабудке:
                    <?= ProductcountWidget::widget(); ?></div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="header-body_top clearfix">
                <div class="logo">
                    <a href="<?= Url::home(); ?>" class="logo-img"><img src="<?= Url::home(); ?>img/logo.png" alt="">НЕЗАБУДКА</a>
                </div>

                <div class="mobile_menublok">
                   <ul>
                       <li> Горячая линия: +7 (495) 234-54-67</li>
                       <li> e-mail: infonezabudka@mail.ru</li></ul>
                    <ul class="mobile-menu">
                        <?php if($modelPages != null){ ?>
                             <?php foreach($modelPages as $page){ ?>
                                <li><a href="<?= Url::home(); ?>page/<?= $page['slug']; ?>"><?= $page['name']; ?></a></li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                    <div class="img_menu_mob"><img src="/img/regissration-.png"></div>
                </div>

                <div class="area-change">
                    <a href="#" class="area-link_mob"><span class="linkCityHeader">Город</span></a>
                    <div class="form-area_mob">
                        <div class="yourCityBlock" style="display:none;">
                            <p style="padding-top:5px;">
                                <span  style="font-size:18px;display:inline-block;font-family: 'Lato';"> Ваш город - </span>
                                <span  style="display:inline-block;font-size:22px;font-weight: bold;font-family: 'Lato';" class="yourCity"></span>
                            </p>
                            <a class="yourCityConfirm" style="font-size:18px;" data-city_id="" >Да</a>
                            <a class="choiseOtherCity" style="font-size:18px;" >Выбрать другой город</a>
                        </div>
                        <div class="otherCityChoisen">
                            <span>Выбрать город</span>
                            <div style="position:relative;">
                                <input type="text" class="choiseCity" placeholder="Введите название своего города">
                                <i class="glyphicon glyphicon-remove deleteChoiseCity" style="position:absolute;right:0px;cursor:pointer;"></i>
                            </div>
                            <div class="style-city-view">
                                <div class="style-city-pre">
                                    <ul class="cityPreview">
                                    </ul>
                                </div>
                            </div>
                            <div class="metroBlock" style="display:none;">
                                <span class="style-city-choose">Выбрать станцию метро:</span>
                                <select name="choiseMetroMobile" id="choiseMetroMobile" style="width:100%;padding:10px;margin-top:10px;"  multiple="multiple">
                                    <option>Выберите станцию метро</option>
                                </select>
                            </div>
                            <a class=" saveCity btn-save" data-button_type="mobile">Готово</a>
                        </div>
                    </div>
                </div>
                <div class="header-search">
                    <div class="style-display-in-block">
                        <input class="headerSearchText" type="text" placeholder="Например: <?= $modelRandomCategory->name; ?>">
                        <i style="" class="deleteSearchText glyphicon glyphicon-remove"></i>
                    </div>
                    <a class="btn-select">
                        <input type="hidden" class="btn-select-input headerCategoryChoise"/>
                        <span class="btn-select-value">Все категории</span>
                        <ul class="headerCategoryUl">
                            <li>Все категории</li>
                            <?php foreach($modelCategory as $category){ ?>
                                <li data-category_slug="<?= $category['slug']; ?>">
                                    <?= $category['name']; ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </a>
                    <a href="#" class="btn-search headerSearchButton"><img src="<?= Url::home(); ?>img/search.png" alt=""></a>
                </div>

                <div class="header-search_mob style-head-search-mob">
                    <input type="text" placeholder="Поиск" class="headerSearchText">
                    <a class="btn-search_mob headerSearchButton"><img src="../img/search-big.png" alt=""></a>
                    <i class="deleteSearchText glyphicon glyphicon-remove"></i>
                </div>
                
                <?php if (!\Yii::$app->user->isGuest) {?>
                    <?= HTML::a('Добавить объявление',Url::home().'addproduct',['class' => 'add-post']); ?>
                <?php }else{ ?>
                    <a class="please_register add-post">Добавить объявление</a>
                <?php } ?>
                <?php if (!\Yii::$app->user->isGuest) {?>
                <div class="unreadMsgCount"><div class="countM">0</div></div>
                <?php } ?>

            </div>

            <div class="col-xs-12 hidden-sm hidden-md hidden-lg style-hidd-style">
                <div class="category-change hideCateg">
                    <a>Выберите категорию</a>
                </div>
                <div class="category_mobile_list style-cat-mob-list">
                    <div class="list-group mobileCategoryMenu" >
                        <?php foreach($modelCategory as $category){ ?>
                            <li class="list-group-item main_categoru style-li-group">
                                <a  class="hideE main_text" data-category_slug="<?= $category['slug']; ?>" data-category_id="<?= $category['id']; ?>"><?= $category['name']; ?></a>

                                <ul class="list-group submenuList style-ul-group"></ul>
                            </li>
                        <?php } ?>
                    </div>
                </div>
            </div>



            <form action="#">
            </form>
        </div>
    </div>
</div>
<div class="container">
    
    <div class="row">
        <div class="app_sidebar top25 hidden-xs">
            <?= CategoryrightmenuWidget::widget(); ?>
            <?= HelpWidget::widget(); ?>

        </div>
        <div class="app_main top25 padding_main">
            <?php
                if(Yii::$app->session->hasFlash('success')):
                        echo Alert::widget([
                                'options' => [
                                        'class' => 'alert-info',
                                ],
                                'body' => 'Ссылка для восстановления пароля отправлена вам на почту!',
                        ]);
                endif;

                if(Yii::$app->session->hasFlash('error')):
                        echo Alert::widget([
                                'options' => [
                                        'class' => 'alert-info',
                                ],
                                'body' => 'При отправке на почту возникла ошибка!',
                        ]);
                endif;
            ?>

            <?= $content ?>
        </div>
    </div>
</div>
<footer class="footer style-footer">
            <div class="footer-cont row style-footer-cont">
                    <div class="footer-menu col-sm-4 col-md-2 col-xs-12 col-lg-2">
                        <div class="footer-menu_center">
                            <span class="footer-menu_title">Общее</span>
                            <ul>
                                <?php if($modelPages != null){ ?>
                                    <?php foreach($modelPages as $page){ ?>
                                        <li><a href="<?= Url::home(); ?>page/<?= $page['slug']; ?>"><?= $page['name']; ?></a></li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="clear-both"></div>
                    </div>
                    <div class="footer-reklama-mob col-xs-12">
                        <div class="footer-reklama-center-mob">
                            <div class="footer-reklama-img-mob">
                                <img src="/web/img/image-reklama.png"/>
                            </div>
                            <div class="footer-reklama-text-mob">
                                <a class="btn-reklama-mob showReklama">Реклама на сайте</a>
                            </div>
                        </div>
                        <div class="clear-both"></div>
                    </div>
                    <div class="footer-city padding_fotter col-sm-8 col-xs-12  col-md-5 col-lg-5">
                        <div class="footer-city-center  ">
                            <span class="footer-city-title">Города</span>
                            <div class="footer-city-ul-name">
                                <ul class="footerCity">

                                    <li ><a class="cityF" data-city_id="4400">Москва</a></li>
                                    <li ><a class="cityF" data-city_id="4962">Санкт-Петербург</a></li>
                                    <li ><a class="cityF" data-city_id="4319">Балашиха</a></li>
                                    <li ><a class="cityF" data-city_id="4741">Владивосток</a></li>
                                    <li ><a class="cityF" data-city_id="3472">Волгоград</a></li>
                                    <li ><a class="cityF" data-city_id="3538">Воронеж</a></li>
                                    <li ><a class="cityF" data-city_id="5106">Екатеринбург</a></li>
                                    <li ><a class="cityF" data-city_id="3731">Иркутск</a></li>
                                    <li ><a class="cityF" data-city_id="5269">Казань</a></li>
                                    <li ><a class="cityF" data-city_id="3854">Калуга</a></li>
                                    <li ><a class="cityF" data-city_id="4079">Краснодар</a></li>
                                    <li ><a class="cityF" data-city_id="4149">Красноярск</a></li>
                                    <li ><a class="cityF" data-city_id="4402">Мытищи</a></li>
                                    <li ><a class="cityF" data-city_id="3612">Нижний Новгород</a></li>
                                    <li ><a class="cityF" data-city_id="4549">Новосибирск</a></li>
                                    <li ><a class="cityF" data-city_id="4580">Омск</a></li>
                                    <li ><a class="cityF" data-city_id="4720">Пермь</a></li>
                                    <li ><a class="cityF" data-city_id="4421">Подольск</a></li>
                                    <li ><a class="cityF" data-city_id="5217">Пятигорск</a></li>
                                    <li ><a class="cityF" data-city_id="4848">Ростов-на-Дону</a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="footer-all-city">
                            <div class="footer-all-city-center">
                                <a href="/site/city"class="btn-all-city">Все города</a>
                            </div>
                        </div>
                        <div class="clear-both"></div>
                    </div>

                    <div class="footer-reklama padding_fotter col-sm-4 col-xs-12 col-md-2 col-lg-2">
                        <div class="footer-reklama-center ">
                            <div class="footer-reklama-img">
                                <img src="/web/img/image-reklama.png"/>
                            </div>
                            <div class="footer-reklama-text">
                                <a class="btn-reklama showReklama">Реклама на сайте</a>
                            </div>
                        </div>
                        <div class="clear-both"></div>
                    </div>
                <div class="footer-contact col-sm-5 col-xs-12  col-md-3 col-lg-3">
                        <div class="footer-contact_center">
                            <span class="footer-contact_title">Быстрая связь</span>
                            <span class="contact-phone">Горячая линия: <?php echo ($settingsArr['phone']);?></span>
                            <span class="contact-mail">e-mail: <?php echo ($settingsArr['e-mail']);?></span>
                            <span class="footer-developer">Сайт был создан в <a href="#">Рекламотерапии</a></span>
                            <span class="footer-copyright-xs">&copy; 2016. Незабудка. Все права защищены.</span>
                        </div>
                        <div class="clear-both"></div>
                </div>
                <div class="clear-both"></div>
            </div>
                <div class="footer-down col-md-3 col-xs-12  col-lg-3">
                    <span class="footer-copyright">&copy; 2016. Незабудка. Все права защищены.</span>
                </div>
</footer>

<div class="hidden"></div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
