<?php

use yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use app\widgets\GetcitynameWidget;
use app\widgets\GetmetronameWidget;
use app\assets\ProductviewAsset;
ProductviewAsset::register($this);
$this->registerJsFile('/js/socket.io-1.3.5.js');
$this->registerJsFile('/js/userOnline.js');

$this->title = 'Незабудка';

?>
<?php
        if(Yii::$app->session->hasFlash('product_update')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Объявление изменено!',
                ]);
        endif; 
        
        if(Yii::$app->session->hasFlash('message_send')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Сообщение отправлено!',
                ]);
        endif; 
        if(Yii::$app->session->hasFlash('message_not_send')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Ошибка!',
                ]);
        endif; 
?>


<div class='print-block'>
    <div class="head-print-block">
        <div class="logo">
            <a class="logo-img"><img src="<?= Url::home(); ?>img/logo.png" alt="">НЕЗАБУДКА</a>
        </div>
    </div>
   
    <div class="container-print-block">
        <div class='title-print-block'>
            <h1 class="product-title"><?= $modelProduct->title; ?></h1>
            <div style="clear:both;"></div>
        </div>
        <div class='number-print-block'>
            <div class='first-print-block'>
                <ul>
                    <li class="card-head_article">№ <?= $modelProduct->id; ?></li>
                    <li class="card-head_date">/
                        <?php
                            $date = date_create($modelProduct->date_create);
                            $month_array = array("01"=>"января","02"=>"февраля","03"=>"марта","04"=>"апреля","05"=>"мая", "06"=>"июня", "07"=>"июля","08"=>"августа","09"=>"сентября","10"=>"октября","11"=>"ноября","12"=>"декабря");
                            ?>
                        <span><?= (int)date_format($date, 'd'); ?> <?= $month_array[date_format($date, 'm')]; ?></span>
                    /</li>
                    <li class="card-head_total"><?= $productCount; ?></li>
                </ul>
            </div>
            <div class="second-print-block">
                <span class="prodavez"><q>Продавец:</q> <a >
                        <?php if(($modelUser->contactname != '') && ($modelUser->contactname != null)){ ?>
                            <?= $modelUser->contactname; ?>
                        <?php }else{ ?>
                            <?= $modelUser->username; ?>
                        <?php } ?>
                    </a></span>
            </div>
        </div>
        <div class='row-print-block'>
                <div class="img-print-block">
                    <div class="print-img-block">
	                    <?php if($modelProduct->img_src != ''){ ?>
                                <img style="width: 100%;"src='<?= Url::home().'img/product/'.$modelProduct->img_src; ?>'/>
	                    <?php }else{ ?>
                                <img src='<?= Url::home(); ?>img/DefaultFotoProduct.png'/>
	                    <?php } ?>
	                    <?php if($productImage){ ?>
		                    <?php foreach($productImage as $image){ ?>
                                    <img style='width: 30%;'src='<?= Url::home(); ?>img/product/<?= $image['img_src']; ?>'/>
                            <?php }  ?>
	                    <?php } ?>
                    </div>
                </div>
                <div class="button-print-block">
                    <div class="price-print-block">
                        Цена: <?= $modelProduct->price; ?> руб.
                    </div>
                    <div class="number-show-block">
                        <a  onclick="ShowHidePassword()">Показать тел.:</a><a href="<?= $modelProduct->mobile_number; ?>"></a>
                    </div>
                    <div class="opus-produkt">
                        <div class="title-opus">Описание товара</div>
                        <div class="container-opus"><?= $modelProduct->about; ?></div>
                    </div>
                </div>
            </div>
    </div>
</div>
<div class='print-none'>
    <div class="hidden-xs">
        <?php  if(isset($arrayCategory['parent_parent'])){ ?>
            <a href="<?= Url::home(); ?>category/<?= $arrayCategory['parent_parent']->slug; ?>" class="app_breadcrumb">
               <b> <?=  $arrayCategory['parent_parent']->name; ?></b>
            </a> <img class="img-split" src="/img/arrow-right.png" alt="">
        <?php } ?>
        <?php if(isset($arrayCategory['parent'])){ ?>
            <a href="<?= Url::home(); ?>category/<?= $arrayCategory['parent']->slug; ?>" class="app_breadcrumb">
                <b><?=  $arrayCategory['parent']->name; ?></b>
            </a> <img class="img-split" src="/img/arrow-right.png" alt="">
            <a href="<?= Url::home(); ?>category/<?= $arrayCategory['category']->slug; ?>"  class="app_breadcrumb">
                <b><?= $arrayCategory['category']->name; ?></b>
            </a> <img class="img-split" src="/img/arrow-right.png" alt="">
        <?php } ?>
        <?php  if(isset($modelProduct)){ ?>
            <a class="app_breadcrumb">
               <b> <?=  $modelProduct->title; ?></b>
            </a>
        <?php } ?>

    </div>


    <div class="row no_margin hidden-xs  hidden-sm col-xs-12">
	    <?php if($modelProduct->user_id == \Yii::$app->user->id){ ?>
		    <?php if($modelProduct->status == 1){ ?>
                <div class="admin-freeze ">
				    <?php $form = ActiveForm::begin([]); ?>
                    <input type="hidden" name="Frozen[type]" value="frozen">
				    <?= Html::submitButton('Заморозить объявление') ?>
				    <?php ActiveForm::end(); ?>
                </div>
		    <?php }elseif($modelProduct->status == 4){ ?>
                <div class="admin-freeze ">
				    <?php $form = ActiveForm::begin([]); ?>
                    <input type="hidden" name="Frozen[type]" value="defrost">
				    <?= Html::submitButton('Разморозить объявление') ?>
				    <?php ActiveForm::end(); ?>
                </div>
		    <?php } ?>
	    <?php }else{ ?>
            <?php if($modelProduct->status == 2){ ?>
                <div style="background-color:#00b2e2;padding:15px;margin-top:10px;color:white;">
                    Обьявление снято
                </div>
            <?php }elseif($modelProduct->status == 3){ ?>
                    <div style="background-color:#00b2e2;padding:15px;margin-top:10px;color:white;">
                        Обьявление в архиве
                    </div>
            <?php }elseif($modelProduct->status == 4){ ?>
                    <div style="background-color:#00b2e2;padding:15px;margin-top:10px;color:white;">
                        Обьявление заморожено
                    </div>
            <?php } ?>
        <?php } ?>
    </div>
    
<?php if($modelProduct->user_id == \Yii::$app->user->id){ ?>
    <?= HTML::a('Редактировать объявление', Url::home().'editproduct/'.$modelProduct->id, [ 'style' => 'color: #00b2e2;']); ?>
<?php } ?>


    <div class="row no_margin">
        <div class="col-sm-12 no_padding">
                <h1 class="product-title"><?= $modelProduct->title; ?></h1>
                <div style="clear:both;"></div>
        </div>
        <div class="col-sm-12 no_padding">
                <div class="card-head_base col-sm-12 col-md-9 no_padding">
                        <ul>
                            <li class="card-head_article">№ <?= $modelProduct->id; ?></li>
                            <li class="card-head_date">/
                                <?php
                                    $date = date_create($modelProduct->date_create);
                                    $month_array = array("01"=>"января","02"=>"февраля","03"=>"марта","04"=>"апреля","05"=>"мая", "06"=>"июня", "07"=>"июля","08"=>"августа","09"=>"сентября","10"=>"октября","11"=>"ноября","12"=>"декабря");
                                    ?>
                                <span><?= (int)date_format($date, 'd'); ?> <?= $month_array[date_format($date, 'm')]; ?></span>
                            /</li>
                            <li class="card-head_total"> <?= $productCount; ?></li>
                            <li class="card-head_total_mob col-xs-12">  <?= $productCount; ?></li>

                        </ul>
                        <a class="card-head_print print-button">Распечатать объявление</a>
                </div>

            <div class="card-head_base col-sm-12 col-md-3 no_padding">
                 <span class="seller_mob "><p>Продавец:</p> 
                    <?php  $contactname = ''; ?>
                    <?php if(($modelUser->contactname != '') && ($modelUser->contactname != null)){ ?>
                        <?php  $contactname = $modelUser->contactname; ?>
                    <?php }else{ ?>
                        <?php  $contactname = $modelUser->username; ?>
                    <?php } ?>
                     <a href="<?= Url::home(); ?>seller/<?= $modelUser->id?>"><?= $contactname; ?></a> 
                 </span>
                <span class="seller"><q>Продавец:</q>
                    <a href="<?= Url::home(); ?>seller/<?= $modelUser->id?>"><?= $contactname; ?></a>
                </span>
            </div>
            <div class="row no_margin hidden-md hidden-lg hidden-sm col-xs-12">
                <?php if($modelProduct->user_id == \Yii::$app->user->id){ ?>
                    <?php if($modelProduct->status == 1){ ?>
                        <div class="admin-freeze ">
                            <?php $form = ActiveForm::begin([]); ?>
                            <input type="hidden" name="Frozen[type]" value="frozen">
                            <?= Html::submitButton('заморозить объявление') ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                    <?php }elseif($modelProduct->status == 4){ ?>
                        <div class="admin-freeze ">
                            <?php $form = ActiveForm::begin([]); ?>
                            <input type="hidden" name="Frozen[type]" value="defrost">
                            <?= Html::submitButton('разморозить объявление') ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                    <?php } ?>
                <?php }else{ ?>
                    <?php if($modelProduct->status == 2){ ?>
                        <div style="background-color:#00b2e2;padding:15px;margin-top:10px;color:white;">
                            Обьявление снято
                        </div>
                    <?php }elseif($modelProduct->status == 3){ ?>
                        <div style="background-color:#00b2e2;padding:15px;margin-top:10px;color:white;">
                            Обьявление в архиве
                        </div>
                    <?php }elseif($modelProduct->status == 4){ ?>
                        <div style="background-color:#00b2e2;padding:15px;margin-top:10px;color:white;">
                            Обьявление заморожено
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>

    </div>

        <div class="row" >
            <div class="col-sm-12 col-md-6">
                <div id="slider" class="flexslider">
                    <div class="slides">
	                    <?php if(($modelProduct->img_src != '') || ($modelProduct->img_src != null)){ ?>
                            <a href="<?= Url::home().'img/product/'.$modelProduct->img_src; ?>" class="fresco" data-fresco-group="example" data-fresco-caption="<?= $modelProduct->title; ?>">
                                <div style="background: url('<?= Url::home().'img/product/'.$modelProduct->img_src; ?>')" class="item-popup-link" data-src="<?= Url::home().'img/product/'.$modelProduct->img_src; ?>"></div>
                            </a>
	                    <?php }else{ ?>
                            <a href="<?= Url::home(); ?>img/DefaultFotoProduct.png" class="fresco" data-fresco-group="example">
                                <div style="background: url('<?= Url::home(); ?>img/DefaultFotoProduct.png')" class="item-popup-link" data-src="<?= Url::home(); ?>img/DefaultFotoProduct.png"></div>
                            </a>
	                    <?php } ?>
	                    <?php if($productImage){ ?>
		                    <?php foreach($productImage as $image){ ?>
                                <a href="<?= Url::home(); ?>img/product/<?= $image['img_src']; ?>" class="fresco" data-fresco-group="example">
                                    <div style="background: url('<?= Url::home(); ?>img/product/<?= $image['img_src']; ?>')" class="item-popup-link" data-src="<?= Url::home(); ?>img/product/<?= $image['img_src']; ?>"></div>
                                </a>
		                    <?php }  ?>
	                    <?php } ?>
                    </div>
                </div>
                <div id="carousel" class="flexslider">
                    <ul class="slides">
	                    <?php if($modelProduct->img_src != ''){ ?>
                            <li class="c_item" style="background: url('<?= Url::home().'img/product/'.$modelProduct->img_src; ?>')"></li>
	                    <?php }else{ ?>
                            <li  class="c_item" style="background: url('<?= Url::home(); ?>img/DefaultFotoProduct.png')"></li>
	                    <?php } ?>
	                    <?php if($productImage){ ?>
		                    <?php foreach($productImage as $image){ ?>
                                <li  class="c_item" style="background: url('<?= Url::home(); ?>img/product/<?= $image['img_src']; ?>')"></li>
		                    <?php }  ?>
	                    <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">

                <div class="card_price col-sm-12">Цена: <?= $nombre_format_francais = number_format($modelProduct->price,0,' ', ' '); ?> руб.</div>
                <div class="col-sm-12 col-md-8 name_city">
                    <p>Город: <?= GetcitynameWidget::widget(['city_id' => $modelProduct->city_id]); ?> </p>
                    <?php if(($modelProduct->metro_id != '') && $modelProduct->metro_id != null){ ?>
                        <p>Станция метро: <?= GetmetronameWidget::widget(['metro_id' => $modelProduct->metro_id]); ?> </p>
                    <?php } ?>
                </div>
                <!--<a href="#" class="cold-ads">Заморозить объявление</a>-->
                <div class="card-head_connect col-sm-12" style="top:0px;position:relative;">
                  <?php
                        if(strlen($modelProduct->mobile_number) !== 0){
                    ?>
                            <div class="col-sm-12 no_padding phone_java"><div class="btn-connect_one"><a  onclick="ShowHidePassword()">Показать тел.:</a><a    href="tel:  <?= $modelProduct->mobile_number; ?>"> <a class="pid"> +7XXX XXX-XX-XX</a> <a class="mobile_show"><?= $modelProduct->mobile_number; ?></a></a></div></div>

                        <?php   }
                    else{
                        echo "";
                    }
                    ?>
                       <?php
                       if(strlen($modelProduct->whatsapp) !== 0){
                        ?>
                           <a href="whatsapp://send?text=Незабутка!&phone=+9198345343451" class="btn-connect_two">WhatsApp</a>
                    <?php   }
                        else{
                           echo "";
                       }
                       ?>
                    <?php
                    if(strlen($modelProduct->viber) !== 0){
                        ?>
                        <a href="viber://tel:9198345343451" class="btn-connect_three">Viber</a>
                    <?php   }
                    else{
                        echo "";
                    }
                    ?>
                </div>

                <div style="clear:both;"></div>
                <?php if($modelProduct->user_id == \Yii::$app->user->id) {
                }else {?>

                <?php if (!\Yii::$app->user->isGuest){ ?>
                    <?php  if($favoriteStatus != null){ ?>
                       <div class="cold-ads deleteWithFavorite" data-product_id="<?= $modelProduct->id; ?>" ><span>Удалить из избранного</span></div>
                   <?php }else{ ?>
                       <div class="cold-ads addToFavorite" data-product_id="<?= $modelProduct->id; ?>" ><span>Добавить в избранное</span></div>
                   <?php } ?>
               <?php }else{ ?>
                       <div class="cold-ads addToFavorite please_register paddng_star" data-product_id="<?= $modelProduct->id; ?>" ><span>Добавить в избранное</span></div>
               <?php } ?>
                     <?php } ?>
                <?php if (!\Yii::$app->user->isGuest){ ?>
                    <?php if($modelUser->id != \Yii::$app->user->id){ ?>
                        <div class="card-head_trade  changeProfileData col-sm-12">
                                <a  class="btn-send_seller ">Написать продавцу</a>
                        </div>
                        <div id="modalUserUpdate" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content content_sviat">
                                    <div class="modal-header line_header head_form_product">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title ">Написать сообщения</h4>
                                    </div>
                                    <?php $form = ActiveForm::begin([]); ?>
                                        <div class="modal-body modal_bodu_padding form-text">
                                                <?= $form->field($modelNewMessage, 'recipient_id')->hiddenInput(['value' => $modelProduct->user_id])->label(false); ?>
                                                <div class="col-xs-12 col-sm-12 class_center">
                                                    <?= $form->field($modelNewMessage, 'title')->textInput(['class' => 'form-control form_style_border form_tex_in ','value' => $modelProduct->title])->label("Заголовок:");; ?>
                                                </div>
                                                    <div class="col-xs-12 col-sm-12 class_center">
                                                        <?= $form->field($modelNewMessage, 'text')->textarea(['class' => 'form-control  form_style_border   width_sviat_message','rows' => 10 ,'placeholder'=>'Текст сообщения'])->label("Напишите сообщения:"); ?>
                                                    </div>
                                        </div>
                                        <div class="modal-footer line_foter">
                                            <div class="col-xs-12 col-sm-12 ">
                                          <!--  <button type="button" class="btn_per  button_personal1" data-dismiss="modal">Close</button>-->
                                            <?= Html::submitButton('Отправить сообщение', ['class' => 'btn_per  button_personal2 button_message button_mesage_svita1 ']) ?>
                                                </div>
                                        </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php }else{ ?>                    
                    <div class="card-head_trade col-sm-12">
                            <a  class="btn-send_seller please_register">Написать продавцу</a>
                    </div>
                <?php } ?>
            </div>
	</div>

    <section class="product-description clearfix">
        <div class="description_title">Описание товара</div>
        <div class="mail"><p><?= $modelProduct->about; ?></p></div>
    </section>
    
    <?php if(($modelUser->address != '') || ($modelUser->radius != '')){ ?>
        <section class="clearfix" style="margin-top:50px;">
            <div class="map-radius-title-find">Радиус продаж</div>
            <input type="hidden" name="user_address" id="user_address" value="<?= $modelUser->address; ?>">
            <input type="hidden" name="user_radius" id="user_radius" value="<?= $modelUser->radius; ?>">
            <div id="map" style="width:100%;height:300px;"></div>
        </section>
    <?php } ?>

    <?php if(count($modelUserProducts) != 0){ ?>
        <section class="all-product-seller">
            <div class="all-product-title">Другие товары продавца</div>
            <div class="carousel-all-product">
                <ul class="slides">
                    <?php foreach($modelUserProducts as $product){ ?>
                            <li class="main-item-layer clearfix productBlock col-xs-12 col-sm-6 col-md-4 col-lg-3 " <?= $product['id']; ?>>
                                <div class="main-cat-item">

                                    <a href="<?= Url::home(); ?>product/<?= $product['id']; ?>">

                                        <div class="item-img " style="display: inherit">
                                            <img src="<?= Url::home().'img/product/'.$product['img_src']; ?>" alt=""  >
                                        </div>
                                    <div class="item-descr"><p><?php echo $product['title']; ?></p></div>
                                    <div class="item-price"><span><?= $nombre_format_francais = number_format($product['price'],0,' ', ' '); ?>руб</span></div>
                                    <div class="item-date">
                                        <?php
                                        $date = date_create($product['date_create']);
                                        $month_array = array("01"=>"января","02"=>"февраля","03"=>"марта","04"=>"апреля","05"=>"мая", "06"=>"июня", "07"=>"июля","08"=>"августа","09"=>"сентября","10"=>"октября","11"=>"ноября","12"=>"декабря");
                                        ?>
                                        <span>от <?= (int)date_format($date, 'd'); ?> <?= $month_array[date_format($date, 'm')]; ?>, <?= date_format($date, 'Y') ?></span>
                                    </div>
                                </a>
                            </div>

                     </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="custom-navigation col-xs-12">
                <a href="#" class="flex-prev"><img src="/img/arrow-left.png" alt=""></a>
                <div class="custom-controls-container"></div>
                <a href="#" class="flex-next"><img src="/img/arrow-right.png" alt=""></a>
            </div>
        </section>
    <?php } ?>
    </section>
</div>
