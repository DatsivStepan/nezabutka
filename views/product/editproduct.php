<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use app\assets\AddproductAsset;
AddproductAsset::register($this);

$this->registerJsFile('/js/socket.io-1.3.5.js');
$this->registerJsFile('/js/userOnline.js');

$this->title = 'Незабудка';

?>
<div class="table table-striped" class="files" id="previews">
  <div id="template" class="file-row">    
  </div>
</div>


<div  class="head_form_prp"><h1>Редактирования обьявления</h1></div>
<div class="title_form_prp"><span>Заполните все поля, чтобы изменить свое обьявления</span></div>

<div class="main-content">
    <div class="add-product col-sm-12" style="margin-bottom:20px;">

            <?php $form = ActiveForm::begin([]); ?>
                <?= $form->field($modelProduct, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
                <div class="row parentCategory">
                    <div class="col-sm-12 col-md-3 choose-category">
                        Выберите категорию:
                    </div>
                    <div class="col-sm-12 col-md-9 categoryBlock">
                        
                        <?php if(isset($arrayCategory['parent_parent'])){?>
                            <?= $form->field($modelProduct, 'category_id')->dropDownList($arrayCategory['parent_parent_array'],['class' => ' form_text parentCategoryDropdown form-control', 'value' => $arrayCategory['parent_parent']->id,'style' => 'display:block;'])->label(false); ?>
                            <?= $form->field($modelProduct, 'category_id_f')->dropDownList($arrayCategory['parent_array'],['class' => ' form_text firstChildCategoryDropdown form-control','value' => $arrayCategory['parent']->id, 'style' => 'display:block;'])->label(false); ?>
                            <?= $form->field($modelProduct, 'category_id_l')->dropDownList($arrayCategory['category_array'],['class' => ' form_text lastChildCategoryDropdown form-control form_text','prompt' => 'Выберите категорию','value' => $arrayCategory['category']->id])->label(false); ?>
                        <?php }elseif(isset($arrayCategory['parent'])){?>
                            <?= $form->field($modelProduct, 'category_id')->dropDownList($arrayCategory['parent_array'],['class' => ' form_text parentCategoryDropdown form-control', 'value' => $arrayCategory['parent']->id,'style' => 'display:block;'])->label(false); ?>
                            <?= $form->field($modelProduct, 'category_id_f')->dropDownList($arrayCategory['category_array'],['class' => ' form_text firstChildCategoryDropdown form-control','value' => $arrayCategory['category']->id, 'style' => 'display:block;'])->label(false); ?>
                            <?php if(!empty($arrayCategory['parent_parent_array'])){ ?>
                                <?= $form->field($modelProduct, 'category_id_l')->dropDownList($arrayCategory['parent_parent_array'],['class' => ' form_text lastChildCategoryDropdown form-control form_text','prompt' => 'Выберите категорию','style' => 'display:block;'])->label(false); ?>
                            <?php }else{ ?>
                                <?= $form->field($modelProduct, 'category_id_l')->dropDownList([],['class' => ' form_text lastChildCategoryDropdown form-control form_text','prompt' => 'Выберите категорию','style' => 'display:none;'])->label(false); ?>
                            <?php }   ?>
                        <?php }else{ ?>
                            <?= $form->field($modelProduct, 'category_id')->dropDownList($arrayCategory['category_array'],['class' => ' form_text parentCategoryDropdown form-control', 'value' => $arrayCategory['category']->id,'style' => 'display:block;'])->label(false); ?>
                            <?= $form->field($modelProduct, 'category_id_f')->dropDownList($arrayCategory['parent_array'],['class' => ' form_text firstChildCategoryDropdown form-control', 'style' => 'display:block;'])->label(false); ?>
                            <?= $form->field($modelProduct, 'category_id_l')->dropDownList([],['class' => ' form_text lastChildCategoryDropdown form-control form_text','prompt' => 'Выберите категорию','style' => 'display:none;'])->label(false); ?>                            
                        <?php } ?>                        
                    </div>
                </div>
                <div class="row сategoryAttribute" style="display: none;margin-top:10px;margin-bottom: 10px;padding:10px;border-top:1px solid #dbdbdb;border-bottom:1px solid #dbdbdb;">
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-3 choose-category">
                        Заголовок обьявления:
                    </div>
                    <div class="col-sm-12 col-md-9 ">
                        <?= $form->field($modelProduct, 'title')->textInput(['class' => 'form_text  form-control'])->label(false); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-3 choose-category">
                        Текст обьявления:
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <?= $form->field($modelProduct, 'about')->textarea(['rows' => '3', 'class' => 'form_text  form-control'])->label(false) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-3 choose-category">
                        Укажите цену:
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <?= $form->field($modelProduct, 'price')->textInput(['type' => 'number', 'class' => 'form_text  form-control'])->label(false); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-3 choose-category">
                        Загрузите фото:
                    </div>
                    <input type="hidden" id="product-image_array" name="Product[image]" value='<?= $stringProductImage; ?>'>
                    <div class="col-sm-12 col-md-9">
                        <div class="maxPhoto" style="display:none;border:1px solid red;color:red;">Максимум 6 фото</div>
                        <div class="row" style="margin-right: -3px;margin-left: -3px;">
                            <?php if($modelProduct->img_src != ''){ ?>
                                <div class="col-sm-6 col-md-4" style="position:relative;padding:5px;">
                                    <div class="container__wrapper" style="box-shadow: 0 0 5px rgba(0,0,0,0.5);overflow: hidden;position: relative;width: 100%;padding-bottom: 100%;">
                                        <div class="container__content addPhoto" style='background-image: url(../../../img/product/<?= $modelProduct->img_src; ?>)'></div>
                                    </div>
                                    <i class="fa fa-times fa-2x deleteProductImage"  data-name="<?= $modelProduct->img_src; ?>" style="cursor:pointer;position:absolute;right:5px;top:-10px;"></i>
                                </div>
                            <?php }else{ ?>
                                <div class="col-sm-6 col-md-4" style="position:relative;padding:5px;">
                                    <div class="container__wrapper" style="box-shadow: 0 0 5px rgba(0,0,0,0.5);overflow: hidden;position: relative;width: 100%;padding-bottom: 100%;">
                                        <div class="container__content addPhoto"></div>
                                    </div>
                                    <i class="fa fa-times fa-2x deleteProductImage"  data-name="" style="cursor:pointer;display:none;position:absolute;right:5px;top:-10px;"></i>
                                </div>
                            <?php } ?>
                            <?php if($modelProductImage){ ?>
                                    <?php for ($i = 0; $i <= 4; $i++) { ?>
                                            <?php  if(array_key_exists($i,$modelProductImage)){ ?> 
                                                <div class="col-sm-6 col-md-4" style="position:relative;padding:5px;">
                                                    <div class="container__wrapper" style="box-shadow: 0 0 5px rgba(0,0,0,0.5);overflow: hidden;position: relative;width: 100%;padding-bottom: 100%;">
                                                        <div class="container__content addPhoto"  style="background-image: url(../../../img/product/<?= $modelProductImage[$i]['img_src']; ?>)"></div>
                                                    </div>
                                                    <i class="fa fa-times fa-2x deleteProductImage"  data-name="<?= $modelProductImage[$i]['img_src']; ?>" style="cursor:pointer;position:absolute;right:5px;top:-10px;"></i>
                                                </div>                                    
                                            <?php }else{ ?>
                                                <div class="col-sm-6 col-md-4" style="position:relative;padding:5px;">
                                                    <div class="container__wrapper" style="box-shadow: 0 0 5px rgba(0,0,0,0.5);overflow: hidden;position: relative;width: 100%;padding-bottom: 100%;">
                                                        <div class="container__content addPhoto"></div>
                                                    </div>
                                                    <i class="fa fa-times fa-2x deleteProductImage"  data-name="" style="cursor:pointer;display:none;position:absolute;right:5px;top:-10px;"></i>
                                                </div>
                                            <?php }  ?>
                                    <?php }?>
                            <?php }else{ ?>
                                    <div class="col-sm-6 col-md-4" style="position:relative;padding:5px;">
                                        <div class="container__wrapper" style="box-shadow: 0 0 5px rgba(0,0,0,0.5);overflow: hidden;position: relative;width: 100%;padding-bottom: 100%;">
                                            <div class="container__content addPhoto"></div>
                                        </div>
                                        <i class="fa fa-times fa-2x deleteProductImage"  data-name="" style="cursor:pointer;display:none;position:absolute;right:5px;top:-10px;"></i>
                                    </div>
                                    <div class="col-sm-6 col-md-4" style="position:relative;padding:5px;">
                                        <div class="container__wrapper" style="box-shadow: 0 0 5px rgba(0,0,0,0.5);overflow: hidden;position: relative;width: 100%;padding-bottom: 100%;">
                                            <div class="container__content addPhoto"></div>
                                        </div>
                                        <i class="fa fa-times fa-2x deleteProductImage"  data-name="" style="cursor:pointer;display:none;position:absolute;right:5px;top:-10px;"></i>
                                    </div>

                                    <div class="col-sm-6 col-md-4" style="position:relative;padding:5px;">
                                        <div class="container__wrapper" style="box-shadow: 0 0 5px rgba(0,0,0,0.5);overflow: hidden;position: relative;width: 100%;padding-bottom: 100%;">
                                            <div class="container__content addPhoto"></div>
                                        </div>
                                        <i class="fa fa-times fa-2x deleteProductImage"  data-name="" style="cursor:pointer;display:none;position:absolute;right:5px;top:-10px;"></i>
                                    </div>
                                    <div class="col-sm-6 col-md-4" style="position:relative;padding:5px;">
                                        <div class="container__wrapper" style="box-shadow: 0 0 5px rgba(0,0,0,0.5);overflow: hidden;position: relative;width: 100%;padding-bottom: 100%;">
                                            <div class="container__content addPhoto"></div>
                                        </div>
                                        <i class="fa fa-times fa-2x deleteProductImage"  data-name="" style="cursor:pointer;display:none;position:absolute;right:5px;top:-10px;"></i>
                                    </div>
                                    <div class="col-sm-6 col-md-4" style="position:relative;padding:5px;">
                                        <div class="container__wrapper" style="box-shadow: 0 0 5px rgba(0,0,0,0.5);overflow: hidden;position: relative;width: 100%;padding-bottom: 100%;">
                                            <div class="container__content addPhoto"></div>
                                        </div>
                                        <i class="fa fa-times fa-2x deleteProductImage"  data-name="" style="cursor:pointer;display:none;position:absolute;right:5px;top:-10px;"></i>
                                    </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
        
                <div class="row" style="margin-top:20px;margin-bottom: 20px;padding: 20px 0px 20px; border-top:1px solid #dbdbdb;border-bottom:1px solid #dbdbdb;">
                    <div class="col-sm-12" style="padding:0px;">
                        <div class="col-sm-12 col-md-3 choose-category">
                            Город:
                        </div>
                        <div class="col-sm-12 col-md-9">
                            <input type="text" name="city_name" class="form-control form_text" value="<?= app\widgets\GetcitynameWidget::widget(['city_id' => $modelProduct->city_id]); ?>">
                            
                            <div style="position:relative;">
                                <div style="position:absolute;top:0px;background-color:white;width:100%;z-index:99;">
                                    <ul class="appProductChoiseCity">
                                    </ul>
                                </div>                                
                            </div>
                            
                            <?= $form->field($modelProduct, 'city_id')->hiddenInput()->label(false); ?>
                        </div>
                    </div>
                    <?php $display = 'display:none;'; ?>
                    <?php if(($modelProduct->metro_id != '') && ($modelProduct->metro_id != null)){ ?>
                        <?php $display = 'display:block;'; ?>
                    <?php } ?>
                    <div class="col-sm-12 metroBlock" style="<?= $display; ?>padding:0px;">
                        <div class="col-sm-12 col-md-3 choose-category">
                            Станция метро:
                        </div>
                        <div class="col-sm-12 col-md-9">
                            <?= $form->field($modelProduct, 'metro_id')->dropDownList($arrayMetro, ['class' => 'form-control form_text'])->label(false); ?>
                        </div>
                    </div>
                    <div class="col-sm-12" style="padding:0px;">
                        <div class="col-sm-12 col-md-3 choose-category">
                            Номер телефона:
                        </div>
                        <div class="col-sm-12 col-md-9">
                            <?= $form->field($modelProduct, 'mobile_number')->textInput(['class' => 'form_text  form-control','value' => $modelUser['telefone']])->label(false); ?>
                        </div>
                    </div>
                    <div class="col-sm-12" style="padding:0px;">
                        <div class="col-sm-12 col-md-3 choose-category">
                            WhatsApp:
                        </div>
                        <div class="col-sm-12 col-md-9">
                            <?= $form->field($modelProduct, 'whatsapp')->textInput(['class' => 'form_text  form-control'])->label(false); ?>
                        </div>
                    </div>
                    <div class="col-sm-12" style="padding:0px;">
                        <div class="col-sm-12 col-md-3  choose-category">
                            Viber:
                        </div>
                        <div class="col-sm-12 col-md-9">
                            <?= $form->field($modelProduct, 'viber')->textInput(['class' => 'form_text  form-control'])->label(false); ?>
                        </div>
                    </div>
                </div>

                <div style="clear:both;">
                </div>
        <div class="form-group">
            <?= Html::submitButton('Разместить объявление', ['class' => 'reg-button ']) ?>
        </div>
            <?php ActiveForm::end(); ?>
    </div>
    <div style="clear: both"></div>
    
</div>
