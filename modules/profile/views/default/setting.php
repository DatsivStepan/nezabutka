<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;

?>
                         <div class="breadcrumb">
                                <ul>
                                        <li><a href="index.html">Главная</a></li>
                                        <li>Личный кабинет</li>
                                </ul>
                        </div>
                        <div class="cat-avto_title">
                                <h1>Ваш личный кабинет</h1>
                        </div>
                        <div class="total-score">Ваш счет: <span>3509 руб.</span></div>
                            <?= $this->render('menu'); ?>
                </div>
        </div>
</div>
<div class="personal-page">
    <div class="container">
            <div id="option" class="personal-block"  style="display:block !important;">
                    <div class="personal-title personal-title_mob personal-title_option">Изменить контактные данные:</div>
                    <div class="personal-option">
                           
                                    <label for="#">Город:
                                            <input type="text" name="city" id="" placeholder="Введите ваш город" value="<?php echo $modelUser['city']; ?>">
                                    </label>
                                    <label for="#">Контактное лицо:
                                            <input type="text" name="contactname" id="" placeholder="Введите имя" value="<?php echo $modelUser['contactname']; ?>">
                                    </label>
                                    <label for="#">Номер телефона:
                                            <input type="text" name="telefone" id="" placeholder="Введите телефон" value="<?php echo $modelUser['telefone']; ?>">
                                    </label>
                                    <button id="btn-set1">Сохранить</button>
                            
                    </div>
                    <div class="personal-title personal-title_mob personal-title_option">Изменить пароль:</div>
                    <div class="personal-option">
                
                                    <label for="#">Новый пароль:
                                            <input type="password" name="password1" id="" placeholder="************">
                                    </label>
                                    <label for="#">Повторить новый пароль:
                                            <input type="password" name="password2" id="" placeholder="************">
                                    </label>
                                    <button id="btn-set2">Сохранить</button>
                           
                    </div>
                    <div class="personal-title personal-title_mob personal-title_option">Изменить адрес электронной почты:</div>
                    <div class="personal-option">
                           
                                   
                                    <label for="#">Новый адрес электронной почты:
                                            <input type="text" name="email" id="" placeholder="email" value="<?php echo $modelUser['email']; ?>">
                                    </label>
                                    <button id="btn-set3">Сохранить</button>
                           
                    </div>
                    <div class="personal-title personal-title_mob personal-title_option">Удалить учетную запись:</div>
                    <div class="personal-option">
                            <button id="btn-set4" class="delete-btn">Удалить учетную запись</button>
                    </div>
            </div>
    </div>
</div>