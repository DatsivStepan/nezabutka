<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
$this->title = 'Личный кабинет';
?>
<?= $this->render('menu',['modelUser' => $modelUser]); ?>

<?php
        if(Yii::$app->session->hasFlash('add_product')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Обьявления добавлено!',
                ]);
        endif; 
?>

<div class="personal-page" style="">
    <div class="">
            <div class="cat-avto_title personal-page_title">
                    <h1 style="margin-top:0px;">Замороженные объявления</h1>
                    <?php foreach($modelProduct as $product){ ?>
                <div class="main-item-layer clearfix productBlock<?= $product['id']; ?> col-xs-12 col-sm-6 col-md-4 col-lg-3 ">
                    <div class="delete_p">
                        <img src="/web/img/krest.png">
                        <span class="deleteProduct" data-product_id="<?= $product['id']; ?>" data-status="frozen" >Разморозить</span>
                    </div>
                    <div class="main-cat-item">

                        <a href="<?= Url::home(); ?>product/<?= $product['id']; ?>">

                            <div class="item-img " style="display: inherit">
                                <img src="<?= Url::home().'img/product/'.$product['img_src']; ?>" alt=""  >
                            </div>
                                    <div class="item-descr">
                                        <p><?php echo $product['title']; ?></p>
                                    </div>
                                    <div class="item-price">
                                        <span><?= $nombre_format_francais = number_format($product['price'],0,' ', ' '); ?> руб</span>
                                    </div>
                                    <div class="item-date">
                                        <?php
                                            $date = date_create($product['date_create']);
                                            $month_array = array("01"=>"января","02"=>"февраля","03"=>"марта","04"=>"апреля","05"=>"мая", "06"=>"июня", "07"=>"июля","08"=>"августа","09"=>"сентября","10"=>"октября","11"=>"ноября","12"=>"декабря");
                                        ?>
                                        <span>от <?= (int)date_format($date, 'd'); ?> <?= $month_array[date_format($date, 'm')]; ?> <?= date_format($date, 'Y') ?></span>
                                    </div>
                                </a>
                            </div>
                    </div>
                    <?php } 
                    if(empty($modelProduct)){?>
                       <div class="product-empty-failed col-xs-12">Объявлений пока нет</div>
                    <?php } ?>
            </div>
            <div class="col-sm-12">
                <?= LinkPager::widget(['pagination'=>$pagination]);  ?>
            </div>
    </div>
</div>