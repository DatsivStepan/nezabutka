<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\assets\SellerAsset;
SellerAsset::register($this);
$this->registerJsFile('/js/socket.io-1.3.5.js');
$this->registerJsFile('/js/userOnline.js');

?>
<div class="row">
    <div class="personal-page_sller">
        <div class="">
                <div class="row">
                    <input type="hidden" id="userId" value="<?=$modelUser->id;?>">
                    <?php
                    $datetime1 = date_create($modelUser->date_create);
                    $datetime2 = date_create(date("Y-m-d H:i:s"));
                    $interval = date_diff($datetime1, $datetime2);


                    ?>
                  <div class="head_seller "><h1 ><span class="checkStatus"></span><?= $modelUser->contactname; ?> <a>На сайте: <?= $interval->days; ?> дней</a> <q><?= $interval->days; ?> дней на сайте</q> </h1></div>
                    <div class="col-sm-6 col-md-4  photo_seller">
                            <?php
                                $image_src = Url::home().'img/users/default_avatar.jpg';
                                if($modelUser->avatar != ''){
                                    $image_src = Url::home().''.$modelUser->avatar;
                                }
                            ?>
                            <img src="<?= $image_src; ?>" style="width:100%;">
                        </div>
                    <div class="col-sm-12 col-md-7 col-lg-6 seller_left">

                        <div class="card-head_connect col-sm-12" style="top:0px;position:relative;">
                            <?php
                            if(strlen($modelUser->telefone) !== 0){
                            ?>
                            <div class="col-sm-12 no_padding phone_java"><div class="btn-connect_one"><a  onclick="ShowHidePassword()">Показать тел.:</a><a    href="tel:  <?= $modelUser->telefone; ?>"> <a class="pid"> +7XXX XXX-XX-XX</a> <a class="mobile_show"><?= $modelUser->telefone; ?></a></a></div></div>
                            <?php   }
                            else{
                                echo "";
                            }
                            ?>
                            <?php if(strlen($modelUser->whatsapp) !== 0){ ?>
                                <a href="whatsapp://send?text=Незабутка!&phone=+9198345343451" class="btn-connect_two">WhatsApp</a>
                            <?php }else{
                                echo "";
                            }?>
                            <?php if(strlen($modelUser->viber) !== 0){ ?>
                                <a href="viber://tel:9198345343451" class="btn-connect_three">Viber</a>
                            <?php }else{
                                echo "";
                            } ?>
                        </div>
                        <div style="clear:both;"></div>
                        
                        <?php if (!\Yii::$app->user->isGuest){ ?>
                            <?php if($modelUser->id != \Yii::$app->user->id){ ?>
                                <div class="card-head_trade card-head_trade_sellser col-sm-12">
                                    <a class="btn-send_seller btn-send_personal buttonSendMessage"  data-toggle="modal" data-target="#modalSendMessage" >Написать продавцу</a>
                                </div>
                                <div id="modalSendMessage" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content content_sviat">
                                            <div class="modal-header line_header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title ">Написать сообщения</h4>
                                            </div>
                                            <?php $form = ActiveForm::begin([]); ?>
                                                <div class="modal-body">
                                                    <?= $form->field($modelNewMessage, 'recipient_id')->hiddenInput(['value' => $modelUser->id])->label(false); ?>
                                                    <?= $form->field($modelNewMessage, 'title')->textInput(['class' => 'form-control','value' => '']); ?>
                                                    <?= $form->field($modelNewMessage, 'text')->textarea(['class' => 'form-control']); ?>
                                                </div>
                                                <div class="modal-footer line_foter">
                                                    <button type="button" class="btn_per  button_personal1" data-dismiss="modal">Close</button>
                                                    <?= Html::submitButton('Сохранить', ['class' => 'btn_per  button_personal2']) ?>
                                                </div>
                                            <?php ActiveForm::end(); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php }else{ ?>
                            <div class="card-head_trade card-head_trade_sellser col-sm-12">
                                <a class="btn-send_seller btn-send_personal please_register">Написать продавцу</a>
                            </div>                            
                        <?php } ?>
                        
                    </div>
                </div>
                <div class="cat-avto_title personal-page_title">
                        <h1 style="margin-top:0px;">Актуальные товары продавца</h1>
                        <?php foreach($modelProduct as $product){ ?>

                            <div class="main-item-layer clearfix productBlock col-xs-12 col-sm-6 col-md-4 col-lg-3 " <?= $product['id']; ?>>
                                <div class="main-cat-item">

                                    <a href="<?= Url::home(); ?>product/<?= $product['id']; ?>">

                                        <div class="item-img " style="display: inherit">
                                            <img src="<?= Url::home().'img/product/'.$product['img_src']; ?>" alt=""  >
                                        </div>
                                        <div class="item-descr"><p><?php echo $product['title']; ?></p></div>
                                        <div class="item-price"><span><?= $nombre_format_francais = number_format($product['price'],0,' ', ' '); ?> руб</span></div>
                                        <div class="item-date">
                                            <?php
                                            $date = date_create($product['date_create']);
                                            $month_array = array("01"=>"января","02"=>"февраля","03"=>"марта","04"=>"апреля","05"=>"мая", "06"=>"июня", "07"=>"июля","08"=>"августа","09"=>"сентября","10"=>"октября","11"=>"ноября","12"=>"декабря");
                                            ?>
                                            <span>от <?= (int)date_format($date, 'd'); ?> <?= $month_array[date_format($date, 'm')]; ?>, <?= date_format($date, 'Y') ?></span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <?php } 
                        if(empty($modelProduct)){?>
                            <div class="product-empty-failed col-xs-12">Объявлений пока нет</div>
                        <?php } ?>
                </div>
                <div class="col-sm-12">
                    <?= LinkPager::widget(['pagination'=>$pagination]);  ?>
                </div>
        </div>
    </div>
</div>