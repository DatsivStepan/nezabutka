<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use app\widgets\ProductmenucountWidget;
    
    use app\assets\ProfileAsset;
    ProfileAsset::register($this);
    
?>
<?php
    $class = ($this->context->getRoute() == 'profile/default/index')?'focus':'';
    $class2 = ($this->context->getRoute() == 'profile/default/sold')?'focus':'';
    $class3 = ($this->context->getRoute() == 'profile/default/soldout')?'focus':'';
    $class4 = ($this->context->getRoute() == 'profile/default/archive')?'focus':'';
    $class5 = ($this->context->getRoute() == 'profile/default/message')?'focus':'';
    $class6 = ($this->context->getRoute() == 'profile/default/favorite')?'focus':'';
    $class7 = ($this->context->getRoute() == 'profile/default/frozen')?'focus':'';
?>

    <div class="col-sm-12 up_menu_personsl" style="margin-top:20px;">
        <a href="<?= Url::home(); ?>"">Главная</a> <img src="/web/img/arrow-right.png">
        <a >Личный кабинет</a>
    </div>
    <div class="exit">
        <?= Html::beginForm(['/site/logout'], 'post',['class'=>'exit']) ?>
            <?= Html::submitButton('Выйти',['style' => 'border:1px solid transparent; background-color: white; color: #ff6f69; font-family: Lato; font-weight: 600;']); ?>
        <?= Html::endForm(); ?>
    </div>
    <div class="col-sm-12 title_personal">
        <div class="cat-avto_title">

                <h1>Ваш личный кабинет</h1>



            <div class="kto_wy  ">
                Вы зашли как: <a><?= $modelUser->contactname; ?></a>
                <img class="changeProfileData"  title="Изменить контактные данные" src="/web/img/nasroyki.png" >
            </div>

        </div>

    </div>

<div id="modalUserUpdate" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:60%;">
        <div class="modal-content content_sviat content_sviat1">
            <div class="modal-header line_header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>               
            <div class="lable_style">
                <div class="modal-body text_modal_form" style="padding:15px;padding-top:0px;">
                    <div class="col-sm-12 ">
                          <h4 class="modal-title title_bottom_style  ">Изменить контактные данные:</h4>
                        <?php $form = ActiveForm::begin(['action' => Url::home().'profile/'.$modelUser->id]); ?>
                            <div class="col-sm-6 padding_bottom_form1">
                                <?= $form->field($modelUser, 'contactname')->textInput(['class'=>'form-control-width form-control'])->label('Контактное имя'); ?>
                                <?= $form->field($modelUser, 'telefone')->textInput(['class'=>'form-control-width form-control'])->label('Телефон'); ?>
                            </div>
                            <div class="col-sm-6 padding_bottom_form1">
                                <?= $form->field($modelUser, 'whatsapp')->textInput(['class'=>'form-control-width form-control']); ?>
                                <?= $form->field($modelUser, 'viber')->textInput(['class'=>'form-control-width form-control']); ?>
                            </div>
                                <?= Html::submitButton('Сохранить', ['name' => 'update_data','class' => 'btn_per  button_personal2']) ?>
                        <?php ActiveForm::end(); ?>
                    </div>

                    <div class="col-sm-12">
                          <h4 class="modal-title  title_bottom_style title_bottom_style1">Изменить пароль:</h4>
                        <?php $form = ActiveForm::begin(['action' => Url::home().'profile/'.$modelUser->id]); ?>
                            <div class="col-sm-6 padding_bottom_form1">
                                <?= $form->field($modelUser, 'password')->textInput(['class'=>'form-control-width form-control']); ?>
                            </div>
                            <div class="col-sm-6 padding_bottom_form1">
                                <?= $form->field($modelUser, 'password_repeat')->textInput(['class'=>'form-control-width form-control']); ?>
                            </div>
                            <?= Html::submitButton('Сохранить', ['name' => 'update_password', 'class' => 'btn_per  button_personal2']) ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="col-sm-12">
                          <h4 class="modal-title  title_bottom_style title_bottom_style1">Изменить адрес электронной почты:</h4>
                        <?php $form = ActiveForm::begin(['action' => Url::home().'profile/'.$modelUser->id]); ?>
                            <div class="col-sm-6 padding_bottom_form1">
                                <?= $form->field($modelUser, 'password')->textInput(['class'=>'form-control-width form-control']); ?>
                            </div>

                            <div class="col-sm-6 padding_bottom_form1">
                                <?= $form->field($modelUser, 'email')->textInput(['class'=>'form-control-width form-control']); ?>

                            </div>
                            <?= Html::submitButton('Сохранить', ['name' => 'update_email', 'class' => 'btn_per  button_personal2']) ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <div class="col-sm-12 class_remove">
                    <h4 class="modal-title title_bottom_style title_bottom_style1">Удалить учетную запись</h4>
                    <?php $form = ActiveForm::begin(['action' => Url::home().'profile/'.$modelUser->id]); ?>
                        <?= Html::submitButton('Удалить учетную запись', ['name' => 'delete_account', 'class' => 'btn_per  button_personal2 button_personal3']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
        </div>
    </div>
</div>
    </div>

<input type="hidden" id="user_id" value="<?=\Yii::$app->user->id?>">
<div class=" personal-block_tab" style="text-align: center;">
    <?php echo HTML::a(\Yii::t('app', 'Ваши данные'), Url::home().'profile/'.\Yii::$app->user->id, ['class' => $class.' profileMenu']); ?>
    <?php echo HTML::a(\Yii::t('app', 'Продается<sup class="">'.ProductmenucountWidget::widget(['product_status' => 1]).'</sup>'), Url::home().'profile/'.\Yii::$app->user->id.'/sold', ['class' => $class2.' profileMenu']); ?>
    <?php echo HTML::a(\Yii::t('app', 'Продано<sup class="">'.ProductmenucountWidget::widget(['product_status' => 2]).'</sup>'), Url::home().'profile/'.\Yii::$app->user->id.'/soldout', ['class' => $class3.' profileMenu']); ?>
    <?php echo HTML::a(\Yii::t('app', 'Замороженные<sup class="">'.ProductmenucountWidget::widget(['product_status' => 4]).'</sup>'), Url::home().'profile/'.\Yii::$app->user->id.'/frozen', ['class' => $class7.' profileMenu']); ?>
    <?php echo HTML::a(\Yii::t('app', 'В архиве<sup class="">'.ProductmenucountWidget::widget(['product_status' => 3]).'</sup>'), Url::home().'profile/'.\Yii::$app->user->id.'/archive', ['class' => $class4.' profileMenu']); ?>
    <?php echo HTML::a(\Yii::t('app', 'Сообщения<sup class="unreadMsg"></sup>'), Url::home().'profile/'.\Yii::$app->user->id.'/message', ['class' => $class5.' profileMenu']); ?>
    <?php echo HTML::a(\Yii::t('app', 'Избранное<sup class="">'.ProductmenucountWidget::widget(['product_status' => 'favorite']).'</sup>'), Url::home().'profile/'.\Yii::$app->user->id.'/favorite', ['class' => $class6.' profileMenu']); ?>
    <?php //echo HTML::a(\Yii::t('app', 'Настройки'), Url::home().'profile/'.\Yii::$app->user->id.'/setting', ['class' => $class4.' profileMenu']); ?>
</div >

            
            
            
