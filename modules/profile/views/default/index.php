<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
$this->title = 'Личный кабинет';
?>
<?php
        if(Yii::$app->session->hasFlash('email_error')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Неверний email!',
                ]);
        endif;
        
        if(Yii::$app->session->hasFlash('password_error')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Неверний пароль!',
                ]);
        endif;
        if(Yii::$app->session->hasFlash('email_update')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Email адрес изменен!',
                ]);
        endif;
        
        if(Yii::$app->session->hasFlash('email_update')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Email адрес изменен!',
                ]);
        endif;
        
        if(Yii::$app->session->hasFlash('password_update')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Пароль изменен!',
                ]);
        endif;
        
        if(Yii::$app->session->hasFlash('add_product')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Обьявления добавлено!',
                ]);
        endif;


        if(Yii::$app->session->hasFlash('user_update')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Данные изменены!',
                ]);
        endif; 
        if(Yii::$app->session->hasFlash('user_not_update')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Ошибка!',
                ]);
        endif; 
?>
<div class="">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
    
    
    <?= $this->render('menu',['modelUser' => $modelUser]); ?>


    <div class="personal-page" >
        <div class="padding_personal_cabin">
                <div class="row personal_cabin">
                        <div class="col-sm-3 col-md-3 col-lg-2  image_cabinet">
                            <?php
                                $image_src = Url::home().'img/users/default_avatar.jpg';
                                if($modelUser->avatar != ''){
                                    $image_src = Url::home().$modelUser->avatar;
                                }
                            ?>
                            <img class="userProfileImage" src="<?= $image_src; ?>">
                            <div class="blockSavePhotoButton">
                                <input type="hidden" name="image_prodile_src" value="<?= $image_src; ?>">
                                <a class="saveProfilePhoto">Сохранить</a>
                            </div>
                            <div class="change_photo">
                                <a class="changeProfilePhoto">Изменить фото</a>
                            </div>
                        </div>
                    <?php
                    $datetime1 = date_create($modelUser->date_create);
                    $datetime2 = date_create(date("Y-m-d H:i:s"));
                    $interval = date_diff($datetime1, $datetime2);
                    ?>
                        <div class="col-sm-9 col-md-7 col-lg-5 text_cabinet">
                            Ваше имя: <a> <?= $modelUser->contactname; ?><br></a>
                            Вы зарегистрированы: <a> <?= $interval->days; ?>  дней на сайте</a>
                        </div>

                        <div class="col-sm-4 text_cabinet_mob">
                            <div>Ваше имя:</div> <a> <?= $modelUser->contactname; ?><br></a>
                            <div class="text_regist_cabin">Вы зарегистрированы:</div> <a> <?= $interval->days; ?></a>
                        </div>

                        <div class="col-sm-12 col-sm-12 col-md-12 col-lg-5 text_mail_cabinet">
                            <div> <q><img src=../../../../web/img/users/tell.png></q>
                                Телефон: <a><?= $modelUser->telefone; ?><br></a></div>
                            <div> <img src=../../../../web/img/users/message.png>
                                Почта: <a><?= $modelUser->email; ?></a></div>
        </div>
                </div>
                <div class="row">
                    <div class="row-radius-location">
                        <h1>Ваш радиус продаж или точное местоположение</h1>
                        <h3>Укажите на карте ваше местоположение, нажав в том месте, где вы находитесь:</h3>
                    </div>
                    <div class="">
                    <div class="col-sm-12 row-background-map">
                        <div class="col-sm-7 row-background-map-back map_css">
                            <div id="map" class="row-id-map">
                            </div>
                        </div>
                        <div class="col-sm-5 row-location-backdround">
                            <form method="post" class="formRadiusPosition">
                                <input type="hidden" name="save_status" id="saveStatusField" value="true">
                                <div class="row-location-map-city">
                                    <q>Введите город:</q>
                                    <?php 
                                    if($modelUser->address != ''){ ?>
                                        <?php $address = $modelUser->address; ?>
                                    <?php }else{ ?>
                                        <?php $address = ''; ?>
                                    <?php } ?>
                                    <input type="text" value="<?= $address; ?>" required="required" name="search_address" class="form-control kart_city" id="searchTextField">
                                </div>
                                <div class="row-location-map-radius">
                                    Укажите радиус (км)ваших продаж:
                                    <select class="form-control kart_city" name="radius">
                                        <?php if($modelUser->radius == '1'){ ?>
                                            <option value="1" selected>1 км</option>
                                        <?php }else{ ?>
                                            <option value="1">1 км</option>
                                        <?php } ?>
                                            
                                        <?php if($modelUser->radius == '5'){ ?>
                                            <option value="5" selected>5 км</option>
                                        <?php }else{ ?>
                                            <option value="5">5 км</option>
                                        <?php } ?>
                                        
                                        <?php if($modelUser->radius == '10'){ ?>
                                            <option value="10" selected>10 км</option>
                                        <?php }else{ ?>
                                            <option value="10">10 км</option>
                                        <?php } ?>
                                            
                                        <?php if($modelUser->radius == '25'){ ?>
                                            <option value="25" selected>25 км</option>
                                        <?php }else{ ?>
                                            <option value="25">25 км</option>
                                        <?php } ?>
                                            
                                        <?php if($modelUser->radius == '50'){ ?>
                                            <option value="50" selected>50 км</option>
                                        <?php }else{ ?>
                                            <option value="50">50 км</option>
                                        <?php } ?>
                                            
                                        <?php if($modelUser->radius == '50+'){ ?>
                                            <option value="50+" selected>более 50 км</option>
                                        <?php }else{ ?>
                                            <option value="50+">более 50 км</option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <!-- <div style="color:white;margin-top:20px;font-family: Lato;">
                                    Или выбирите готовые варианты:<br>
                                    <input type="radio" name="radius_2" value="ie"> - вся Россия<br>
                                    <input type="radio" name="radius_2" value="opera"> - вся область<br>
                                    <input type="radio" name="radius_2" value="firefox"> - только город<br>
                                </div> -->
                                <a class="btn btnSavePosition">Применить</a>
                            </form>
                        </div>
                    </div>
                        </div>
                </div>
        </div>
    </div>
</div>