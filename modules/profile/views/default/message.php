<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\widgets\UserinfoWidget;
$this->title = 'Личный кабинет';
?>
<?= $this->render('menu',['modelUser' => $modelUser]); ?>

<div class="personal-page head_page_margin">

    <div class="head_message">
        <div class=" col-sm-12 col-md-4 col-lg-3 title_message">
        <a>Ваши сообщения</a>
            </div>
        <div class=" col-sm-12  col-md-3 col-lg-2">
        <div class=" count_message"> новых
            <sup class="unreadMsg sup_style">
                </sup>
        </div>
            </div>
        <div class="col-sm-12 col-md-5 button_style_message">
        <button class="button_message" data-toggle="modal" data-target="#messageModal">Написать новое сообщение</button>

        <div id="messageModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal_sviat">
                <div class="modal-content content_sviat">
                    <div class="modal-header line_header head_form_product">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title ">Написать сообщение</h4>
                    </div>
                    <?php $form = ActiveForm::begin([]); ?>
                        <div class="modal-body modal_bodu_padding_message form-text message_text col-xs12">
                            <div class="col-xs-12 col-sm-5 class_center">
                            <?= $form->field($modelNewMessage, 'recipient_id')->dropDownList($arrayUsers ,['class' => 'form-control form_message form_padding_message col-xs-6'])->label("Выбирите собеседника:"); ?>
                            </div>
                            <div class="col-xs-12 col-sm-5 class_center">
                            <?= $form->field($modelNewMessage, 'title')->textInput(['class' => 'form-control form_message form_padding_message '])->label("Заголовок:"); ?>
                                </div>
                            <div class="col-xs-12 col-sm-5 class_center class_down_message">
                            <?= $form->field($modelNewMessage, 'text')->textarea(['class' => 'form-control  form_message form_message_sviat  form_padding_message ','rows' => 10 ,'placeholder'=>'Текст сообщения'])->label("Напишите сообщение:"); ?>
                                </div>
                        </div>
                        <div class="modal-footer line_foter">
                          <!--  <button type="button" class="btn_per  button_personal1" data-dismiss="modal">Close</button>-->
                            <div class=" class_center">
                                <?= Html::submitButton('Отправить сообщение', ['class' => 'btn_per  button_personal2 button_message button_mesage_svita']) ?>
                        </div>
                            </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

    </div>
        </div>
    <div class="col-xs-12 class_message_mob">
            <div class="col-sm-12">
                <?php foreach($modelMessageGroup as $messageGroup){ ?>
                    <div class="row message_style" >
                        <div class="userInfoBlock    col-sm-12 col-md-6 col-lg-5">
                            <?php if($messageGroup->sender_id == \Yii::$app->user->id){ ?>
                                    <?= UserinfoWidget::widget(['user_id' => $messageGroup->recipient_id, 'date' => $messageGroup->date]) ?>
                            <?php }else{ ?>
                                    <?= UserinfoWidget::widget(['user_id' => $messageGroup->sender_id, 'date' => $messageGroup->date]); ?>
                            <?php } ?>

                        </div>
                        <div class="userInfoBlock  message_design col-xs-12 col-md-6 col-lg-7 group-box">
                            <a  data-group-id="<?=$messageGroup->id;?>" data-sender="<?=($messageGroup->sender_id == \Yii::$app->user->id)?$messageGroup->recipient_id:$messageGroup->sender_id;?>"><?= $messageGroup->title; ?></a>
                            <div class="chat-box">
                                <div class="msg-box ">
                                    <div class="messages style-scrollbar">
                                        <div class="load-data">
                                            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="send-box">
                                    <div class="group-form">
                                        <div class="my_message"><q>Вы можете ответить на сообщение:</q> <g>Ответить на сообщение:</g></div>
                                        <textarea class="send-textarea send_form" cols="30" rows="10" placeholder="Текст сообщения"></textarea>
                                    </div>
                                    <div class="group-form"><button class="send-btn-msg send_message_buttton">Ответить</button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
</div>
            <div class="col-sm-12">
                <?= LinkPager::widget(['pagination'=>$pagination]);  ?>
            </div>
</div>
