<?php

$module_name = basename(dirname(dirname(__FILE__)));

return [
    'modules' => [
        $module_name => [
            'class' => 'app\modules\\'.$module_name.'\\Module'
        ],
    ],

    'components' => [
        'urlManager' => [
            'rules' => [
                $module_name . '/<action:\w+>/<id:\d+>' => $module_name . '/default/<action>',
                $module_name . '/<action:\w+>' => $module_name . '/default/<action>',
            ],
        ],
    ],
];
