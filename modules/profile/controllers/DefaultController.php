<?php

namespace app\modules\profile\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Product;
use app\models\Profile;
use app\models\Favorite;
use yii\data\ActiveDataProvider;
use app\models\Messagegroup;
use app\models\Message;
/**
 * Default controller for the `profile` module
 */
class DefaultController extends Controller
{
    
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    public function actionIndex($id=null)
    {
        if (!\Yii::$app->user->isGuest && (\Yii::$app->user->id == $id)){
            $modelUser = User::find()->where(['id' => $id])->one();
            
            $modelUser->scenario = 'update_profile_data';
            if($_POST){
                if(isset($_POST['update_data'])){
                    $modelUser->scenario = 'update_user_data';
                    if($modelUser->load(Yii::$app->request->post())){
                        if($modelUser->save()){
                            \Yii::$app->session->setFlash('user_update');
                        }else{
                            \Yii::$app->session->setFlash('user_not_update');
                        }
                    }
                }
                if(isset($_POST['update_email'])){
                    $modelUser->scenario = 'update_user_email';
                    if(\Yii::$app->getSecurity()->validatePassword($_POST['User']['password'], $modelUser->password_hash)){
                        if($_POST['User']['email'] != ''){
                            $modelUser->email = $_POST['User']['email'];
                            if($modelUser->save()){
                                \Yii::$app->session->setFlash('email_update');                                
                            }else{
                                \Yii::$app->session->setFlash('email_error');
                            }
                        }else{
                            \Yii::$app->session->setFlash('email_error');
                        }
                    }else{
                        \Yii::$app->session->setFlash('password_error');                        
                    }
                    exit;
                }
                if(isset($_POST['update_password'])){
                    $modelUser->password_hash = \Yii::$app->security->generatePasswordHash($_POST['User']['password']);
                    if($modelUser->load(Yii::$app->request->post())){
                        if($modelUser->save()){
                            \Yii::$app->session->setFlash('password_update');
                        }else{
                            \Yii::$app->session->setFlash('user_not_update');
                        }
                    }
                }
            }
            return $this->render('index',[
                'modelUser' => $modelUser,
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }

    
    public function actionSold($id=null)
    {
        if (!\Yii::$app->user->isGuest && (\Yii::$app->user->id == $id)){
            $modelUser = User::find()->where(['id' => $id])->one();
            $query = Product::find()->where(['status' => '1','user_id' => $id])->orderBy('date_create DESC');
            $modelProduct = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 12]]);
            return $this->render('sold',[
                'modelUser' => $modelUser,
                'modelProduct' => $modelProduct->getModels(),
                'pagination' => $modelProduct->pagination,
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionSoldout($id=null)
    {
        if (!\Yii::$app->user->isGuest && (\Yii::$app->user->id == $id)){
            $modelUser = User::find()->where(['id' => $id])->one();
            $query = Product::find()->where(['status' => '2','user_id' => $id]);
            $modelProduct = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 12]]);
            return $this->render('soldout',[
                'modelUser' => $modelUser,
                'modelProduct' => $modelProduct->getModels(),
                'pagination' => $modelProduct->pagination,
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    public function actionFrozen($id=null)
    {
        if (!\Yii::$app->user->isGuest && (\Yii::$app->user->id == $id)){
            $modelUser = User::find()->where(['id' => $id])->one();
            $query = Product::find()->where(['status' => '4','user_id' => $id]);
            $modelProduct = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 12]]);
            return $this->render('frozen',[
                'modelUser' => $modelUser,
                'modelProduct' => $modelProduct->getModels(),
                'pagination' => $modelProduct->pagination,
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionFavorite($id=null)
    {
        if (!\Yii::$app->user->isGuest && (\Yii::$app->user->id == $id)){
            $modelUser = User::find()->where(['id' => $id])->one();
            $modelFavorite = Favorite::find()->where(['user_id' => $id])->asArray()->all();
            $product_id = [];
            foreach($modelFavorite as $favorite){
                $product_id[] = $favorite['product_id'];
            }
            $query = Product::find()->where(['status' => '1','id' => $product_id]);
            $modelProduct = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 12]]);
            return $this->render('favorite',[
                'modelUser' => $modelUser,
                'modelProduct' => $modelProduct->getModels(),
                'pagination' => $modelProduct->pagination,
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionArchive($id=null)
    {
        if (!\Yii::$app->user->isGuest && (\Yii::$app->user->id == $id)){
            $modelUser = User::find()->where(['id' => $id])->one();
            $query = Product::find()->where(['status' => '3','user_id' => $id]);
            $modelProduct = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 12]]);
            return $this->render('archive',[
                'modelUser' => $modelUser,
                'modelProduct' => $modelProduct->getModels(),
                'pagination' => $modelProduct->pagination,
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionMessage($id=null)
    {
        if (!\Yii::$app->user->isGuest && (\Yii::$app->user->id == $id)){
            $modelUser = User::find()->where(['id' => $id])->one();
            $query = Messagegroup::find()->where(['sender_id' => \Yii::$app->user->id])->orWhere(['recipient_id' => \Yii::$app->user->id])->orderBy('date DESC');
            $modelMessageGroup = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 12]]);
            
            $modelNewMessage = new Message();
            $modelNewMessage->scenario = 'add_message';

            if($_POST){
                if(isset($_POST['Message'])){
                    if($modelNewMessage->load(\Yii::$app->request->post())){
                        $modelNewMessageGroup = new Messagegroup();
                        $modelNewMessageGroup->scenario = 'add_group_message';
                        $modelNewMessageGroup->sender_id = \Yii::$app->user->id;
                        $modelNewMessageGroup->recipient_id = $modelNewMessage->recipient_id;
                        $modelNewMessageGroup->title = $_POST['Message']['title'];
                        if($modelNewMessageGroup->save()){
                            $modelNewMessage->group_id = $modelNewMessageGroup->id;
                            $modelNewMessage->sender_id = $modelNewMessageGroup->sender_id;
                            $modelNewMessage->status = 0;
                            if($modelNewMessage->save()){
                                \Yii::$app->session->setFlash('message_send');
                            }else{
                                \Yii::$app->session->setFlash('message_not_send');                                
                            }
                        }
                    }
                }
            }
            
            $arrayUsers = ArrayHelper::map(User::find()->where(['not',['id' => $id]])->all(), 'id', 'username');
//            var_dump($arrayUsers);exit;
            return $this->render('message',[
                'arrayUsers' => $arrayUsers,
                'modelNewMessage' => $modelNewMessage,
                'modelUser' => $modelUser,
                'modelMessageGroup' => $modelMessageGroup->getModels(),
                'pagination' => $modelMessageGroup->pagination,
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionSetting($id=null)
    {
        $modelUser = User::find()->where(['id' => $id])->one();
        return $this->render('setting',[
            'modelUser' => $modelUser
        ]);
    }
    
    public function actionUpdateaddress()
    {
        $result = [];
        $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();
        if($modelUser != null){
            $modelUser->scenario = 'update_address';
            $modelUser->radius = $_POST['name_radius'];
            $modelUser->address = $_POST['search_address'];
            if($modelUser->save()){
                $result['status'] = 'success';                
            }else{
                $result['status'] = 'error';
            }
        }else{
            $result['status'] = 'error';
        }
        
        echo json_encode($result);
    }
    
    
    
    
    
    public function actionSeller($id=null)
    {
        $modelUser = User::find()->where(['id' => $id])->one();
        $query = Product::find()->where(['status' => '1','user_id' => $id]);
        $modelProduct = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 12]]);
        $modelNewMessage = new Message();
        $modelNewMessage->scenario = 'add_message';
        
        if($_POST){
            if(isset($_POST['Message'])){
                if($modelNewMessage->load(\Yii::$app->request->post())){
                    $modelNewMessageGroup = new Messagegroup();
                    $modelNewMessageGroup->scenario = 'add_group_message';
                    $modelNewMessageGroup->sender_id = \Yii::$app->user->id;
                    $modelNewMessageGroup->recipient_id = $modelNewMessage->recipient_id;
                    $modelNewMessageGroup->title = $_POST['Message']['title'];
                    if($modelNewMessageGroup->save()){
                        $modelNewMessage->group_id = $modelNewMessageGroup->id;
                        $modelNewMessage->sender_id = $modelNewMessageGroup->sender_id;
                        $modelNewMessage->status = 0;
                        if($modelNewMessage->save()){
                            \Yii::$app->session->setFlash('message_send');
                        }else{
                            \Yii::$app->session->setFlash('message_not_send');                                
                        }
                    }
                }
            }
        }
            
        return $this->render('seller',[
            'modelUser' => $modelUser,
            'modelNewMessage' => $modelNewMessage,
            'modelProduct' => $modelProduct->getModels(),
            'pagination' => $modelProduct->pagination,
        ]);
    }
    
    
    
    
    
    
    public function actionProfile()
    {
        $model = ($model = Profile::findOne(Yii::$app->user->id));
            if($model->updateProfile(Yii::$app->request->post())):
               return true; 
            else:
               return false;
            endif;
    }
        public function actionProfilepass()
    {
        $model = ($model = Profile::findOne(Yii::$app->user->id));
        $pas = Yii::$app->request->post();
            if($pas['password1']==$pas['password2']){
             if($model->updateProfile(Yii::$app->request->post())):
               return true; 
             else:
               return false;
             endif;
            }
    }
    
    public function actionProfileemail()
    {
        $model = ($model = Profile::findOne(Yii::$app->user->id));
            if($model->updateProfile(Yii::$app->request->post())):
               return true; 
            else:
               return false;
            endif;
    }
    
    public function actionProfiledelete()
    {
            $model = ($model = Profile::findOne(Yii::$app->user->id));
            if($model->deleteProfile()):
               return true; 
            else:
               return false;
            endif;
    }

    public function actionSaveprofilephoto()
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/img/users_images/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4
            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.png';  //5

            move_uploaded_file($tempFile,$targetFile); //6
            return 'img/users_images/'.\Yii::$app->user->id.'/'.$for_name.'_'.$randNumber.'.png'; //5
        }
    }
    
    public function actionUpdateprofilephoto()
    {
        $result = [];
        $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();
        $modelUser->scenario = 'update_image';
        $modelUser->avatar = $_POST['image_src'];
        if($modelUser->save()){
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }
        
        echo json_encode($result);
    }
}
