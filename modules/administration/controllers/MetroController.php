<?php

namespace app\modules\administration\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\administration\models\LoginForm;
use app\modules\administration\models\ContactForm;
use app\models\User;
use app\models\Category;
use app\models\Metro;
use app\models\Attribute;
use yii\data\ActiveDataProvider;
/**
 * Default controller for the `administration` module
 */
class MetroController extends Controller
{
    public function beforeAction($action) {
        if((!\Yii::$app->user->isGuest) && (Yii::$app->user->identity->type == 'admin')){
                $this->enableCsrfValidation = false;
                return parent::beforeAction($action);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionIndex()
    {
        $this->layout = 'adminLayout';
        $modelNewMetro = new Metro();
        $modelNewMetro->scenario = 'add';
        
        if($_POST){
            if($modelNewMetro->load(Yii::$app->request->post())){
                if($modelNewMetro->save()){
                    Yii::$app->session->setFlash('add_metro');
                }else{
                    Yii::$app->session->setFlash('not_add_metro');
                }
            }
        }
        
        $queryMetro = Metro::find()->orderBy('date_create DESC');
        $modelMetro = new ActiveDataProvider(['query' => $queryMetro, 'pagination' => ['pageSize' => 20]]);
        
        return $this->render('index', [
            'modelNewMetro' => $modelNewMetro,
            
            'modelMetro' => $modelMetro
        ]);
    }
    
    public function actionMetrodelete($id)
    {
        $modelMetro = Metro::find()->where(['id' => $id])->one();
        if($modelMetro->delete()){
            Yii::$app->session->setFlash('delete_metro');
        }else{
            Yii::$app->session->setFlash('not_delete_metro');
        }
    }
    
}
