<?php

namespace app\modules\administration\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\administration\models\LoginForm;
use app\modules\administration\models\ContactForm;
use app\models\User;
use app\widgets\controller\CategorynameWidget;
use app\models\Category;
use app\models\Attribute;
use yii\data\ActiveDataProvider;
/**
 * Default controller for the `administration` module
 */
class CategoryController extends Controller
{
    public function beforeAction($action) {
        if((!\Yii::$app->user->isGuest) && (Yii::$app->user->identity->type == 'admin')){
                $this->enableCsrfValidation = false;
                return parent::beforeAction($action);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionIndex()
    {
        throw new \yii\web\NotFoundHttpException();
    }
        
    public function actionCategorylist()
    {
        $this->layout = 'adminLayout';
        $modelNewCategory = new Category();
        $modelNewCategory->scenario = 'add_category';
        if($_POST){
            if($modelNewCategory->load(Yii::$app->request->post())){
                if($modelNewCategory->parent_id == ''){
                    $modelNewCategory->parent_id = 0;
                }
                if($modelNewCategory->save()){
                    Yii::$app->session->setFlash('add_category');
                }else{
                    Yii::$app->session->setFlash('not_add_category');
                }
            }
        }
        
        $modelCategories = Category::find()->all();
        $categoryArray = [];
        foreach($modelCategories as $category){
            $categoryArray[$category->id] = CategorynameWidget::widget(['category_id' =>  $category->id]);
        }
        asort($categoryArray);
        $modelNewCategory->position = 1;
        
//        $query = Category::find()->where(['parent_id' => 0])->orWhere(['parent_id' => null])->orderBy('name ASC');
//        $modelCategoryAll = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
        
        
        $searchModel = new \app\models\CategorySearch;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('categorylist', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            
            'modelNewCategory' => $modelNewCategory,
            'categoryArray' => $categoryArray,
        ]);
    }
    
    public function actionCategoryupdate($id=null)
    {
        $this->layout = 'adminLayout';
        $modelNewAttribute = new Attribute();
        $modelNewAttribute->scenario = 'add_attribute';
        
        $modelCategories = Category::find()->all();
        $categoryArray = [];
        foreach($modelCategories as $category){
            $categoryArray[$category->id] = CategorynameWidget::widget(['category_id' =>  $category->id]);
        }
        asort($categoryArray);
        
        $modelCategory = Category::find()->where(['id' => $id])->one();
        $modelCategory->scenario = 'add_category';
        if($_POST){
            if($modelCategory->load(Yii::$app->request->post())){
                if($modelCategory->parent_id == ''){
                    $modelCategory->parent_id = 0;
                }
                if($modelCategory->save()){
                    Yii::$app->session->setFlash('update_category');
                }else{
                    Yii::$app->session->setFlash('not_update_category');
                }
            }
        }
        
        
        if($modelCategory != null){
//            $queryAttributes = Attribute::find()->where(['category_id' => $modelCategory->id]);
//            $modelAttributes = new ActiveDataProvider(['query' => $queryAttributes, 'pagination' => ['pageSize' => 10]]);
            
            $arrtibuteSuccess = true;
            $modelCategoryChildd = Category::find()->where(['parent_id' => $modelCategory->id])->all();
            if($modelCategoryChildd != null){
                $arrtibuteSuccess = false;
            }
            
            return $this->render('categoryview', [
//                'modelAttributes' => $modelAttributes->getModels(),
//                'modelNewAttribute' => $modelNewAttribute,
                'modelCategory' => $modelCategory,
                'categoryArray' => $categoryArray,
                'arrtibuteSuccess' => $arrtibuteSuccess
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionDeletecategory()
    {
        $category_id = $_POST['category_id'];
        
        $modelCategory = Category::find()->where(['id' => $category_id])->one();
        if($modelCategory->delete()){
            echo json_encode(['status'=>true]);
        }else{
            echo json_encode(['status'=>false]);
        }
    }
    
    public function actionDeleteattribute()
    {
        $attribute_id = $_POST['attribute_id'];
        $modelAttribute = Attribute::find()->where(['id' => $attribute_id])->one();
        
        if($modelAttribute->delete()){
            $result['status'] = 'delete';
        }else{
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }
    
    public function actionAddattribute()
    {
        $modelNewAttribute = new Attribute();
        $modelNewAttribute->scenario = 'add_attribute';
  
        foreach($_POST['data'] as $data){
            switch($data['name']){
                case 'Attribute[name]':
                    $modelNewAttribute->name = $data['value'];
                    $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', ' ', '  ');
                    $lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya', '-', '-');
                    $modelNewAttribute->slug = str_replace($rus, $lat, $data['value']);
                break;
                case 'Attribute[type]':
                    $modelNewAttribute->type = $data['value'];
                break;
                case 'Attribute[category_id]':
                    $modelNewAttribute->category_id = $data['value'];
                break;
                case 'Attribute[data]':
                    $modelNewAttribute->data = $data['value'];
                break;
                case 'Attribute[status_required]':
                    $modelNewAttribute->status_required = $data['value'];
                break;
            }
        }
        if($modelNewAttribute->save()){
            $modelAttribute = Attribute::find()->where(['id' => $modelNewAttribute->id])->asArray()->one();
            $result['data'] = $modelAttribute;
            $result['status'] = 'save';
        }else{
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }
    
    public function actionSavecategoryimg(){
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/img/category/';   //2
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder)){
              mkdir($storeFolder, 0777, true);
            }
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $targetPath =  $storeFolder.$ds;  //4
            $for_name = time();
            $targetFile =  $targetPath.$for_name.'.png';  //5
            move_uploaded_file($tempFile,$targetFile); //6
            return $for_name.'.png';
        }
    }
    
}
