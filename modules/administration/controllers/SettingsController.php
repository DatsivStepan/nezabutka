<?php

namespace app\modules\administration\controllers;

use Yii;
use yii\web\Controller;
use app\modules\administration\models\Settings;

/**
 * Default controller for the `administration` module
 */
class SettingsController extends Controller
{
    public function beforeAction($action) {
        if((!\Yii::$app->user->isGuest) && (Yii::$app->user->identity->type == 'admin')){
//                $this->enableCsrfValidation = false;
                return parent::beforeAction($action);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionIndex()
    {
        if(isset($_POST['update'])){
            $updateSettingsPhone = Settings::find()->where(['key' => 'phone'])->one();
            $updateSettingsEmail = Settings::find()->where(['key' => 'e-mail'])->one();
            $updateSettingsPhone->value=$_POST['phone'];
            $updateSettingsEmail->value=$_POST['e-mail'];
            $updateSettingsPhone->save();
            $updateSettingsEmail->save();
            if($updateSettingsPhone->save()){
                Yii::$app->session->setFlash('updated');
            }else{
                Yii::$app->session->setFlash('not_updated');
            }
        }
        $settings = Settings::find()->all();

        $this->layout = 'adminLayout';
        return $this->render('index', [
            'settings' => $settings,
        ]);
    }

}
