<?php

namespace app\modules\administration\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\administration\models\LoginForm;
use app\modules\administration\models\ContactForm;
use app\models\User;
use app\models\Category;
use app\models\Pages;
use yii\data\ActiveDataProvider;
/**
 * Default controller for the `administration` module
 */
class DefaultController extends Controller
{
    
    public function actionIndex()
    {
        $this->layout = 'adminLayout';
        
        if (!\Yii::$app->user->isGuest){
            return $this->redirect('administration/category/categorylist');
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('administration/category/categorylist');
        }
        
        return $this->render('index', [
            'model' => $model,
        ]);
    }
    
    public function actionPages(){
        $this->layout = 'adminLayout';
        
        $modelPages = Pages::find()->all();
        return $this->render('pages', [
            'modelPages' => $modelPages,
        ]);
    }
    
    public function actionPage($id=null){
        $this->layout = 'adminLayout';
        
        $modelPage = Pages::find()->where(['id' => $id])->one();
        $modelPage->scenario = 'update';
        if($_POST){
            if($modelPage->load(Yii::$app->request->post())){
                if($modelPage->save()){
                    Yii::$app->session->setFlash('update_page');
                    return $this->redirect('/administration/default/pages');
                }else{
                    Yii::$app->session->setFlash('not_update_page');
                    return $this->redirect('/administration/default/pages');
                }
            }
        }
        
        return $this->render('page', [
            'modelPage' => $modelPage,
        ]);
    }
    
}
