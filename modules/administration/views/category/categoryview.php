<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use kartik\select2\Select2;
use app\widgets\controller\CategorynameWidget;
use app\assets\AdminAsset;
AdminAsset::register($this);


if(Yii::$app->session->hasFlash('update_category')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Категория редактирована!',
                ]);
        endif; 
        if(Yii::$app->session->hasFlash('not_update_category')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Категория не редактирована!',
                ]);
        endif; 
        
?>
<div class="dashboard-container">

      <div class="container">
        <div id="cssmenu">
            <?= $this->render('/default/menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Категории'), '/administration',['class'=>'AdminHomePageLink']); ?>
            </li>
          </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
         
          <!-- Row Start -->
            <div class="row wrap" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                            <div class="widget-header" style="border-bottom: 0px">
                                  Редактирование
                            </div>
                            <div class=" widget-body">
                                <div class="row">
                                        <div class="col-sm-12">
                                            <?php $form = ActiveForm::begin(); ?>
                                                <?= $form->field($modelCategory, 'name')->textinput(); ?>
                                                <?= $form->field($modelCategory, 'slug')->textinput(); ?>
                                                <?php if($modelCategory->img_src == ''){ 
                                                    $img_src = 'img/category/default_img.png';
                                                }else{
                                                    $img_src = 'img/category/'.$modelCategory->img_src;
                                                } ?>
                                                <img src="<?= Url::home().$img_src; ?>" action="<?= Url::home(); ?>administration/category/savecategoryimg" class="category_photo" style="width:100px;height:100px;"><br>
                                                <a  class="btn btn-primary newPhoto" >Изменить</a>
                                                <?= $form->field($modelCategory, 'img_src')->hiddeninput()->label(false); ?>
                                                <?= $form->field($modelCategory, 'parent_id')->widget(Select2::classname(), [
                                                        'data' => $categoryArray,
                                                        'options' => ['placeholder' => 'Родительская категория'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]);
                                                ?>
                                                <?= $form->field($modelCategory, 'position')->input('number',['min' => 1]); ?>
                                            <?php // $categoryArray ?>
                                                <div class="col-sm-2" style="padding:0px;">
                                                    <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-info', 'style' => 'box-shadow: 0 0 10px rgba(0,0,0,0.5);']) ?>
                                                </div>
                                            <?php ActiveForm::end(); ?>
                                        </div>                                    
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            
          <!-- Row End -->

        </div>

      </div>
    </div>

