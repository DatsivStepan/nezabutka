<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use kartik\select2\Select2;
use yii\grid\GridView;;
use yii\widgets\Pjax;
use app\widgets\controller\CategorynameWidget;
use app\assets\AdminAsset;
AdminAsset::register($this);

if(Yii::$app->session->hasFlash('add_category')):
        echo Alert::widget([
                'options' => [
                        'class' => 'alert-info',
                ],
                'body' => 'Категория добавлена!',
        ]);
endif; 
if(Yii::$app->session->hasFlash('not_add_category')):
        echo Alert::widget([
                'options' => [
                        'class' => 'alert-error',
                ],
                'body' => 'Категория не добавлена!',
        ]);
endif; 
?>
<div class="dashboard-container">

      <div class="container">
        <div id="cssmenu">
            <?= $this->render('/default/menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Категории'), '/administration',['class'=>'AdminHomePageLink']); ?>
            </li>
          </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
         
          <!-- Row Start -->
            <div class="row wrap" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                          <div class="title" style="height:40px;">
                            <i class="fa fa-arrow-down" data-action="show"> </i> Форма добавления категории
                            <span class="mini-title displayNone">
                                
                            </span>
                          </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(); ?>
                                        <?= $form->field($modelNewCategory, 'name')->textinput(); ?>
                                        <?= $form->field($modelNewCategory, 'slug')->textinput(); ?>
                                        <img src="<?= Url::home(); ?>img/category/default_img.png" action="<?= Url::home(); ?>administration/category/savecategoryimg" class="category_photo" style="width:100px;height:100px;"><br>
                                        <a  class="btn btn-primary newPhoto" >Изменить</a>
                                        <?= $form->field($modelNewCategory, 'img_src')->hiddeninput()->label(false); ?>
                                        <?= $form->field($modelNewCategory, 'parent_id')->widget(Select2::classname(), [
                                                'data' => $categoryArray,
                                                'options' => ['placeholder' => 'Родительская категория'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                        ?>
                                        <?php //= $form->field($modelNewCategory, 'parent_id')->dropDownList($categoryArray,['prompt' => '']); ?>
                                        <?= $form->field($modelNewCategory, 'position')->input('number',['min' => 1]); ?>
                                    <?php // $categoryArray ?>
                                        <div class="col-sm-2" style="padding:0px;">
                                            <?= Html::submitButton(Yii::t('app', 'Добавить'), ['class' => 'btn btn-info', 'style' => 'box-shadow: 0 0 10px rgba(0,0,0,0.5);']) ?>
                                        </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                          <div class="title" style="height:40px;">
                            <i class="fa fa-arrow-down" data-action="show"> </i> Категории
                            <span class="mini-title displayNone">
                                
                            </span>
                          </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <?php Pjax::begin();?>
                                <?= GridView::widget([
                                    'id' => 'category-table',
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'rowOptions' => function ($model) {
                                        return [
                                            'data-category_id' => $model->id,
                                            'class' => 'categoryId' . $model->id,
                                            'style' => 'white-space: normal !important;'];
                                    },
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        'id',
                                        [
                                            'label' => 'Full name',
                                            'attribute' => 'categoryFullName',
                                            'format' => 'raw',
                                            'value' => function ($model) {
                                                return ($model->categorynamemodel) ?  $model->categorynamemodel->name : '';
                                            }
                                        ],
                                        'name',
                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '{update} {delete}',
                                            'buttons' => [
                                                'delete' => function ($url, $model) {
                                                    return Html::button(
                                                        '<i class="glyphicon glyphicon-trash"></i>',
                                                        ['data-pjax' => 0, 'data-id' => $model->id, 'class' => 'deleteCategory btn btn-sm btn-danger']
                                                    );
                                                },
                                                'update' => function ($url, $model) {
                                                    return Html::a(
                                                        '<i class="glyphicon glyphicon-pencil"></i>',
                                                        ['/administration/category/categoryupdate/', 'id' => $model->id],
                                                        ['data-pjax' => 0, 'class' => 'btn btn-sm btn-primary']
                                                    );
                                                }
                                            ],
                                        ],
                                    ],
                                ]); ?>
                                <?php Pjax::end();?>
                                <?php /* ?>
                                <div class="col-sm-12">
                                    <table class="table">
                                        <tr>
                                            <td>#</td>
                                            <td>Name</td>
                                            <td>Дата создания</td>
                                            <td>Позиция</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <?php function setCategory($categories){ ?>
                                            <?php foreach($categories as $category){ ?>
                                                <tr>
                                                    <td><?= $category->id; ?></td>
                                                    <td><?= CategorynameWidget::widget(['category_id' =>  $category->id]) ?></td>
                                                    <td><?= $category->date_create; ?></td>
                                                    <td><?= $category->position; ?></td>
                                                    <td>
                                                        <a href="<?= Url::home(); ?>administration/category/categoryupdate/<?= $category->id; ?>" >
                                                            <i class="fa fa-edit fa-2x"></i>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <i style="cursor:pointer;" data-id="<?= $category->id; ?>" class="deleteCategory fa fa-times fa-2x"></i>
                                                    </td>
                                                </tr>
                                                <?php  if($category->childcateogory){ ?>
                                                    <?php setCategory($category->childcateogory); ?> 
                                                <?php  } ?>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php setCategory($modelCategoryAll); ?>        
                                        
                                    </table>

                                    <div class='col-sm-12'>
                                        <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                                    </div>
                                </div>
                                <?php */ ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
            </div>
            
          <!-- Row End -->

        </div>

      </div>
    </div>

