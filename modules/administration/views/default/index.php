<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="site-login">
    <div class="style_administration">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="head_administratin"><g> Пожалуйста, заполните следующие поля для входа:</g></div>
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
            'labelOptions' => ['class' => ' control-label text_administration'],
        ],
    ]); ?>


        <?= $form->field($model, 'username')->textInput(['class' =>'form-control form_width_administration'], ['autofocus' => true])->label('Логин') ?>

        <?= $form->field($model, 'password')->passwordInput(['class' =>'form-control form_width_administration'])->label('Пароль') ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\" col-lg-12\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
        ])->label('запомнинь меня') ?>

        <div class="form-group">
            <div class=" administration_login col-lg-12">
                <?= Html::submitButton('Авторизоваться', ['class' => 'button_administration', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
