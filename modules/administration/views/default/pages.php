<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\assets\AdminAsset;
AdminAsset::register($this);

    if(Yii::$app->session->hasFlash('update_page')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Страница сохранена',
        ]);
    endif;
if(Yii::$app->session->hasFlash('not_update_page')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Ошибка!',
    ]);
endif;
?>
<div class="dashboard-container">
    <div class="container">
        <div id="cssmenu">
            <?= $this->render('/default/menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
            <ul>
                <li>
                    <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i></a><a style="font-size:15px;padding:0px;">Категории'), '/administration',['class'=>'AdminHomePageLink']); ?>
                </li>
            </ul>
        </div>
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa fa-arrow-down" data-action="show"> </i> Настройки
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                        <table class="table">
                                            <tr>
                                                <td><b>#</b></td>
                                                <td><b>Страница</b></td>
                                                <td><b>Сортировка</b></td>
                                                <td></td>
                                            </tr>
                                            <?php foreach($modelPages as $page){ ?>
                                                <tr>
                                                    <td><?= $page['id']; ?></td>
                                                    <td><?= $page['name']; ?></td>
                                                    <td><?= $page['sort']; ?></td>
                                                    <td><a href="<?= Url::home(); ?>administration/default/page/<?= $page['id']; ?>" class="btn btn-primary">Update</a></td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>