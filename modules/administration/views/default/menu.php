<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>
<?php
    $class = ($this->context->getRoute() == 'administration/category/categorylist')?'active':'';
    $class2 = ($this->context->getRoute() == '#')?'active':'';
    $class3 = ($this->context->getRoute() == 'administration/settings/index')?'active':'';
    $class4 = ($this->context->getRoute() == 'administration/default/pages')?'active':'';
    $class5 = ($this->context->getRoute() == 'administration/metro/index')?'active':'';
?>

<ul>
    <li class="<?= $class; ?>">
        <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-list"></i>Категории'), '/administration/category/categorylist'); ?>
    </li>
    
    <?php /* ?>
    <li class="<?= $class2; ?>">
        <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-file-text-o"></i>Объявление'), '#'); ?>
    </li>
    <?php */ ?>
    
    <li class="<?= $class3; ?>">
        <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-cogs"></i>Настройки'), '/administration/settings/'); ?>
    </li>
    <li class="<?= $class4; ?>">
        <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-cogs"></i>Страници'), '/administration/default/pages'); ?>
    </li>
    <li class="<?= $class5; ?>">
        <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-cogs"></i>Метро'), '/administration/metro/index'); ?>
    </li>
</ul>
            
            
            
