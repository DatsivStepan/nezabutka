<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\assets\AdminAsset;
AdminAsset::register($this);

    if(Yii::$app->session->hasFlash('updated')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Настройки изменены',
        ]);
    endif;
if(Yii::$app->session->hasFlash('not_updated')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'настройки не изменены',
    ]);
endif;
?>
<div class="dashboard-container">
    <div class="container">
        <div id="cssmenu">
            <?= $this->render('/default/menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
            <ul>
                <li>
                    <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i></a><a style="font-size:15px;padding:0px;">Категории'), '/administration',['class'=>'AdminHomePageLink']); ?>
                </li>
            </ul>
        </div>
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa fa-arrow-down" data-action="show"> </i> Настройки
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(); ?>
                                    <?php
                                        foreach($settings as $value){
                                    ?>
                                    <label><?php echo $value['key'];?></label><br>
                                    <input type="text" class="form-control" name="<?php echo $value['key']; ?>" value="<?php echo $value['value'];?>"><br>
                                    <?php } ?>
                                    <?= Html::submitButton(Yii::t('app', 'Изменить'), ['name'=>'update', 'class' => 'btn btn-info settingUpdate', 'style' => 'box-shadow: 0 0 10px rgba(0,0,0,0.5);']) ?>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

