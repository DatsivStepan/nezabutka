<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use app\widgets\GetcitynameWidget;
?>
<?php
if (Yii::$app->session->hasFlash('add_metro')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Станция добавлена!',
    ]);
endif;
if (Yii::$app->session->hasFlash('not_add_metro')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Проблема!',
    ]);
endif;

if (Yii::$app->session->hasFlash('metro_delete')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'metro deleted!',
    ]);
endif;
if (Yii::$app->session->hasFlash('brand_not_delete')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Brand not deleted!',
    ]);
endif;
?>

<div class="dashboard-container">
    <div class="container">
        <div id="cssmenu">
            <?= $this->render('/default/menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Метро'), '/administration',['class'=>'AdminHomePageLink']); ?>
            </li>
          </ul>
        </div>
        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-12 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Добавить станцию
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(); ?>
                                            <?= $form->field($modelNewMetro, 'name')->textinput(); ?>
                                            <div class="form-group field-metro-city_id required ">
                                                    <label class="control-label" for="metro-city_id">Город</label>
                                                    <input type="text" name="city_name" AUTOCOMPLETE="off" class="form-control form_text">

                                                    <div style="position:relative;">
                                                        <div style="position:absolute;top:0px;background-color:white;width:100%;z-index:99;">
                                                            <ul class="appProductChoiseCity">
                                                            </ul>
                                                        </div>                                
                                                    </div>

                                                    <?= $form->field($modelNewMetro, 'city_id')->hiddenInput()->label(false); ?>
                                            </div>
                                            
                                        <?= Html::submitButton(Yii::t('app', 'Добавить'), ['class' => 'btn btn-primary pull-right']) ?>
                                    <?php ActiveForm::end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa" data-action="show"> </i> Станции
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelMetro,
                                        'columns' => [
                                            'id',
                                            'name',
                                            [
                                                'attribute' => 'City',
                                                'format' => 'html',
                                                'value' => function ($modelMetro) {                                                    
                                                    return GetcitynameWidget::widget(['city_id' => $modelMetro['city_id']]);
                                                }
                                            ],
                                            
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{delete} ',
                                                'buttons' => [
                                                    'delete' => function ($url, $modelMetro) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-trash"></span>',
                                                            'metrodelete?id=' . $modelMetro['id']);
                                                    },
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>