<?php

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'roket.kaktus.team@gmail.com',
    'user.passwordResetTokenExpire' => '259200',
];
