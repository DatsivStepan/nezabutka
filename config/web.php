<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'name' => 'Незабудка',
    'timeZone' => 'Europe/Moscow', 
    'modules' => [
        'administration' => [
            'class' => 'app\modules\administration\Module',
        ],
        'profile' => [
            'class' => 'app\modules\profile\Module',
        ],
        'gridview' => ['class' => 'kartik\grid\Module']
    ],
    'components' => [
        'request' => [
        	'baseUrl' => '',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'KIPpz30dWlEb9Km_jokPce0-IU4i6-YE',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
              'class' => 'Swift_SmtpTransport',
              'host' => 'smtp.gmail.com',
              'username' => 'roket.kaktus.team@gmail.com',
              'password' => 'datsivkn46',
              'port' => '587',
              'encryption' => 'tls',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules' => [
                ''                                 => 'site/index',
                'page/<urlname:\w+>'                    => 'site/page',
                'index'                            => 'site/index',
                'about'                            => 'site/about',
                'login'                            => 'site/login',
                'signup'                           => 'site/signup',
                'addnew'                           => 'site/addnew',
                'contact'                          => 'site/contact',
                'getchildcategory'                 => 'category/getchildcategory',
                'addproduct'                       => 'product/addproduct',
                'editproduct/<product_id:\d+>'                       => 'product/editproduct',
                
                'administration/category/categoryupdate/<id:\d+>' => 'administration/category/categoryupdate',
                
                'categories/<category_slug>'   => 'category/index',
                'category/<category_slug>'     => 'category/category',
                'product/getproducts'                 => 'product/getproducts',
                'product/saveproductimg'                 => 'product/saveproductimg',
                'product/deleteproduct'                 => 'product/deleteproduct',
                'product/productfavorite'                 => 'product/productfavorite',
                'product/<id:\w+>'                 => 'product/product',
                'subcategory/<category_slug:\w+>'  => 'category/subcategory',
                'administration/default/page/<id:\d+>' => 'administration/default/page',
                'administration/default/page/<id:\d+>' => 'administration/default/page',
                'seller/<id:\d+>'                 => 'profile/default/seller',
                'profile/<id:\d+>'                 => 'profile/default/index',
                'profile/<id:\d+>/message'         => 'profile/default/message',
                'profile/<id:\d+>/setting'         => 'profile/default/setting',
                'profile/<id:\d+>/sold'         => 'profile/default/sold',
                'profile/<id:\d+>/soldout'         => 'profile/default/soldout',
                'profile/<id:\d+>/archive'         => 'profile/default/archive',
                'profile/<id:\d+>/favorite'         => 'profile/default/favorite',
                'profile/<id:\d+>/frozen'         => 'profile/default/frozen',
                'profilecont'                      => 'profile/default/profile',
                'profilepass'                      => 'profile/default/profilepass',
                'profileemail'                     => 'profile/default/profileemail',
                'profiledelete'                    => 'profile/default/profiledelete',
            ],
        ],
        
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
