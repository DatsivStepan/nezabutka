<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>

<div class="password-reset">
    <div style="margin:10px;">
        <img src="http://nezabutka.roketdev.pro/img/logo_all.png">
    </div>
    <h2 style="margin:0px;font-weight:400;">Сбросить пароль</h2>
    
    <p>Здравствуйте <?= Html::encode($user->username) ?>!</p>

    <p>Перейдите по ссылке ниже, чтобы сбросить пароль:</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
