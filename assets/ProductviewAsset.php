<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ProductviewAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    	'/js/flex/css/flexslider.css',
    	'js/fresco/css/fresco/fresco.css'
    ];
    public $js = [
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyDcBLaan_dFaE2Nxr9HkJwZJOSHYUeKo4c&libraries=places',
        'js/fresco/js/fresco.js',
        'js/flex/js/jquery.easing.js',
        'js/flex/js/jquery.mousewheel.js',
        'js/flex/js/jquery.flexslider.js',
        'js/productview.js',
        'js/print.js'

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',#REMOVE
    ];
}