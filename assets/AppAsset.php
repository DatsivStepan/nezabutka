<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap-modal.css',
        'css/bootstrap-multiselect.css',
        'css/font-awesome.css',
        'css/sweetalert.css',
        'css/site.css',
        'css/fonts.css',
        'css/main.css',
        'css/media.css',
        "https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css",
    ];
    public $js = [
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyDcBLaan_dFaE2Nxr9HkJwZJOSHYUeKo4c&libraries=places&language=ru',
        'js/bootstrap-multiselect.js',
        'js/bootstrap-manager.js',
        'js/bootstrap-modal.js',
        'js/jquery/jquery-cookie.js',
        'js/modernizr/modernizr.js',
        'js/html5shiv/es5-shim.min.js',
        'js/html5shiv/html5shiv-printshiv.min.js',
        'js/html5shiv/html5shiv.min.js',
        'js/respond/respond.min.js',
        'js/plugins-scroll/plugins-scroll.js',
//        'js/formstyler/jquery.formstyler.min.js',
        'js/magnific/jquery.magnific-popup.js',
        'js/maskedinput.js',
        'js/common.js',
        'js/sweetalert.js',
        'js/contactForm.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',#REMOVE
    ];
}