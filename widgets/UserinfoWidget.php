<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\User;

class UserinfoWidget extends Widget{
    public $user_id;
    public $date;
    
    public function init(){
        parent::init();
    }

    public function run(){
        $modelUser = User::find()->where(['id' => $this->user_id])->asArray()->one();
        return $this->render('userinfo',[
            'modelUser' => $modelUser,
            'date' => $this->date
        ]);
    }
    
}
?>
