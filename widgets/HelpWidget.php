<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class HelpWidget extends Widget{
 
    public function init(){
        parent::init();
    }

    public function run(){
        return $this->render('help',[
        ]);
    }
    
}
?>
