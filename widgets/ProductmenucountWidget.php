<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Product;
use app\models\Favorite;

class ProductmenucountWidget extends Widget{
    public $product_status;
    
    public function init(){
        parent::init();
    }

    public function run(){
        if($this->product_status == 'favorite'){
            
            $modelFavorite = Favorite::find()->where(['user_id' => \Yii::$app->user->id])->asArray()->all();
            $product_id = [];
            foreach($modelFavorite as $favorite){
                $product_id[] = $favorite['product_id'];
            }
            $countProduct = Product::find()->where(['status' => '1','id' => $product_id])->count();
            
        }else{
            $countProduct = Product::find()->where(['status' => $this->product_status, 'user_id' => \Yii::$app->user->id])->count();
        }
        
        return $countProduct;
    }
    
}
?>
