<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Product;

class ProductcountWidget extends Widget{
 
    public function init(){
        parent::init();
    }

    public function run(){
        $countCategories = Product::find()->where(['status' => 1])->count();
        $res = (string)$countCategories;
        return $this->render('count',[
            'res' => $res
        ]);
    }
    
}
?>
