<?php
namespace app\widgets\controller;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Category;

class CategorynameWidget extends Widget{
    public $category_id;
    public $name = '';

    public function init(){
        parent::init();
    }
    
    public function getCategoryName($category_id){
        
        $modelCategory = Category::find()->where(['id' => $category_id])->one();
        if($modelCategory != null){
            if($this->name != ''){
                $this->name = $modelCategory->name.' -> '.$this->name;                
            }else{
                $this->name = $modelCategory->name;
            }
            if($modelCategory->parent_id != 0){
                $this->getCategoryName($modelCategory->parent_id);                
            }
        }
        return $this->name;
    }
    
    public function run(){
        $name = $this->getCategoryName($this->category_id);
        return $name;        
    }
    
}
?>