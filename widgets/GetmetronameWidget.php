<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Metro;

class GetmetronameWidget extends Widget{
    public $metro_id;
    
    public function init(){
        parent::init();
    }

    public function run(){
        $modeMetro = Metro::find()->where(['id' => $this->metro_id])->asArray()->one();
        return $modeMetro['name'];
    }
    
}
?>
