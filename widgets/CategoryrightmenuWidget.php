<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Category;

class CategoryrightmenuWidget extends Widget{
 
    public function init(){
        parent::init();
    }

    public function run(){
        $modelCategories = Category::find()->where(['parent_id' => 0])->orderBy('position ASC')->all();
//        $categoryArray = [];
//        foreach($modelCategories as $category){
//            $categoryArray[$category['id']]['info'] = $category;
//            $categoryArray[$category['id']]['child'] = Category::find()->where(['parent_id' => $category['id']])->count();
//        }
        return $this->render('categorylist',[
            'modelCategories' => $modelCategories
        ]);
    }
    
}
?>
