<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\City;

class GetcitynameWidget extends Widget{
    public $city_id;
    
    public function init(){
        parent::init();
    }

    public function run(){
        $modelCity = City::find()->where(['city_id' => $this->city_id])->asArray()->one();
        return $modelCity['name'];
    }
    
}
?>
