<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php if($modelCategories){ ?>
    <ul class="rightBlockMenu">
        <li>
            <a href="<?= Url::home(); ?>">Все объявления</a>
        </li>
        <?php foreach($modelCategories as $category){ ?>
                <?php if($category->getProductcount()){ ?>
                    <li class="eHover" data-category_id="<?= $category->id; ?>" data-category_slug="<?= $category->slug; ?>">
                        <a href="<?= Url::home(); ?>category/<?= $category->slug; ?>" ><span><?= $category->name; ?></span></a>
                        <ul class="submenuFirst" style="padding-left:10px;"></ul>
                    </li>
                <?php }else{ ?>
                    <li data-category_id="<?= $category->id; ?>" data-category_slug="<?= $category->slug; ?>">
                        <a href="<?= Url::home(); ?>category/<?= $category->slug; ?>"><?= $category->name; ?></a>
                    </li>
                <?php } ?>
        <?php } ?>
    </ul>
<?php } ?>