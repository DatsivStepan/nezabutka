<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


?>
<div class="col-sm-12 padding_user">
    <div class="col-xs-5 col-sm-5 col-md-6 col-lg-5 img_message">
        <?php
            $image_src = Url::home().'img/users/default_avatar.jpg';
            if($modelUser['avatar'] != ''){
                $image_src = Url::home().$modelUser['avatar'];
            }
        ?>
        <img  src="<?= $image_src; ?>">
    </div>
    <div class=" col-xs-7 col-sm-6  col-lg-7 class_info_user user_message_style" data-user-id="<?= $modelUser['id']; ?>">
        <?php if($modelUser['contactname'] != ''){ ?>
            <?= $modelUser['contactname']; ?>            
        <?php }else{ ?>
            <?= $modelUser['username']; ?>            
        <?php } ?>
        <div class="status_online">
            <a></a>
        </div>
        <div class="date">
            <?php
            $date = date_create($date);
            $month_array = array("01"=>"января","02"=>"февраля","03"=>"марта","04"=>"апреля","05"=>"мая", "06"=>"июня", "07"=>"июля","08"=>"августа","09"=>"сентября","10"=>"октября","11"=>"ноября","12"=>"декабря");
            ?>
            <span> <?= date ( ' h:i '); ?>  <?= (int)date_format($date, 'd '); ?> <?= $month_array[date_format($date, 'm')]; ?>, <?= date_format($date, 'Y') ?></span>
        </div>
    </div>
</div>