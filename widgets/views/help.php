<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


?>
<div class="col-xs-12">
    <div class="loader-help"></div>
    <div class="hidd-help">
        <i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i>
    </div>
    <div  class="help_user_glavn">
        <div class="help_user_img">
            <img src="../../web/img/helpuser.png">
        </div>
        <?php $form = ActiveForm::begin(['options' => [
                    'class' => 'SendHelpToEmail'
                ]
            ]); ?>
            <a>Помощь онлайн</a>
            <input type="email" name="help_email" class="form-control help_form" placeholder="Ваш e-mail:" required="required">
            <textarea type="text" name="help_question" rows="6"  class="form-control help_form" placeholder="Ваш вопрос:" required="required"></textarea>
            <?= Html::submitButton('Отправить') ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>