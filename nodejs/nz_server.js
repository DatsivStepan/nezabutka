var app = require('express')();
var fs = require('fs');
var options = {
    key: fs.readFileSync('./keys/private/ca.key'),
    cert: fs.readFileSync('./keys/certs/ca.crt'),
    requestCert: false,
    rejectUnauthorized: false
};
var server = require('https').Server(options, app),
    io = require('socket.io').listen(server),
    chatController = require('./controllers/chat');

var users = {},
    filaments = [];


server.listen(2877, function () {
    console.info('listening on *:2877');
});

app.get('/', function (req, res) {
    res.send('Hello');
});

var users = {},
    filaments = [];
io.on('connection', function (socket) {
    console.log(socket.id + ': user connected');
    socket.on('login', function (user) {
        var promise = new Promise(function(resolve, reject) {
            var user_name = Object.keys(users);
            var tmp_user = '';
            if (user_name.indexOf(user.toString()) != -1) {
                if (Array.isArray(users[user])) {
                    users[user].push(socket);
                } else {
                    tmp_user = users[user];
                    users[user] = [];
                    users[user].push(tmp_user, socket);
                }
            } else {
                users[user] = socket;
            }
            resolve(users);
        });
        promise.then(function (data) {
            console.log(Object.keys(data));
            socket.emit('onlineChange', Object.keys(data));
            socket.broadcast.emit('onlineChange', Object.keys(data));
            chatController.allUnread(user, socket);
        });
        filaments[socket.id] = user;

    });

    socket.on('getHistory', function (group_id, userId) {
        chatController.getHistory(group_id, socket);
        chatController.setReaded(group_id, userId, socket);
        console.log('getHistory');
    });

    socket.on('readedMsg', function(group_id, userId){
        chatController.setReaded(group_id, userId, socket);
        console.log('readedMsg');
    })

    socket.on('sendMassage', function (msg) {
        chatController.addMessage(msg, users);
        console.log('sendMassage');
    });

    socket.on('checkStatus', function (id) {
        socket.emit('reciveStatus', Object.keys(users).indexOf(String(id)))
        console.log('checkStatus');
    })

    socket.on('allUnread', function(user){
        chatController.allUnread(user, socket);
        console.log('allUnread');
    })

    socket.on('disconnect', function () {
        var id = filaments[socket.id],
            index = -1;
        if (Array.isArray(users[id])) {
            index = users[id].indexOf(socket);
            users[id].splice(index, 1);
            if (users[id].length === 0) {
                delete users[id];
                delete filaments[socket.id];
                socket.broadcast.emit('onlineChange', {'user': id, status: 0});
            }
        } else {
            delete users[id];
            delete filaments[socket.id];

            socket.broadcast.emit('onlineChange', Object.keys(users));
        }

        console.log(socket.id + ': user disconnected');
    });
});
