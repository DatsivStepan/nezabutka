var mysql = require('mysql'),
    mysqlUtilities = require('mysql-utilities'),
    db_config = require("../db/config");

var pool = mysql.createPool({
    database: db_config.database,
    host: db_config.host,
    user: db_config.user,
    password: db_config.password,
    connectionLimit: db_config.connectionLimit
});

exports.allUnreadMsg = function (user, callback) {
    pool.getConnection(function(err, connection){
        if(err) throw err;
        mysqlUtilities.upgrade(connection);
        mysqlUtilities.introspection(connection);
        connection.queryRow('SELECT COUNT(*) AS unread FROM `message` WHERE status = 0 AND `recipient_id` = ' + user, function (err, result) {
            callback(err, result);
        });
        connection.release();
    })
};

exports.allUnreadTitle = function (user, callback) {
    pool.getConnection(function(err, connection){
        if(err) throw err;
        mysqlUtilities.upgrade(connection);
        mysqlUtilities.introspection(connection);
        connection.query("SELECT DISTINCT mg.id, (SELECT count(*) FROM message m2 WHERE mg.id = m2.group_id AND m2.status = 0 AND m2.recipient_id = " + user + ") AS count FROM message_group mg LEFT JOIN message m ON mg.id = m.group_id WHERE m.status = 0 AND m.recipient_id = " + user, function (err, result) {
            callback(err, result);
        });
        connection.release();
    })
};

exports.getHistory = function (group, callback) {
    pool.getConnection(function(err, connection){
        if(err) throw err;
        mysqlUtilities.upgrade(connection);
        mysqlUtilities.introspection(connection);
        connection.query('SELECT * FROM `message` WHERE `group_id` = ' + group + ' ORDER BY date_create DESC', function (err, result) {
            callback(err, result);
        });
        connection.release();
    })
};

exports.addMessage = function (messageObj, callback) {
    pool.getConnection(function(err, connection){
        if(err) throw err;
        mysqlUtilities.upgrade(connection);
        mysqlUtilities.introspection(connection);

        connection.insert('message', messageObj, function (err, recordId) {
            callback(err, recordId);
        });
        connection.release();
    })
};

exports.updateGroup = function (groupId, dateVal, callback) {
    pool.getConnection(function(err, connection){
        if(err) throw err;
        mysqlUtilities.upgrade(connection);
        mysqlUtilities.introspection(connection);

        connection.query("UPDATE `nezabutka`.`message_group` SET `date` = '" + dateVal + "' WHERE `message_group`.`id` = " + groupId, function (err, affectedRows) {
            callback(err, affectedRows);
        });
        connection.release();
    })
};

exports.setReaded = function (groupId, userId, callback) {
    pool.getConnection(function(err, connection){
        if(err) throw err;
        mysqlUtilities.upgrade(connection);
        mysqlUtilities.introspection(connection);

        connection.query("UPDATE `nezabutka`.`message` SET `status` = 1 WHERE `message`.`recipient_id` = " + userId + " AND group_id = " + groupId, function (err, affectedRows) {
            callback(err, affectedRows);
        });
        connection.release();
    })
}
exports.getLastMessages = function (groupId, userId, callback) {
    pool.getConnection(function(err, connection){
        if(err) throw err;
        mysqlUtilities.upgrade(connection);
        mysqlUtilities.introspection(connection);

        connection.query("UPDATE `nezabutka`.`message` SET `status` = 1 WHERE `message`.`recipient_id` = " + userId + " AND group_id = " + groupId, function (err, affectedRows) {
            callback(err, affectedRows);
        });
        connection.release();
    })
}
