var chatModel = require('../models/chat');
var moment = require('moment');

exports.allUnread = function (user, socket) {
    var unreadObj = {
        msg_count: 0,
        title_id: null
    };
    chatModel.allUnreadMsg(user, function(err, result){
        if(err) throw err;
        unreadObj.msg_count = result.unread;

        chatModel.allUnreadTitle(user, function(err, result){
            if(err) throw err;
            console.log(result);
            unreadObj.title_count = result;
            socket.emit('unreadCount', unreadObj);
        })
    })
};

exports.getHistory = function(group, socket){
    chatModel.getHistory(group, function (err, results) {
        if(err) throw err;
        socket.emit('history', results);
    })
};

exports.addMessage = function (messageObj, users) {
    messageObj.date_create = moment().format('YYYY-MM-DD HH:mm:ss');
    chatModel.addMessage(messageObj, function (err, recorgId) {
        if(err) throw err;
        chatModel.updateGroup(messageObj.group_id, moment().format('YYYY-MM-DD HH:mm:ss'), function (err, affectedRows) {
            if(err) throw err;
            if(users[messageObj.recipient_id]) {
                if (!Array.isArray(users[messageObj.recipient_id])) {
                    users[messageObj.recipient_id].emit('give_message', messageObj);
                } else {
                    for (var i = 0; i < users[messageObj.recipient_id].length; i++) {
                        users[messageObj.recipient_id][i].emit('give_message', messageObj);
                    }
                }
            }
        })
    })
};

exports.setReaded = function(group, user, socket){
    var self = this;
    console.log("group: ", group);
    if(group >= 1 && user >= 1){
        chatModel.setReaded(group, user, function (err, affectedRows) {
            if(err) throw err;
            self.allUnread(user, socket);
        })
    }
}
