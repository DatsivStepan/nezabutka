<?php

use yii\db\Migration,
    app\models\Category,
    app\models\CategoryName;

/**
 * Class m171009_152749_add_category_name_table
 */
class m171009_152749_add_category_name_table extends Migration
{
    public $fullName = '';
    
    public function safeUp()
    {
        $modelCategories = Category::find()->all();
        foreach($modelCategories as $category){
            $modelCategoryname = CategoryName::find()->where(['category_id' => $category->id])->one();
            if(!$modelCategoryname){
                $modelCategoryname = new CategoryName();
                $modelCategoryname->category_id = $category->id;
            }
            $this->fullName = '';
            $modelCategoryname->name = $this->CategoryName($category->id);
            $modelCategoryname->save();
        }
    }

    public function safeDown()
    {
        echo "m171008_083246_change_link_to_report_prawa_jazdy_in_sprawozdania cannot be reverted.\n";

        return false;
    }
    
    public function CategoryName($category_id){
        $modelCategory = Category::find()->where(['id' => $category_id])->one();
        if($modelCategory != null){
            if($this->fullName != ''){
                $this->fullName = $modelCategory->name.' -> '.$this->fullName;                
            }else{
                $this->fullName = $modelCategory->name;
            }
            if($modelCategory->parent_id != 0){
                $this->CategoryName($modelCategory->parent_id);                
            }
        }
        return $this->fullName;
    }
    
}