<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;
//status: 1 - продается, 2 - Продано, 3 - в архиве, 4 - заморожено
class Message extends \yii\db\ActiveRecord
{
    public $title;
    
    public static function tableName()
    {
        return '{{%message}}';
    }

    public function scenarios()
    {
        return [
            'add_message' => ['group_id', 'sender_id', 'recipient_id', 'text', 'status'],
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
        ];
    }

}
