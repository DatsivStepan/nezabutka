<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%category_name}}".
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 */
class CategoryName extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category_name}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'category_id'], 'integer'],
            [['name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
    
    public function scenarios()
    {
        return [
            'default' => [],
        ];
    }
    
}
