<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\models\CategoryName;
use yii\helpers\Url;

class Category extends \yii\db\ActiveRecord
{
    public $category_name_all;
    
    public static function tableName()
    {
        return '{{%category}}';
    }

    public function scenarios()
    {
        return [
            'add_category' => ['name', 'slug', 'parent_id','img_src', 'position'],
            'default' => ['name', 'slug', 'parent_id','img_src', 'position']
        ];
    }
    
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class'         => 'app\behaviors\Slug',
                'in_attribute'  => 'name',
                'out_attribute' => 'slug',
                'translit'      => true
            ]
        ];
    }
    
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['slug'], 'unique'],
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function getImage()
    {
        return $this->img_src ? '/img/category/'.$this->img_src : null;
    }

    public function getChildcateogory()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'id'])->orderBy('name ASC');
    }
    
    public function getCategorynamemodel()
    {
        return $this->hasOne(CategoryName::className(), ['category_id' => 'id']);
    }
    
    public function getProductcount()
    {
        return Productcategory::find()->joinWith('product')->where(['product_category.category_id' => $this->id, 'product.status' => '1'])->count();
    }
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
            'name' => Yii::t('app', 'Название'),
            'slug' => Yii::t('app', 'Артикул'),
            'parent_id' => Yii::t('app', 'Родительская категория'),
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveCategoryName();
    }
    
    public function saveCategoryName(){
        $modelCategoryname = $this->categorynamemodel;
        if(!$modelCategoryname){
            $modelCategoryname = new CategoryName();
            $modelCategoryname->category_id = $this->id;
        }

        $modelCategoryname->name = $this->СategoryName($this->id);
        $modelCategoryname->save();
    }
    
    public function СategoryName($category_id){
        $modelCategory = Category::find()->where(['id' => $category_id])->one();
        if($modelCategory != null){
            if($this->category_name_all != ''){
                $this->category_name_all = $modelCategory->name.' -> '.$this->category_name_all;                
            }else{
                $this->category_name_all = $modelCategory->name;
            }
            if($modelCategory->parent_id != 0){
                $this->СategoryName($modelCategory->parent_id);                
            }
        }
        return $this->category_name_all;
    }

}
