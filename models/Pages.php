<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

//status: 1 - продается, 2 - Продано, 3 - в архиве
class Pages extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%pages}}';
    }

    public function scenarios()
    {
        return [
            'update' => ['name', 'content', 'sort'],
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
        ];
    }

}
