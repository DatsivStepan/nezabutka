<?php
namespace app\models;
use Yii;
/**
 * This is the model class for table "profile".
 *
 * @property integer $user_id
 * @property string $avatar
 * @property string $first_name
 * @property string $second_name
 * @property string $middle_name
 * @property integer $birthday
 * @property integer $gender
 *
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }
   
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    public function updateProfile($date)
    {
       
        $profile              = ($profile = Profile::findOne(Yii::$app->user->id));
        $profile->id          = Yii::$app->user->id;
        if($date['city'])       { $profile->city        = $date['city'];}
        if($date['contactname']){ $profile->contactname = $date['contactname'];}
        if($date['telefone'])   { $profile->telefone    = $date['telefone'];}
        if($date['email'])      { $profile->email       = $date['email'];}
        if($date['password2'])  { $profile->setPassword($date['password2']);}
        
        if($profile->save()):
          return true;
        endif;
        return false;
         
    }
     public function deleteProfile()
    {
       
        $profile              = ($profile = Profile::findOne(Yii::$app->user->id));
        $profile->id          = Yii::$app->user->id;
        
        
        if($profile->delete()):
          return true;
        endif;
        return false;
         
    }
}