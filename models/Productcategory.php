<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use app\models\Product;

class Productcategory extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%product_category}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'add_product_category' => ['category_id', 'product_id'],
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }
    

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
        ];
    }
    
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

}
