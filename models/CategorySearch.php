<?php

namespace app\models;

use yii\data\ActiveDataProvider;
use app\models\Category;
/**
 * ReportDriverLicensesSearch represents the model behind the search form about `app\models\client\Cases`.
 */
class CategorySearch extends \app\models\Category
{
    public $categoryFullName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['id', 'parent_id'],
                'integer'
            ],
            [
                ['name', 'categoryFullName', 'slug', 'position', 'img_src'],
                'string',
            ],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Category::find()
        ->alias('category')
        ->joinWith(['categorynamemodel categoryName']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'categoryFullName' => [
                        'asc' => ['categoryName.name' => SORT_ASC],
                        'desc' => ['categoryName.name' => SORT_DESC],
                    ],
                    'name',
                    
                ],
            ],
        ]);
        
        if (!$this->validate()) {
//            $query->andWhere('0 = 1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', "category.name", $this->name]);
//            ->andFilterWhere(['like', "CONCAT(agent.name, ' ', agent.surname)", $this->agentName])
//            ->andFilterWhere(['like', "client.code", $this->clientCode]);

        return $dataProvider;
    }
}
