<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;
/*
 * type = [0 => inputText, 1 => 'textarea', 2 => radio, 3 => select, 4 => checkbox]
 * status_required = [0 => not_required, 1 => required]
 * 
*/
class Attribute extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return '{{%attribute}}';
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function scenarios()
    {
        return [
            'add_attribute' => ['name', 'category_id', 'type', 'slug', 'data','status_required'],
        ];
    }
    
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
        ];
    }
    

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
            'name' => Yii::t('app', 'Название'),
            'type' => Yii::t('app', 'Тип атрибута'),
            'status_required' => Yii::t('app', 'Обезательность для заполнения?'),
        ];
    }

}
