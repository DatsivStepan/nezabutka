<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

class Messagegroup extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%message_group}}';
    }

    public function scenarios()
    {
        return [
            'add_group_message' => ['sender_id', 'recipient_id', 'title'],
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord){
            $this->date = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
        ];
    }

}
