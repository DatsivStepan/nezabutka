<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use app\models\Product;
//status: 1 - головная картинка
class Productimages extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%product_images}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'add_product_image' => ['img_src', 'product_id', 'status'],
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }
    

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
        ];
    }
}
