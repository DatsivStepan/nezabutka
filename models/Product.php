<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;
//status: 1 - продается, 2 - Продано, 3 - в архиве, 4 - заморожено
class Product extends \yii\db\ActiveRecord
{
    public $verifyCode;
    public $reCaptcha;
    public $category_id_f;
    public $category_id_l;
 
    public static function tableName()
    {
        return '{{%product}}';
    }

    public function scenarios()
    {
        return [
            'add_product' => ['title', 'metro_id', 'reCaptcha', 'about', 'price', 'whatsapp', 'viber', 'img_src', 'city_id',  'category_id', 'mobile_number', 'user_id'],
            'update_image' => ['img_src'],
            'update_status' => ['status'],
        ];
    }
    
    public function rules()
    {
        return [
            ['title', 'required', 'message' => 'Пожалуйста, введите Заголовок обьявления'],
            ['about', 'required', 'message' => 'Пожалуйста, введите Текст обьявления'],
            ['price', 'required', 'message' => 'Пожалуйста, укажите цену'],
            ['city_id', 'required', 'message' => 'Пожалуйста, выбирите  Город'],
            ['category_id', 'required', 'message' => 'Пожалуйста, укажите Категорию'],
            //[['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6Ley2RYUAAAAAKl7r8PkkZotWk47s65jrckJ1gk_']
        ];
    }
    
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
            $this->status = '1';
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
        ];
    }

}
