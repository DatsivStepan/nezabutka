<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

class City extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%city}}';
    }

    
    public function rules()
    {
        return [
        ];
    }
    
    public function attributeLabels()
    {
        return [
        ];
    }

}
