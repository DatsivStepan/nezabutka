$(document).ready(function(){
    var paginations = {
        limit: 16,
        offset: 0
    };
    
    $(document).on('click', '.show_more_product_btn', function(){
        paginations.offset += paginations.limit;
        getProducts();
    })
    
    function getProducts(){
        //product output type 
        var product_output_type = 'grid';
        if($.cookie('productOutputType')){
            if($.cookie('productOutputType') != 'null'){
                product_output_type = $.cookie('productOutputType');
            }
        }
        //product output type 
        
        var category_id = $('[name=filter_category_id]').val() 
        var search_key = $('.headerSearchText').val() 
                
        var filter_sort = $('[name=filter_sort]:checked').val() 
        var filter_date_to = $('[name=filter_date_to]:checked').val() 
        
        var filter_price_to = $('[name=filter_price_to]').val()
        var filter_price_from = $('[name=filter_price_from]').val()
        var city_id = '';
        if($.cookie('myCityId')){
            if($.cookie('myCityId') != 'null'){
                city_id = $.cookie('myCityId');
            }
        }
        var metro_id = '';
        if($.cookie('myMetroId')){
            if($.cookie('myMetroId') != 'null'){
                metro_id = $.cookie('myMetroId');
            }
        }
//        console.log(filter_date_sort)
//        console.log(filter_date_to)
//        console.log(filter_price_to)
//        console.log(filter_price_from)
//        console.log(city_id)
//        console.log(category_id)

        function formatDate(DateObj){
            var date = new Date(DateObj);
            var options = {
                year: 'numeric',
                month: 'long',
                day: 'numeric',
                timezone: 'UTC'
            };
            return date.toLocaleString("ru", options).slice(0,-3);
        }
        $.ajax({
                type: 'POST',
                url: '../../../product/getproducts',
                data: {filter_sort:filter_sort, filter_date_to:filter_date_to, filter_price_to:filter_price_to,
                filter_price_from:filter_price_from, city_id:city_id, metro_id:metro_id, category_id:category_id, search_key:search_key,
                limit:paginations.limit, offset:paginations.offset},
                dataType:'json',
                success: function(response){
                    //$('.productsBlock').html('');
                    if(response.length == 0){
                        if(paginations.offset == 0){
                            $('.productsBlock').append('<span style=font-size:16px;>Объявлений не найдено, приходите еще, возможно скоро кто-нибудь добавить то, что Вам нужно.</span>');
                        }
                    }
                    if(response.length < 16){
                        $(".show_more_product_btn").hide();
                    }
                    for(var key in response){
                        var product = response[key];
                        var price = number_format(product['price'],0,' ',' ')
                        if(product_output_type == 'grid'){
                            $('.productsBlock').append('<div class="main-item-layer clearfix col-xs-12 col-sm-6 col-md-4 col-lg-3"><div class="main-cat-item">'+
                                    '<a href="../../../product/'+product['id']+'">'+
                                        '<div class="item-img" style="background: url(../../../img/product/'+product['img_src']+') center no-repeat">'+
                                           //' <img src="../../../img/product/'+product['img_src']+'" alt="">'+
                                        '</div>'+
                                        '<div class="item-descr">'+
                                            '<p>'+product['title']+'</p>'+
                                        '</div>'+
                                        '<div class="item-price">'+
                                         '<span>'  +price+' руб</span>'+                                    '</div>'+
                                        '<div class="item-date">'+
                                            '<span>от  '+formatDate(product['date_create'])+'</span>'+
                                        '</div>'+
                                    '</a>'+
                                '</div></div>');                            
                        }else{
                            $('.productsBlock').append('<div class="main-item-layer clearfix col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
                                    '<div class="main-cat-item" style="height:auto;">'+
                                        '<a href="../../../product/'+product['id']+'">'+
                                            '<div class="col-sm-3" style="padding-left:0px;">'+
                                                '<div class="item-img" style="height:188px;background: url(../../../img/product/'+product['img_src']+') center no-repeat">'+
                                                   //' <img src="../../../img/product/'+product['img_src']+'" alt="">'+
                                                '</div>'+
                                            '</div>'+
                                            '<div class="col-sm-8">'+
                                                '<div class="item-descr" style="height:auto;padding-left:0px;padding-bottom:10px;">'+
                                                    '<p style="font-weight:700;">'+product['title']+'</p>'+
                                                '</div>'+
                                                '<div class="item-descr" style="color:#ed4848;padding:0px;font-size:12px;height:auto;">'+
                                                    '<p style="color:#ed4848;">'+product['name']+'</p>'+                                                    
                                                '</div>'+
                                                '<div class="item-descr" style="padding-top:10px;padding-left:0px;font-size:12px;">'+
                                                    '<p style="color:#848484;">'+product['about'].substr(0,155)+'...</p>'+
                                                '</div>'+
                                                '<div class="item-date" style="float:left;">'+
                                                    '<span>от  '+formatDate(product['date_create'])+'</span>'+
                                                '</div>'+
                                                '<div class="item-price"  style="float:right;">'+
                                                    '<span>'  +price+' руб</span>'+
                                                 '</div>'+
                                            '</div>'+
                                        '</a>'+
                                    '</div>'+
                                '</div>');                            
                        }
                    }
                }
        });
    }
    
    if($.cookie('productOutputType')){
        if($.cookie('productOutputType') != 'null'){
            if($.cookie('productOutputType') == 'grid'){
                $('.gridStyle').addClass('active');
                $('.gridStyle img').attr('src','/img/show_grid_active.png');
            }else{
                $('.listStyle').addClass('active');
                $('.listStyle img').attr('src','/img/show_list_active.png');                
            }
        }else{
            $('.gridStyle').addClass('active');
            $('.gridStyle img').attr('src','/img/show_grid_active.png');            
        }
    }else{
        $('.gridStyle').addClass('active');        
        $('.gridStyle img').attr('src','/img/show_grid_active.png');        
    }
    
    $(document).on('click', '.gridStyle', function(){
        $('.listStyle').removeClass('active');
        $('.listStyle img').attr('src','/img/show_list.png');
        
        $('.gridStyle').addClass('active');
        $('.gridStyle img').attr('src','/img/show_grid_active.png');
        $.cookie('productOutputType', 'grid');
        $('.productsBlock').html('');
        getProducts();
    })
    
    $(document).on('click', '.listStyle', function(){
        $('.listStyle').addClass('active');
        $('.listStyle img').attr('src','/img/show_list_active.png');
        
        $('.gridStyle').removeClass('active');
        $('.gridStyle img').attr('src','/img/show_grid.png');
        
        $.cookie('productOutputType', 'list');
        $('.productsBlock').html('');
        getProducts();
    })
    
    if($('.productsBlock').length){
        if($.cookie('filter_sort')){
            if($.cookie('filter_sort') != 'null'){
                $("[name=filter_sort]").each(function( index ) {
                    if($(this).val() == $.cookie('filter_sort')){
                        $(this).prop( "checked", true );
                    }
                })
            }else{
                $.cookie('filter_sort',$('[name=filter_sort]:checked').val())
            }
        }else{
                $.cookie('filter_sort',$('[name=filter_sort]:checked').val())
        }
        
        if($.cookie('filter_date_to')){
            if($.cookie('filter_date_to') != 'null'){
                $("[name=filter_date_to]").each(function( index ) {
                    if($(this).val() == $.cookie('filter_date_to')){
                        $(this).prop( "checked", true );
                    }
                })
            }else{
                $.cookie('filter_date_to',$('[name=filter_date_to]:checked').val())
            }
        }else{
                $.cookie('filter_date_to',$('[name=filter_date_to]:checked').val())
        }
        
        if($.cookie('filter_price_from')){
            if($.cookie('filter_price_from') != 'null'){
                    $('[name=filter_price_from]').val($.cookie('filter_price_from'))
            }else{
                $.cookie('filter_price_from',$('[name=filter_price_from]').val())
            }
        }else{
            $.cookie('filter_price_from',$('[name=filter_price_from]').val())
        }
        
        if($.cookie('filter_price_to')){
            if($.cookie('filter_price_to') != 'null'){
                    $('[name=filter_price_to]').val($.cookie('filter_price_to'))
            }else{
                $.cookie('filter_price_to',$('[name=filter_price_to]').val())
            }
        }else{
            $.cookie('filter_price_to',$('[name=filter_price_to]').val())
        }
        
        if($.cookie('headerSearchText')){
            if($.cookie('headerSearchText') != 'null'){
                    $('.headerSearchText').val($.cookie('headerSearchText'))
            }else{
                //$.cookie('filter_price_to',$('[name=filter_price_to]').val())
            }
        }else{
            //$.cookie('filter_price_to',$('[name=filter_price_to]').val())
        }
        if($.cookie('headerCategoryChoise')){
            if($.cookie('headerCategoryChoise') != 'null'){
                    $('.headerCategoryUl li').removeClass('selected')
                    
                    $('.headerCategoryUl li').each(function(index){
                        console.log($(this).data('category_slug'))
                        console.log($.cookie('headerSearchText'))
                        if($(this).data('category_slug') == $.cookie('headerCategoryChoise')){
                            $(this).addClass('selected');
                            $('span.btn-select-value').text($(this).text());
                        }
                    })
            }else{
            }
        }else{
            //$.cookie('filter_price_to',$('[name=filter_price_to]').val())
        }
        
        getProducts()
    }    
    
    $(document).on('change', '[name=filter_sort]', function(){
        $.cookie('filter_sort',$('[name=filter_sort]:checked').val())
        $('.productsBlock').html('');
        getProducts();
    });
    
    $(document).on('change', '[name=filter_date_to]', function(){
        $.cookie('filter_date_to',$('[name=filter_date_to]:checked').val())
        $('.productsBlock').html('');
        getProducts();
    });
    
    $(document).on('change', '[name=filter_price_to]', function(){
        console.log($('[name=filter_price_to]').val())
        $.cookie('filter_price_to',$('[name=filter_price_to]').val())
        $('.productsBlock').html('');
        getProducts();
    });
    
    $(document).on('change', '[name=filter_price_from]',function(){
        console.log($('[name=filter_price_from]').val())
        $.cookie('filter_price_from',$('[name=filter_price_from]').val())
        $('.productsBlock').html('');
        getProducts();
    })

    function number_format(number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function(n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                        .toFixed(prec);
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '')
                .length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1)
                .join('0');
        }
        return s.join(dec);
    }
})