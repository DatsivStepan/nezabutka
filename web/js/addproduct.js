$(document).ready(function(){
    $(document).on('change','.parentCategoryDropdown',function(){
        var category_id = $(this).val();
        var thisElement = $(this);
        var typeG = 'parent';
        addData(category_id,thisElement,typeG);
    });
    
    $(document).on('change','.firstChildCategoryDropdown',function(){
        var category_id = $(this).val();
        var thisElement = $(this);
        var typeG = 'first';
        addData(category_id,thisElement,typeG);
    });
    
    $(document).on('change','.lastChildCategoryDropdown',function(){
        var category_id = $(this).val();
        var thisElement = $(this);
        var typeG = '';
        addData(category_id,thisElement,typeG);
    });
    
    function addData(category_id,thisElement,typeG){

        if((category_id != '') && (category_id != '0')){
            $.ajax({
                url:'../../../../getchildcategory',
                method:'POST',
                data:{category_id:category_id},
                dataType:'json',
                success:function(response){
                    console.log(response)
                    if(response.status == 'success'){
                        if(response.type == 'child_category'){
                            if(typeG == 'parent'){
                                $('.firstChildCategoryDropdown').parent().show();
                                $('.firstChildCategoryDropdown').show();
                                $('.firstChildCategoryDropdown').html('');
                                
                                $('.lastChildCategoryDropdown').hide();
                                $('.lastChildCategoryDropdown').html('');
                                $('.сategoryAttribute').hide();
                                $('.сategoryAttribute').html('');
                                $('.firstChildCategoryDropdown').append('<option value="0">Выберите подкатегорию...</option>');
                                for(var key in response.data){
                                    $('.firstChildCategoryDropdown').append('<option value="'+key+'">'+response.data[key]+'</option>');
                                }
                            }else if(typeG == 'first'){
                                $('.lastChildCategoryDropdown').parent().show();
                                $('.lastChildCategoryDropdown').show();
                                $('.lastChildCategoryDropdown').html('');
                                $('.сategoryAttribute').hide();
                                $('.сategoryAttribute').html('');
                                $('.lastChildCategoryDropdown').append('<option value="0">Выберите подкатегорию...</option>');
                                for(var key in response.data){
                                    $('.lastChildCategoryDropdown').append('<option value="'+key+'">'+response.data[key]+'</option>');
                                }
                            }
                        }else if(response.type == 'attribute'){
                            $('.сategoryAttribute').html('');
                            $('.сategoryAttribute').show();
//                            $('.сategoryAttribute').append();
                                    //0 inputText 
                                    //1 textarea
                                var attribute = response.data;
                                for(var key in attribute){
//                                    console.log(attribute[key].type);
                                    switch (attribute[key].type) {
                                        case '0':
                                            $('.сategoryAttribute').append('<div class="col-sm-12">'+
                                                    '<div class="col-sm-3">'+
                                                        attribute[key].name+
                                                    '</div>'+
                                                    '<div class="col-sm-9" style="padding-bottom:15px">'+
                                                        '<input type="text" name="Attribute['+attribute[key].slug+']" class="form-control">'+
                                                    '</div>'+
                                                '</div>');
                                            break;
                                        case '1':
                                            $('.сategoryAttribute').append('<div class="col-sm-12">'+
                                                    '<div class="col-sm-3">'+
                                                        attribute[key].name+
                                                    '</div>'+
                                                    '<div class="col-sm-9" style="padding-bottom:15px">'+
                                                        '<textarea name="Attribute['+attribute[key].slug+']" class="form-control"></textarea>'+
                                                    '</div>'+
                                                '</div>');
                                            break;
                                        case '2':
                                            $('.сategoryAttribute').append('<div class="col-sm-12">'+
                                                    '<div class="col-sm-3">'+
                                                        attribute[key].name+
                                                    '</div>'+
                                                    '<div class="col-sm-9 radio'+attribute[key].slug+'" style="padding-bottom:15px">'+
                                                    '</div>'+
                                                '</div>');
                                                var attrData = jQuery.parseJSON(attribute[key].data);
                                                var check = '';
                                                for(var keyy in attrData){
                                                    console.log(keyy)
                                                    if(keyy == 0){
                                                        check = ' checked ';
                                                    }else{
                                                        check = ' ';
                                                    }
                                                    $('.сategoryAttribute').find('.radio'+attribute[key].slug).append('<input type="radio" '+check+' name="Attribute['+attribute[key].slug+']" value="'+keyy+'">'+attrData[keyy]+' ');
                                                }
                                                
                                            //2 radio
                                            break;
                                        case '3':
                                            $('.сategoryAttribute').append('<div class="col-sm-12">'+
                                                    '<div class="col-sm-3">'+
                                                        attribute[key].name+
                                                    '</div>'+
                                                    '<div class="col-sm-9" style="padding-bottom:15px">'+
                                                        '<select name="Attribute['+attribute[key].slug+']" class="select'+attribute[key].slug+' form-control">'+
                                                        '</select>'+
                                                    '</div>'+
                                                '</div>');
                                                var attrData = jQuery.parseJSON(attribute[key].data);
                                                for(var keyy in attrData){
                                                    $('.сategoryAttribute').find('.select'+attribute[key].slug).append('<option value="'+keyy+'">'+attrData[keyy]+'</option>');
                                                }
                                            //3 select
                                            break;
                                        case '4':
                                            $('.сategoryAttribute').append('<div class="col-sm-12">'+
                                                    '<div class="col-sm-3">'+
                                                        attribute[key].name+
                                                    '</div>'+
                                                    '<div class="col-sm-9 checkbox'+attribute[key].slug+'" style="padding-bottom:15px">'+
                                                    '</div>'+
                                                '</div>');
                                                var attrData = jQuery.parseJSON(attribute[key].data);
                                                for(var keyy in attrData){
                                                    $('.сategoryAttribute').find('.checkbox'+attribute[key].slug).append('<input type="checkbox" name="Attribute['+attribute[key].slug+']['+keyy+']" value="'+keyy+'"> '+attrData[keyy]);
                                                }
                                            console.log('4-4')
                                            //4 checkbox
                                            break;
                                    }
                                }
//                            console.log(response.data);
                            
                        }
                    }else{
                        $('.сategoryAttribute').hide();
                        $('.сategoryAttribute').html('');
                        if(typeG == 'parent'){
                            $('.firstChildCategoryDropdown').parent().hide();
                            $('.firstChildCategoryDropdown').hide();
                            $('.firstChildCategoryDropdown').html('');
                            $('.lastChildCategoryDropdown').parent().hide();
                            $('.lastChildCategoryDropdown').hide();
                            $('.lastChildCategoryDropdown').html('');
                        }else if(typeG == 'first'){
                            $('.lastChildCategoryDropdown').parent().hide();
                            $('.lastChildCategoryDropdown').html('');
                        }
                    }
                    
                }
            });
        }else{
            $('.сategoryAttribute').hide();
            $('.сategoryAttribute').html('');
            if(typeG == 'parent'){
                $('.firstChildCategoryDropdown').parent().hide();
                $('.firstChildCategoryDropdown').hide();
                $('.firstChildCategoryDropdown').html('');
                $('.lastChildCategoryDropdown').hide();
                $('.lastChildCategoryDropdown').parent().hide();
                $('.lastChildCategoryDropdown').html('');
            }else if(typeG == 'first'){
                $('.lastChildCategoryDropdown').hide();
                $('.lastChildCategoryDropdown').parent().hide();
                $('.lastChildCategoryDropdown').html('');
            }
        }
    }
    
//    $('.DefaultFotoProduct').hover(
//        function() {
//            $( this ).attr('src', '../../../img/DefaultFotoProductActive.png');
//        }, function() {
//            $( this ).attr('src', '../../../img/DefaultFotoProduct.png');
//        }
//    );

//    $( ".addPhoto" ).each(function() {
//        alert('afadff')
//    });
//    alert('dafdaf');
//    $(document).on('click',".addPhot",function() {
//    var val = $(this).css('background-image');
//    if ( val.indexOf('DefaultFotoProduct') !== -1 ){
//        console.log('yes');
//    }else{
//        console.log('n');
//    }
//    });
    if($('.addPhoto').length){
            var previewNode = document.querySelector("#template");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);
            var photosArray = $.parseJSON($('#product-image_array').val());
            console.log(photosArray);
            var myDropzoneG = new Dropzone($('.addPhoto')[0], {
                url: "../../../product/saveproductimg",
                maxFiles:6,
                previewTemplate: previewTemplate,
//                autoQueue: false,
                previewsContainer: "#previews", 
                clickable: ".addPhoto" 
            });

            myDropzoneG.on("maxfilesexceeded", function(file) {
            });

            myDropzoneG.on("complete", function(response) {
                if (response.status == 'success') {
                    $( ".addPhoto" ).each(function() {
                        //alert('-----');
                        var thisBackground = $(this).css('background-image');
                        if ( thisBackground.indexOf('product_add_photo') !== -1 ){
                            photosArray.push(response.xhr.response);
                            $('#product-image_array').val(JSON.stringify(photosArray));
                            
                            $(this).css('background-image', 'url(/img/product/'+response.xhr.response+')')
                            $(this).parent().parent().find('.deleteProductImage').show();
                            $(this).parent().parent().find('.deleteProductImage').data('name',response.xhr.response);
                            return false;
                        }
                    });
                }else{
                    $('.maxPhoto').show()
                }
            });

            myDropzoneG.on("removedfile", function(response) {
                if(response.xhr != null){
                    
                }
            });
            
            $(document).on('click','.deleteProductImage',function(){
                var shos = $(this).parent();
                var name_photo = $(this).data('name');
                $.each( myDropzoneG.files, function( key, value ) {
                    if(value.xhr.response == name_photo){
                        myDropzoneG.removeFile(myDropzoneG.files[key]);
                    }
                });
                $( ".addPhoto" ).each(function() {
                    var thisBackground = $(this).css('background-image');
                    if ( thisBackground.indexOf(name_photo) !== -1 ){
                            delete(photosArray[name_photo]);
                            var removeItem = name_photo;
                            photosArray = jQuery.grep(photosArray, function(value) {
                                return value != removeItem;
                              });
                        $('#product-image_array').val(JSON.stringify(photosArray));

                        $(this).css('background-image', 'url(/img/product_add_photo.png)')
                        $(this).parent().parent().find('.deleteProductImage').hide();
                    }
                })
                
            });
    }
    
    $(document).on('keyup',"input[name=city_name]",function(){
        if($(this).val().length > 1){
            var search_key = $(this).val()
            $.ajax({
                type: 'POST',
                url: '../../../site/getcity',
                data: {search_key:search_key},
                dataType:'json',
                success: function(response){
                    $('.appProductChoiseCity').html('')
                    $('#product-city_id').val('')
                    $('.metroBlock').hide();
                    $('.metroBlock').find('#product-metro_id').html('');    
                        for(var key in response){
                            var city = response[key];
                            $('.appProductChoiseCity').append('<li data-city_id="'+city.city_id+'">'+city.name+'</li>')
                        }                    
                }
            })
        }else{
            $('#product-city_id').val('')
            $('.metroBlock').hide();
            $('.metroBlock').find('#product-metro_id').html('');    
            $('.appProductChoiseCity').html('')
        }
    })
    
    $(document).on('click', ".appProductChoiseCity li", function(){
        $('#product-city_id').val($(this).data('city_id'))
        $('input[name=city_name]').val($(this).text())
        $('.appProductChoiseCity').html('');
        
        var city_id = $(this).data('city_id');
            $.ajax({
                type: 'POST',
                url: '../../../site/getmetro',
                data: {city_id:city_id},
                dataType:'json',
                success: function(response){
                    $('.metroBlock').find()
                    $('.metroBlock').find('#product-metro_id').html('');                        
                    if(response.length > 0){
                        $('.metroBlock').show()
                        for(var key in response){
                            var metro = response[key];
                            $('.metroBlock').find('#product-metro_id').append('<option value="'+metro.id+'">'+metro.name+'</option>')
                        }
                    }else{
                        $('.metroBlock').hide()                        
                    }
                }
            })
    })
    
    //$('.firstChildCategoryDropdown').parent().hide();
    //$('.lastChildCategoryDropdown').parent().hide();
})