var map;
function initMap(address,LatLng) {
    var myLatLng = {lat: 55.7494718, lng: 37.3516334};
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8,
      center: myLatLng,
      maxZoom: 10,
    });
    
    var address = document.getElementById('user_address').value;
    var radius = document.getElementById('user_radius').value;

    if(address != ''){
        var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                var Local = results[0].geometry.location;
                var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};                    
                if(radius != '50+'){
                    
                    var marker = new google.maps.Circle({
                        strokeColor: '#b2c7f2',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#b2c7f2',
                        fillOpacity: 0.35,
                        map: map,
                        center: latLng,
                        radius: parseInt(radius) * 1000
                    });
                    map.setCenter(latLng);
                    map.setZoom(8);
                    
                }else{
                    
                    var KmlLayer = new google.maps.KmlLayer({
                        url: 'http://nezabutka.roketdev.pro/js/one.kml',
                        map: map,
                        center: latLng,
                        zoom:8,
                    });
                    google.maps.event.addListenerOnce(map, 'zoom_changed', function() {
                        this.setZoom(2);
                    })
                }
            }else{
                alert('error');
            }
        });
    }
}

$(document).ready(function(){
    if($('#map').length){
        initMap();        
    }
    
    $(document).on('click', '.addToFavorite', function(){
        var product_id = $(this).data('product_id');
        var thisElement = $(this);
        $.ajax({
                type: 'POST',
                url: '../../../product/productfavorite',
                data: {product_id:product_id, status:'add'},
                dataType:'json',
                success: function(response){
                    if(response.status != 'error'){
                        thisElement.removeClass('addToFavorite')
                        thisElement.addClass('deleteWithFavorite')
                        thisElement.html('<span>Удалить из избранного</span>')
                    }
                }
        })
        
    })
    
    $(document).on('click', '.deleteWithFavorite', function(){
        var product_id = $(this).data('product_id');
        var thisElement = $(this);
        $.ajax({
                type: 'POST',
                url: '../../../product/productfavorite',
                data: {product_id:product_id, status:'delete'},
                dataType:'json',
                success: function(response){
                    if(response.status != 'error'){
                        thisElement.removeClass('deleteWithFavorite')
                        thisElement.addClass('addToFavorite')
                        thisElement.html('<span>Добавить в избранное</span>')
                    }
                }
        })
    })
});

$(window).load(function() {
    // The slider being synced must be initialized first
    $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 129,
        itemMargin: 2,
        asNavFor: '#slider'
    });

    $('#slider').flexslider({
        selector: "a",
        animation: "fade",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
    });
});

(function() {

    // store the slider in a local variable
    var $window = $(window),
        flexslider = { vars:{} };

    // tiny helper function to add breakpoints
    function getGridSize() {
        return (window.innerWidth < 768) ? 1 :
            (window.innerWidth < 1024) ? 2 :
            (window.innerWidth < 1366) ? 3 : 4;
    }

    $window.ready(function() {
        $('.carousel-all-product').flexslider({
            animation: "slide",
            animationLoop: false,
            touch: true,
            slideshow: false,
            controlNav: true,
            itemWidth: 210,
            itemMargin: 16,
            minItems: getGridSize(), // use function to pull in initial value
            maxItems: getGridSize(), // use function to pull in initial value
            controlsContainer: $(".custom-controls-container"),
            customDirectionNav: $(".custom-navigation a"),
            start: function(slider){
                console.log(slider.pagingCount)
                if(slider.pagingCount <= 1){
                    $(".custom-navigation a").hide();
                }
            }
        });
    });

    // check grid size on resize event
    $window.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
    });
}());

$('.phone_java').on('click', function () {
    $('.pid').hide();
    $('.mobile_show').show();

})