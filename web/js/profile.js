var markers = [];
var map = '';
function initMap(address,LatLng) {
    var myLatLng = {lat: 55.7494718, lng: 37.3516334};
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8,
      center: myLatLng
    });
    var input = document.getElementById('searchTextField');
    if(input.value != ''){
        var address = input.value; 
        var geocoder = new google.maps.Geocoder();
        //var address = $(this).val();
            geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                var Local = results[0].geometry.location;
                var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};                    
                  var marker = new google.maps.Marker({
                    position: latLng,
                    zoom:6,
                    map: map,
                 //   title: 'Hello World!'
                  });
                  map.setCenter(latLng);
                  map.setZoom(8);
            }else{
                alert('error');
            }
        });
    }
    
    
    var options = {
      //types: ['(cities)'],
      componentRestrictions: {country: "ru"}
    };
    var autocomplete = new google.maps.places.Autocomplete(input, options);
    
    autocomplete.addListener('place_changed', function() {
        setMapOnAll(null);
        var input = document.getElementById('saveStatusField').value = 'true';
        var place = autocomplete.getPlace();
        map.setCenter(place.geometry.location);
        var marker = new google.maps.Marker({
            position: place.geometry.location,
            map: map,
            title: 'Hello World!'
        });
        markers.push(marker);        
       
    });
    
}

function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

$(document).ready(function(){
    
    $("#user-telefone").mask("(999) 999-9999");
    
    if($('#map').length){
        initMap();
    }
    
    
    if($('.changeProfilePhoto').length){
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone(document.body, {
            url: "../../../profile/default/saveprofilephoto",
            previewTemplate: previewTemplate,
            previewsContainer: "#previews",
            clickable: ".changeProfilePhoto",
            uploadMultiple:false,
            maxFiles:1,
        });

        myDropzone.on("maxfilesexceeded", function(file) {
                myDropzone.removeAllFiles();
                myDropzone.addFile(file);
        });

        myDropzone.on("complete", function(response) {
            if (response.status == 'success') {
                $('.userProfileImage').attr('src', '/'+response.xhr.response)
                $('[name=image_prodile_src]').val(response.xhr.response);
                $('.blockSavePhotoButton').show();
            }
        });

        myDropzone.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });

    }
    
    $(document).on('click', '.saveProfilePhoto',function(){
        var image_src = $('[name=image_prodile_src]').val();
        $.ajax({
            type: 'POST',
            url: '../../../../profile/default/updateprofilephoto',
            data: {image_src:image_src},
            dataType:'json',
            success: function(response){
                if(response.status == 'success'){
                    swal("Аватар сохранено!", "", "success")
                    $('.blockSavePhotoButton').hide();
                }
            }
        });            
    })
    
    $(document).on('click', '.changeProfileData',function(){
        $('#modalUserUpdate').modal('show')
    })
    
    $('.formRadiusPosition').submit(function(e){
        e.preventDefault();
    });
    
    $('[name=search_address]').keydown(function(e){
        if((e.keyCode == 37) || (e.keyCode == 38) || (e.keyCode == 39) || (e.keyCode == 40)){
            
        }else{
            $('[name=save_status]').val('false')            
        }
    });
    
    $(document).on('click', '.btnSavePosition', function(e){
        var name_radius = $('[name=radius]').val();
        var search_address = $('[name=search_address]').val();
//        console.log(search_address);
        if($('[name=save_status]').val() == 'true'){
            if(search_address != ''){
                $.ajax({
                    type: 'POST',
                    url: '../../../profile/default/updateaddress',
                    data: {name_radius:name_radius, search_address:search_address},
                    dataType:'json',
                    success: function(response){
                        if(response.status == 'success'){
                            swal("Местонахождение сохранено!", "", "success")
                        }
                    }
                });            
            }            
        }else{
            swal("Адрес не подходит!", "выберите адрес из предложенных вариантов", "success")            
        }
    });
    
    $(document).on('click', '.deleteWithFavorite', function(){
        var product_id = $(this).data('product_id');
        var thisElement = $(this);
        $.ajax({
                type: 'POST',
                url: '../../../product/productfavorite',
                data: {product_id:product_id, status:'delete'},
                dataType:'json',
                success: function(response){
                    if(response.status != 'error'){
                        $('.favoriteProduct'+product_id).remove()
                    }
                }
        })
    })
    
    function deleteProduct(status,product_id){
        console.log(product_id);
        console.log(status);
        $.ajax({
                type: 'POST',
                url: '../../../product/deleteproduct',
                data: {product_id:product_id, status:status},
                dataType:'json',
                success: function(response){
                    if(response.status != 'error'){
                        $('.productBlock'+product_id).remove();
                        switch (status) {
                            case 'sold':
                                swal("Удалено!", "Товар добавлен в 'Продано'!", "success")
                            break;
                            case 'frozen':
                                swal("Разморожен!", "Товар добавлен в 'Продается'!", "success")
                            break;
                            case 'soldout':
                                swal("Удалено!", "Товар добавлен в 'Архив'!", "success")
                            break;
                            case 'archive':
                                swal("Удалено!", "Товар удален из сайта!", "success")
                            break;
                        }
                    }
                }
        })        
    }
    
    $(document).on('click', '.deleteProduct', function(){
        var product_id = $(this).data('product_id');
        var work_status = $(this).data('status');
        switch (work_status) {
            case 'sold':
                    swal({
                        title: "Вы уверены?",
                        text: "Что хотите снять товар с продажи!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Да, уверен!",
                        cancelButtonText: "Отменить",
                        closeOnConfirm: false
                    },function(){
                        deleteProduct(work_status,product_id);
                    });
                break;
            case 'soldout':
                    swal({
                        title: "Вы уверены?",
                        text: "Что хотите удалить товар!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Да, уверен!",
                        cancelButtonText: "Отменить",
                        closeOnConfirm: false
                    },function(){
                        deleteProduct(work_status,product_id);
                    });
                break;
            case 'frozen':
                    swal({
                        title: "Вы уверены?",
                        text: "Что хотите разморозить товар!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Да, уверен!",
                        cancelButtonText: "Отменить",
                        closeOnConfirm: false
                    },function(){
                        deleteProduct(work_status,product_id);
                    });
                break;
            case 'archive':
                    swal({
                        title: "Вы уверены?",
                        text: "Что хотите удалить товар с архива!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Да, уверен!",
                        cancelButtonText: "Отменить",
                        closeOnConfirm: false
                    },function(){
                        deleteProduct(work_status,product_id);
                    });
                break;
        }
    })
    
});        
