$(document).ready(function(){
    
    
    $(document).on('keyup',"input[name=city_name]",function(){
        if($(this).val().length > 1){
            var search_key = $(this).val()
            $.ajax({
                type: 'POST',
                url: '../../../site/getcity',
                data: {search_key:search_key},
                dataType:'json',
                success: function(response){
                    $('.appProductChoiseCity').html('')
                    $('#metro-city_id').val('')
                    for(var key in response){
                        var city = response[key];
                        $('.appProductChoiseCity').append('<li style="padding:8px;font-size:16px;" data-city_id="'+city.city_id+'">'+city.name+'</li>')
                    }
                }
            })
        }else{
            $('#metro-city_id').val('')
            $('.appProductChoiseCity').html('')
        }
    })
    
    $(document).on('click', ".appProductChoiseCity li", function(){
        $('#metro-city_id').val($(this).data('city_id'))
        $('input[name=city_name]').val($(this).text())
        $('.appProductChoiseCity').html('')
    })
    
    if($('.category_photo').length){
            var myDropzone = new Dropzone($('.category_photo')[0], { // Make the whole body a dropzone
                uploadMultiple:false,
                maxFiles:1,
                clickable:'.newPhoto'
            });

            myDropzone.on("maxfilesexceeded", function(file) {
                    myDropzone.removeAllFiles();
                    myDropzone.addFile(file);
            });

            myDropzone.on("complete", function(response) {
                if (response.status == 'success') {
                    $('.category_photo').attr('src','/img/category/'+response.xhr.response);
                    $('#category-img_src').val(response.xhr.response);
                    //console.log(response.xhr.response);
                }
            });

            myDropzone.on("removedfile", function(response) {
                if(response.xhr != null){
                   //deleteFile(response.xhr.response);
                }
            });
        }
        
        
        var attributeArray = [];
        $(document).on('click','.buttonAddAttributeValue',function(){
            var attributeValue = $('.textAttributeValue').val();
            if(attributeValue != ''){
                    $('.attributeValuesBlock').append('<tr>'+
                                                            '<td class="attributeDataa">'+
                                                                attributeValue+
                                                            '</td>'+
                                                            '<td>'+
                                                                '<i style="font-size:16px;" class="fa fa-times deleteAttributeValue"></i>'+
                                                            '</td>'+
                                                        '</tr>');
                    attributeArray.push(attributeValue);
                    $('#attribute-data').val(JSON.stringify(attributeArray));
                    $('.textAttributeValue').val('');
            }else{
                $('.textAttributeValue').focus();
            }
        });
        
        $(document).on('click','.deleteAttributeValue',function(){
            var text = $(this).parent().parent().find('.attributeDataa').text();
            $(this).parent().parent().remove();
            attributeArray = jQuery.grep(attributeArray, function(value) {
              return value != text;
            });
            $('#attribute-data').val(JSON.stringify(attributeArray));
        });
        
        $(document).on('change','[name=attribute_type]',function(){
            $('#attribute-type').val($(this).val());
            if($(this).val() > 1){
                $('.addAttributeValueBlock').show();
            }else{
                $('.addAttributeValueBlock').hide();
                var attributeArray = [];
                $('.attributeValuesBlock').html(' ');
                $('#attribute-data').val('');
            }
        });
        
        
        $(document).on('click','[name=addAttribute]',function(event){
            event.preventDefault();
            if($('#attribute-type').val() > 1){
                if(($('#attribute-data').val() == '') || ($('#attribute-data').val() == '[]')){
                    $('.textAttributeValue').focus();
                    swal({
                        title: "",
                        text: "Введите хотя бы одно значение атрибута!",
                        type: "warning",
                        confirmButtonColor: "#5cb85c",
                        confirmButtonText: "Ок!",
                        closeOnConfirm: true 
                    }, function(){
                        $('#attribute-name').focus();
                    });
                }else{
                    if($('#attribute-name').val().length > 0){
                        addAttributAjax($('#newFormAttribute').serializeArray())
                    }else{
                        swal({
                            title: "",
                            text: "Введите название!",
                            type: "warning",
                            confirmButtonColor: "#5cb85c",
                            confirmButtonText: "Ок!",
                            closeOnConfirm: true 
                        }, function(){
                            $('#attribute-name').focus();
                        });
                    }
                }
            }else{
                if($('#attribute-name').val().length > 0){
                    addAttributAjax($('#newFormAttribute').serializeArray())
                }else{
                    swal({
                        title: "",
                        text: "Введите название!",
                        type: "warning",
                        confirmButtonColor: "#5cb85c",
                        confirmButtonText: "Ок!",
                        closeOnConfirm: true 
                    }, function(){
                        $('#attribute-name').focus();
                    });
                }
            }
        });
        
        function addAttributAjax(data){
            //console.log(data);
            $.ajax({
                url:'../../../../administration/category/addattribute',
                method:'POST',
                data:{data:data},
                dataType:'json',
                success:function(response){
                    if(response.status == 'save'){
                        $('.attributeViewedBlock').append('<tr>'+ 
                                    '<td>'+response.data['name']+'</td>'+
                                    '<td>'+response.data['type']+'</td>'+
                                    '<td>'+response.data['data']+'</td>'+
                                    '<td><i class="fa fa-times deleteAttribute" data-id="'+response.data['id']+'"></i></td>'+
                              '</tr>');
                      $('#addAtr').modal('hide');
                    }
                }
            });
        }
        
        $(document).on('click','.deleteAttribute',function(){
            var attribute_id = $(this).data('id');
            var thisElement = $(this);
            $.ajax({
                url:'../../../../administration/category/deleteattribute',
                method:'POST',
                data:{attribute_id:attribute_id},
                dataType:'json',
                success:function(response){
                    if(response.status == 'delete'){
                        thisElement.parent().parent().remove();
                        swal("", "Атрибут удалено!", "success")
                    }
                }
            });
        })
        
        $(document).on('click','.deleteCategory',function(){
            var category_id = $(this).data('id');
            var thisElement = $(this);
            
            swal({   
                title: "Удалить категорию?",
                text: "с ней удаляются все ее под категории",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Да, удалить",
                closeOnConfirm: false,
                cancelButtonText: "Нет, не удалять",
            }, function(){
                $.ajax({
                    url:'../../../../administration/category/deletecategory',
                    method:'POST',
                    data:{category_id:category_id},
                    dataType:'json',
                    success:function(response){
                        if(response.status){
                            swal("Удалено!", "", "success"); 
                            $('#category-table').find('.categoryId' + category_id).remove();
                        }
                    }
                });
            });
            
        })
        
})