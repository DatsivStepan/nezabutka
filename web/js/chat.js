var socket = io.connect('https://35.165.139.87:2877');
//var socket = io.connect('https://192.168.0.108:2877');
var chatModule = (function() {
    var unreadSelector,
        current_user,
        groupSelector,
        group_id,
        currentGroup,
        groupOpen,
        currentTextarea,
        sender,
        msgTick,
        mcs;

    return {
        setup: function (socket) {
            $(groupSelector).children('*').on('click', function (e) {
                e.stopPropagation();
            });
            $(groupSelector).on('click', function (e) {
                if(groupOpen !== $(this).data("group-id")) {
                    $(this).parent().find('.chat-box').find('.messages').html('<div class="load-data"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');
                    $('.chat-box').slideUp();
                    group_id = $(this).data("group-id");
                    sender = $(this).data("sender");
                    currentGroup = $(this);
                    currentTextarea = $(this).parent().find('.send-textarea');
                    $(this).parent().find('.chat-box').slideDown("slow");
                    $(this).parent().find('.load-data').show("fast");
                    socket.emit('getHistory', group_id, current_user);
                }else{
                    this.groupOpen = 0;
                    $(this).parent().find('.chat-box').slideToggle("slow");
                }
                groupOpen = $(this).data("group-id");
            });

            $(groupSelector).parent().find('.send-btn-msg').on('click', function (e) {
                if(currentTextarea.val() != '') {
                    currentGroup.parent().find('.messages').append('<div class="msg-text msgFrom">' + currentTextarea.val() + '</div>');
                    currentGroup.parent().find('.messages').scrollTop(currentGroup.parent().find('.messages')[0].scrollHeight);

                    socket.emit('sendMassage', {
                        group_id: currentGroup.data("group-id"),
                        text: currentTextarea.val(),
                        recipient_id: sender,
                        sender_id: current_user
                    });
                    currentTextarea.val('');
                }
                e.stopPropagation();
            })
            msgTick = document.getElementById("newMSG");
            console.log(msgTick);
        },
        init: function(config, callback) {
            unreadSelector = $(config.unreadSelector);
            current_user = $(config.userIdSelector).val();
            groupSelector = $(config.groupSelector);
            callback(this);
        },
        setUnread: function (count) {
            if(count > 0){
                count = '+' + count;
            }else if(count == 0){
                count = count;
            }
            unreadSelector.text(count);
        },
        setUnreadTitle: function (arrayCount) {
            if(arrayCount){
                $(".group-box a sup.unreadMsg").remove();
                arrayCount.forEach(function(e, i, arr){
                    $(".group-box a[data-group-id='"+e.id+"']").append('<sup class="unreadMsg">+' + e.count + '</sup>');
                })
            }
        },
        getCurrentUser: function () {
            return current_user;
        },
        getCurrentGroup: function () {
            return currentGroup;
        },
        getMsgTick: function () {
            return msgTick;
        },
        getGroupId: function () {
            return group_id;
        }
    }
}());
chatModule.init({
    unreadSelector: ".unreadMsg",
    userIdSelector: "#user_id",
    groupSelector: ".group-box a"
}, function (chat) {
    socket.on('connect', function () {
        chat.setup(socket);
        socket.emit('login', chat.getCurrentUser());
        socket.on('unreadCount', function (unreadCount) {
            console.log(unreadCount);
            chat.setUnread(unreadCount.msg_count);
            chat.setUnreadTitle(unreadCount.title_count);
            if(unreadCount.msg_count){
                chat.getMsgTick().play();
            }
        });
        socket.on('history', function (results) {
            chat.getCurrentGroup().parent().find('.load-data').hide("fast", function () {
                results.forEach(function (el, i, arr) {
                    if(el.recipient_id == chat.getCurrentUser()){
                        chat.getCurrentGroup().parent().find('.messages').prepend('<div class="msg-text msgTo">' + el.text + '</div>');
                    }else{
                        chat.getCurrentGroup().parent().find('.messages').prepend('<div class="msg-text msgFrom">' + el.text + '</div>');
                    }
                });
                chat.getCurrentGroup().parent().find('.messages').scrollTop(chat.getCurrentGroup().parent().find('.messages')[0].scrollHeight);
            });
        });

        socket.on('give_message', function (data) {
            if((chat.getCurrentGroup()) && chat.getCurrentGroup().data("group-id") == data.group_id) {
                if (parseInt(data.recipient_id) == chat.getCurrentUser()) {
                    chat.getCurrentGroup().parent().find('.messages').append('<div class="msg-text msgTo">' + data.text + '</div>');
                    chat.getCurrentGroup().parent().find('.messages').scrollTop(chat.getCurrentGroup().parent().find('.messages')[0].scrollHeight);
                } else {
                    chat.getCurrentGroup().parent().find('.messages').append('<div class="msg-text msgFrom">' + data.text + '</div>');
                    chat.getCurrentGroup().parent().find('.messages').scrollTop(chat.getCurrentGroup().parent().find('.messages')[0].scrollHeight);
                }
                chat.getMsgTick().play();
                if(chat.getGroupId()){
                    socket.emit('readedMsg', chat.getGroupId(), chat.getCurrentUser());
                }
            }else{
                socket.emit('readedMsg', 0, chat.getCurrentUser());
            }
        })

        socket.on('onlineChange', function (data) {
            $(".class_info_user").each(function (index) {
                if(data.indexOf(String($(this).data("user-id"))) != -1){
                    $(this).find(".status_online").removeClass('offline');
                    $(this).find(".status_online").addClass('online').children('a').html('В сети');
                }else{
                    $(this).find(".status_online").removeClass('online');
                    $(this).find(".status_online").addClass('offline').children('a').html('Не в сети');
                }
            })
        })
    });
});
