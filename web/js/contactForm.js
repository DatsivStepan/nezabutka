$(document).ready(function () {
    function checkForm(id) {
        if(typeof id != 'undefined'){
            if($('#'+id).val() == ''){
                return 0;
            }
        }
        switch (id) {
            case 'userName':
                if (!($('#' + id).val().match(/^[a-zA-Zа-яА-Я]+$/))) {
                    $('#errorMasege > .' + id).text('Некоректно заполнено Имя пользователя');
                    $('#' + id).css('border', '1px solid red');
                    $('#errorMasege > .' + id).show();
                } else {
                    $('#errorMasege > .' + id).text('');
                    $('#' + id).css('border', '1px solid green');
                    $('#errorMasege > .' + id).hide();
                }
                break;
            case 'e-mail':
                if (!($('#' + id).val().match(/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i))) {
                    $('#errorMasege > .' + id).text('Некоректно заполнено Email');
                    $('#' + id).css('border', '1px solid red');
                    $('#errorMasege > .' + id).show();
                } else {
                    $('#errorMasege > .' + id).text('');
                    $('#' + id).css('border', '1px solid green');
                    $('#errorMasege > .' + id).hide();
                }
                break;
            case 'massege':
                if (!($('#' + id).val().length >= 10)) {
                    $('#errorMasege > .' + id).text('Минимальная длина сообщения десеть знаков');
                    $('#' + id).css('border', '1px solid red')
                    $('#errorMasege > .' + id).show();
                } else {
                    $('#errorMasege > .' + id).text('');
                    $('#' + id).css('border', '1px solid green');
                    $('#errorMasege > .' + id).hide();
                }
                break;
            default:
                var errorCount = 0;
                if (!($('#userName').val().match(/^[a-zA-Zа-яА-Я]+$/))) {
                    $('#userName').css('border', '1px solid red');
                    errorCount++;
                    $('#errorMasege > .userName').show();
                } else {
                    $('#errorMasege > .userName').text('');
                    $('#userName').css('border', '1px solid green');
                    $('#errorMasege > .userName').hide();
                }
                if (!($('#e-mail').val().match(/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i))) {
                    $('#e-mail').css('border', '1px solid red');
                    errorCount++;
                    $('#errorMasege > .e-mail').show();
                } else {
                    $('#errorMasege > .e-mail').text('');
                    $('#e-mail').css('border', '1px solid green');
                    $('#errorMasege > .e-mail').hide();
                }
                if (!($('#massege').val().length >= 10)) {
                    $('#massege').css('border', '1px solid red');
                    errorCount++;
                    $('#errorMasege > .massege').show();
                } else {
                    $('#errorMasege > .massege').text('');
                    $('#massege').css('border', '1px solid green');
                    $('#errorMasege > .massege').hide();
                }
                if (errorCount == 0) {
                    return true;
                } else {
                    return false;
                }
                break;
        }
    }

    $('.contactItem').on('blur', function () {
        console.log($(this).attr('id'));
        checkForm($(this).attr('id'));
    });
    $('#btn-contact').on('click', function (even) {
        even.preventDefault();
        if (checkForm()) {
            var res = $('#contactForm').serializeArray();
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
            $.ajax({
                url: '/site/contact',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function (data) {
                    $('.alert.alert-success').show();
                    $('form#contactForm').trigger('reset');
                }
            });
        } else {
            console.log('Incorrectly completed forms');
        }
    });

});