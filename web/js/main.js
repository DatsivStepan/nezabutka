var geocoder = new google.maps.Geocoder();
$(document).ready(function(){

    function initializationMultiple(){
        $('#choiseMetro').multiselect({
            allSelectedText: 'Выбрано все ',
            filterPlaceholder:'Поиск',
            selectAllText:'Выбрать все',
            nonSelectedText:'Выбрать станцию метро',
            nSelectedText: 'выбрано',
            enableFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 400,
            enableCaseInsensitiveFiltering: true
        });
    }
    function initializationMultipleMobile(){
        $('#choiseMetroMobile').multiselect({
            allSelectedText: 'Выбрано все ',
            filterPlaceholder:'Поиск',
            selectAllText:'Выбрать все',
            nonSelectedText:'Выбрать станцию метро',
            nSelectedText: 'выбрано',
            enableFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 400,
            enableCaseInsensitiveFiltering: true
        });
    }


    $('.headerSearchText').keyup(function(e){
        if(e.keyCode == 13)
        {
            var inputValue = $('.headerSearchText').val();
            var category_slug = $('.headerCategoryChoise').val();
            if(inputValue.length >= 2){
                    $.cookie('headerSearchText',inputValue)
                    $.cookie('headerCategoryChoise',category_slug)
                if(category_slug != ''){
                    window.location.replace("/category/"+category_slug);
                }else{
                    window.location.replace("/");
                }            
            }
        }
    });

    function showPosition(position) {
//        console.log(position)
        var latLng = {lat:parseFloat(position.coords.latitude),lng:parseFloat(position.coords.longitude)};
        geocoder.geocode({
            'location': latLng,
            //componentRestrictions: {country: 'RU'}
        }, function(results, status) {
            if (status === 'OK') {
                if (results[1]) {
                    var arrAddress = results[1].address_components;
                    var city_name = '';
                    $.each(arrAddress, function (i, address_component) {
                        if (address_component.types[0] == "locality"){
                            city_name = address_component.long_name;
                            city_name = 'Москва';
                            $.ajax({
                                type: 'POST',
                                url: '../../../site/ifcityinbase',
                                data: {city_name:city_name},
                                dataType:'json',
                                success: function(response){
                                    if(response.status == 'success'){
                                        $('.area-link').addClass('active');
                                        $('.area-link_mob').addClass('active');
                                        $('.form-area').css('display','block');
                                        $('.form-area_mob').css('display','block');
                                        
                                        $('.otherCityChoisen').hide();
                                        $('.yourCityBlock').show();
                                        $('.yourCity').html(response.city_model.name+'?');
                                        $('.yourCityConfirm').data('city_id',response.city_model.city_id);
                                    }
                                    console.log(response);
                                }
                            });
                        }
                    });
                } else {
                    //window.alert('No results found');
                }
            } else {
              window.alert('Geocoder failed due to: ' + status);
            }
        })
//        console.log(position.coords.longitude)
        //initMap2('',{lat:position.coords.latitude,lng:position.coords.longitude});
    }
    $(document).on('click','.choiseOtherCity',function(){
        $('.otherCityChoisen').show();
    })
    function getLocation() {
        if (navigator.geolocation) {
            return navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            return "Geolocation is not supported by this browser.";
        }
    }
//    $.cookie('myCityId',null);
    if((!$.cookie('myCityId')) || ($.cookie('myCityId') == 'null')){
        getLocation();
    }
    
    $(document).on('click',".yourCityConfirm",function(){
            $.cookie("myCityId", $(this).data('city_id'), { path: ''});
            $.cookie("myCityId", $(this).data('city_id'), { path: '/'});
            $.cookie("myCityId", $(this).data('city_id'), { path: '/profile/'});
            $.cookie("myCityId", $(this).data('city_id'), { path: '/category/'});
            
            //$.cookie('myCityId', $(this).data('city_id'));            
            window.location.reload()
    })
    
    $(document).on('click',".cityChoiseClose",function(){
        $('.area-link').removeClass('active');
        $('.form-area').css('display','none');
    })
    
    $(document).on('click',".loginFormClose",function(){
        $('.form-account').hide();
        $('.account-link').addClass('active');
    })
    
    function $_GET(param){
        var vars = {};
        window.location.href.replace( location.hash, '' ).replace( 
                /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
                function( m, key, value ) { // callback
                        vars[key] = value !== undefined ? value : '';
                }
        );

        if ( param ) {
                return vars[param] ? vars[param] : null;	
        }
        return vars;
    }
    var w = $_GET('w')
    
    
    $(document).on('click', '.category-change.hideCateg',function(){
        $(this).parent().find('.category_mobile_list').show();
        $(this).removeClass('hideCateg');
        $(this).addClass('showCateg');
    });
    $(document).on('click', '.category-change.showCateg',function(){
        $(this).parent().find('.category_mobile_list').hide();
        $(this).removeClass('showCateg');
        $(this).addClass('hideCateg');
        
        $('.submenuList').hide();
        $('.submenuList').removeClass('showE')
        $('.submenuList').addClass('hideE')
        
        $('.submenuSubList').hide();
        $('.submenuSubList').removeClass('showEl')
        $('.submenuSubList').addClass('hideEl')
    });
    
    
    $(document).on('click', '.mobileCategoryMenu  li a.hideE',function(){
        var category_slug = $(this).data('category_slug');
        var category_id = $(this).data('category_id');
        $(this).removeClass('hideE')
        $(this).addClass('showE')
        var thisElement = $(this);
        if($(this).parent().find('.submenuList').html() == ''){
            $.ajax({
                type: 'POST',
                url: '../../../site/getchildcategory',
                data: {category_id:category_id},
                dataType:'json',
                success: function(response){
                    if(response.length != 0){
                        thisElement.parent().find('.submenuList').show();
                        thisElement.css('color', '#ff6f69' );

                        thisElement.parent().find('.submenuList').append('<li class="list-group-item ogoloshena_pid" >'+
                                '<a href="../../../category/'+category_slug+'">Все объявления</a>'+
                            '</li>');
                    
                        for(var key in response){
                            var category = response[key];
                            if(category.child == '0'){
                                thisElement.parent().find('.submenuList').append('<li class="list-group-item ogoloshena_pid" >'+
                                    '<a href="../../category/'+category.info['slug']+'">'+
                                        category.info['name']+
                                    '</a>'+
                                '</li>');
                            }else{
                                thisElement.parent().find('.submenuList').append('<li class="list-group-item ogoloshena_pid" >'+
                                    '<a class="hideEl" data-category_slug="'+category.info['slug']+'" data-category_id="'+category.info['id']+'">'+
                                        category.info['name']+
                                    '</a>'+
                                    '<ul class="list-group submenuSubList ogoloshena_micro"></ul>'+
                                '</li>');
                            }
                            
                        }
                    }else{
                        window.location.replace("/category/"+category_slug);                        
                    }
                    
                }
            })
        }else{
            $(this).parent().find('.submenuList').show();
            $(this).css('color', '#ff6f69');
        }
    })
    $(document).on('click', '.mobileCategoryMenu li a.showE',function(){
        $(this).parent().find('.submenuList').hide();
        $(this).css('color', 'black');
        $(this).removeClass('showE')
        $(this).addClass('hideE')
    })
    
    
    
    $(document).on('click', '.submenuList li a.hideEl',function(){
        var category_slug = $(this).data('category_slug');
        var category_id = $(this).data('category_id');
        $(this).removeClass('hideEl')
        $(this).addClass('showEl')
        var thisElement = $(this);
        if($(this).parent().find('.submenuSubList').html() == ''){
            $.ajax({
                type: 'POST',
                url: '../../../site/getchildcategory',
                data: {category_id:category_id},
                dataType:'json',
                success: function(response){
                    if(response.length != 0){
                        thisElement.parent().find('.submenuSubList').show();
                        thisElement.parent().find('.submenuSubList').append('<li class="list-group-item ogoloshena_pid_micro">'+
                                '<a href="../../../category/'+category_slug+'">Все объявления</a>'+
                            '</li>');

                        for(var key in response){
                            var category = response[key];
                                thisElement.parent().find('.submenuSubList').append('<li class="list-group-item ogoloshena_pid_micro" >'+
                                    '<a href="../../category/'+category.info['slug']+'">'+
                                        category.info['name']+
                                    '</a>'+
                                '</li>');                        
                        }
                    }else{
                        window.location.replace("/category/"+category_slug);                        
                    }

                }
            })
        }else{
            $(this).parent().find('.submenuSubList').show();
        }
    });
    $(document).on('click', '.submenuList li a.showEl',function(){
        $(this).parent().find('.submenuSubList').hide();
        $(this).removeClass('showEl')
        $(this).addClass('hideEl')
    })
    
    function getCategoryElement(thisElement){
        if(thisElement.find('.submenuFirst').length){
                var text = thisElement.find('.submenuFirst').html();
                var category_id = thisElement.data('category_id');
                var category_slug = thisElement.data('category_slug');
                thisElement.find('.submenuFirst').show();
                if(text == ''){
                    $.ajax({
                        type: 'POST',
                        url: '../../../site/getchildcategory',
                        data: {category_id:category_id},
                        dataType:'json',
                        success: function(response){
                            thisElement.find('.submenuFirst').css('border', '1px solid #c2c2c2');
                            thisElement.find('.submenuFirst').css('background-color', '#f8f8f8');
                            thisElement.find('.submenuFirst').append('<li data-category_id="'+category_id+'"><a href="../../../category/'+category_slug+'">Все объявления</a></li>');
                            console.log(response)
                            for(var categ_id in response){
                                var category = response[categ_id];
//                                if(category.child != 0){
//                                    thisElement.find('.submenuFirst').append('<li class="eHover" data-category_slug="'+category.info['slug']+'" data-category_id="'+category.info['id']+'">'+
//                                            '<span>'+category.info['name']+'</span>'+
//                                            '<ul class="submenuSecond"></ul>'+
//                                            '</li>');
//                                }else{ 
                                    thisElement.find('.submenuFirst').append('<li data-category_id="'+category.info['id']+'">'+
                                            '<a href="../../../category/'+category.info['slug']+'">'+category.info['name']+'</a>'+
                                            '</li>');
//                                }
                            }
                        }
                    });
                }else{
                    thisElement.find('.submenuFirst').show();
                    thisElement.css('color', '#ff6f69');

                }
            }else{
                console.log('--------');
            }
    }
        
    $(".rightBlockMenu li").hover(function() {
            var thisElement = $(this);            
            getCategoryElement(thisElement);            
        },function(){
            var classH = $(this).find('.submenuFirst');
            if(classH.length){
                setTimeout(function () {
                    if (classH.is(':hover')) {
                    } else {
                        classH.hide();
                    }
                }, 100);
            }
        }
    );
    
//    $(document).on('mouseenter',".submenuFirst li",function(){
//            var thisElement = $(this);
//            if(thisElement.find('.submenuSecond').length){
//                var text = thisElement.find('.submenuSecond').html();
//                var category_id = thisElement.data('category_id');
//                var category_slug = thisElement.data('category_slug');
//                if(text == ''){
//                    $.ajax({
//                        type: 'POST',
//                        url: '../../../site/getchildcategory',
//                        data: {category_id:category_id},
//                        dataType:'json',
//                        success: function(response){
//                            thisElement.find('.submenuSecond').show();
//                            thisElement.find('.submenuSecond').append('<li data-category_id="'+category_id+'"><a href="../../../category/'+category_slug+'">Все объявления</a></li>');
//                            console.log(response)
//                            for(var categ_id in response){
//                                var category = response[categ_id];
//                                    thisElement.find('.submenuSecond').append('<li data-category_id="'+category.info['id']+'">'+
//                                            '<a href="../../../category/'+category.info['slug']+'">'+category.info['name']+'</a>'+
//                                            '</li>');
//                            }
//                        }
//                    });
//                }else{
//                    thisElement.find('.submenuSecond').show();
//                }
//                
//            }
//    })
//    
//    $(document).on('mouseleave',".submenuFirst li",function(){
//            var classH = $(this).find('.submenuSecond');
//            if(classH.length){
//                setTimeout(function () {
//                    if (classH.is(':hover')) {
//                    } else {
//                        classH.hide();
//                    }
//                }, 200);
//            }
//    })

    $(document).on('keyup',".choiseCity",function(){
        if($(this).val().length > 1){
            var search_key = $(this).val()
            $.ajax({
                type: 'POST',
                url: '../../../site/getcity',
                data: {search_key:search_key},
                dataType:'json',
                success: function(response){
                    $('.cityPreview').html('')
                    $('[name=cityChoiseId]').val('0')
                    for(var key in response){
                        var city = response[key];
                        $('.cityPreview').append('<li data-city_id="'+city.city_id+'">'+city.name+'</li>')
                    }
                }
            })
        }else{
            $('[name=cityChoiseId]').val('0')
        }
    })
    
    $(document).on('click','.deleteChoiseCity',function(){
        $('.choiseCity').val('');
        $.cookie("myCityId", null, { path: ''});
        $.cookie("myCityId", null, { path: '/'});
        $.cookie("myCityId", null, { path: '/profile/'});
        $.cookie("myCityId", null, { path: '/category/'});
        
        $.cookie("myMetroId", null, { path: ''});
        $.cookie("myMetroId", null, { path: '/'});
        $.cookie("myMetroId", null, { path: '/profile/'});
        $.cookie("myMetroId", null, { path: '/category/'});
        
//        $.cookie('myCityId', null);
//        $.cookie('myMetroId', null);
        
        window.location.reload()
    });
    
    $(document).on('click',".cityPreview li",function(){
        var city_id = $(this).data('city_id');
        $('[name=cityChoiseId]').val(city_id);
        var city_name = $(this).text();
        $('.choiseCity').val(city_name);
        $('.metroBlock').html('');
        $('.metroBlock').append('<span class="style-city-choose">Выбрать станцию метро:</span>'+
                '<select name="choiseMetro" id="choiseMetro" style="width:100%;padding:10px;margin-top:10px;"  multiple="multiple">'+
                    '<option>Выберите станцию метро</option>'+
                '</select>');
        $('.form-area_mob .metroBlock').html('');
        $('.form-area_mob .metroBlock').append('<span class="style-city-choose">Выбрать станцию метро:</span>'+
                '<select name="choiseMetroMobile" id="choiseMetroMobile" style="width:100%;padding:10px;margin-top:10px;"  multiple="multiple">'+
                    '<option>Выберите станцию метро</option>'+
                '</select>');
                                
        $.ajax({
            type: 'POST',
            url: '../../../site/getcityname',
            data: {city_id:city_id},
            dataType:'json',
            success: function(response){
                if(response.metro.length != 0){
                    $('.metroBlock').show();
                    $('[name=choiseMetro]').html(' ');
                    var metros = response.metro;
                    
                    var arr = $.map(metros, function(el) { return el });
                    var metrosCount = arr.length;
                    var k = 0;
                    for(var metro_id in metros){
                                var k = k + 1;
                                var metro_name = metros[metro_id];
                                var cookie_metro_id = 0;
                                if($.cookie('myMetroId')){
                                    if($.cookie('myMetroId') != 'null'){
                                        var cookie_metro_id = $.cookie('myMetroId');
                                    }
                                }
                                if(cookie_metro_id == metro_id){
                                    $('[name=choiseMetro]').append('<option selected value="'+metro_id+'">'+metro_name+'</option>');
                                    $('[name=choiseMetroMobile]').append('<option selected value="'+metro_id+'">'+metro_name+'</option>');
                                }else{
                                    $('[name=choiseMetro]').append('<option value="'+metro_id+'">'+metro_name+'</option>');
                                    $('[name=choiseMetroMobile]').append('<option value="'+metro_id+'">'+metro_name+'</option>');
                                }

                                if(metrosCount == k){
                                    initializationMultiple();
                                    initializationMultipleMobile();
                                }
                    }
                }else{
                    $('.metroBlock').hide();                            
                }
            }
        });
        
        $('.cityPreview').html('')
    })
    
    if($.cookie('myCityId')){
        if($.cookie('myCityId') != 'null'){
            var city_id = $.cookie('myCityId');
            $('[name=cityChoiseId]').val(city_id);
            $.ajax({
                type: 'POST',
                url: '../../../site/getcityname',
                data: {city_id:city_id},
                dataType:'json',
                success: function(response){
                    if(response.status == 'success'){
                        if(response['city_name'].length > 6){
                            $('.linkCityHeader').text(response['city_name'].slice(0,6)+'...')
                        }else{
                            $('.linkCityHeader').text(response['city_name'])
                        }
                        $('.choiseCity').val(response['city_name']);
                        
                        if(response.metro.length != 0){
                            $('.metroBlock').show();
                            $('[name=choiseMetro]').html(' ');
                            $('[name=choiseMetroMobile]').html(' ');
                            var metros = response.metro;
                            
                            var arr = $.map(metros, function(el) { return el });
                            var metrosCount = arr.length;
                            var k = 0;
                            for(var metro_id in metros){
                                 k = k + 1;
                                var metro_name = metros[metro_id];
                                var cookie_metro_id = 0;
                                var metroArrays = [];
                                if($.cookie('myMetroId')){
                                    if($.cookie('myMetroId') != 'null'){
                                        metroArrays = jQuery.parseJSON($.cookie('myMetroId'));
                                    }
                                }
                                
                                if(jQuery.inArray( metro_id, metroArrays ) >= 0){
                                    $('[name=choiseMetro]').append('<option selected value="'+metro_id+'">'+metro_name+'</option>');
                                    $('[name=choiseMetroMobile]').append('<option selected value="'+metro_id+'">'+metro_name+'</option>');
                                }else{
                                    $('[name=choiseMetro]').append('<option value="'+metro_id+'">'+metro_name+'</option>');                                    
                                    $('[name=choiseMetroMobile]').append('<option value="'+metro_id+'">'+metro_name+'</option>');                                    
                                }
                                
                                if(metrosCount == k){
                                    initializationMultiple();
                                    initializationMultipleMobile();
                                }
                            }
                        }else{
                            $('.metroBlock').hide();                            
                        }
                    }else{
                        $('[name=cityChoiseId]').val('0');
                    }
                }
            });
        }else{
//            console.log('нема');
        }
    }else{
//        console.log('нема');
    }
    
    
    $(document).on('click',".saveCity",function(){
        var buttonType = $(this).data('button_type');
        
        if($('[name=cityChoiseId]').val() == 0){
            $('.linkCityHeader').text('Город')
            if($.cookie('myCityId')){
                $.cookie("myCityId", null, { path: ''});
                $.cookie("myCityId", null, { path: '/'});
                $.cookie("myCityId", null, { path: '/profile/'});
                $.cookie("myCityId", null, { path: '/category/'});
            }
        }else{
            $.cookie("myCityId", $('[name=cityChoiseId]').val(), { path: ''});
            $.cookie("myCityId", $('[name=cityChoiseId]').val(), { path: '/'});
            $.cookie("myCityId", $('[name=cityChoiseId]').val(), { path: '/profile/'});
            $.cookie("myCityId", $('[name=cityChoiseId]').val(), { path: '/category/'});
//            $.cookie('myCityId', $('[name=cityChoiseId]').val());
            
            if($('.choiseCity').val().length < 7){
                $('.linkCityHeader').text($('.choiseCity').val())
            }else{
                $('.linkCityHeader').text($('.choiseCity').val().slice(0,6)+'...')
            }
        }
        
        
        
        if(buttonType == 'mobile'){
                if(($('[name=choiseMetroMobile]').val() == '') || ($('[name=choiseMetroMobile]').val() == '[]')){
                        $.cookie('myMetroId', null);
                        $.cookie("myMetroId", null, { path: ''});
                        $.cookie("myMetroId", null, { path: '/'});
                        $.cookie("myMetroId", null, { path: '/profile/'});
                        $.cookie("myMetroId", null, { path: '/category/'});
                }else{
                        var metrosChoiseJson = JSON.stringify($('[name=choiseMetroMobile]').val());
                        $.cookie('myMetroId', metrosChoiseJson);
                        $.cookie("myMetroId", metrosChoiseJson, { path: ''});
                        $.cookie("myMetroId", metrosChoiseJson, { path: '/'});
                        $.cookie("myMetroId", metrosChoiseJson, { path: '/profile/'});
                        $.cookie("myMetroId", metrosChoiseJson, { path: '/category/'});
                }
        }else{
                if(($('[name=choiseMetro]').val() == '') || ($('[name=choiseMetro]').val() == '[]')){
                        $.cookie('myMetroId', null);
                        $.cookie("myMetroId", null, { path: ''});
                        $.cookie("myMetroId", null, { path: '/'});
                        $.cookie("myMetroId", null, { path: '/profile/'});
                        $.cookie("myMetroId", null, { path: '/category/'});
                }else{
                        var metrosChoiseJson = JSON.stringify($('[name=choiseMetro]').val());
                        $.cookie('myMetroId', metrosChoiseJson);
                        $.cookie("myMetroId", metrosChoiseJson, { path: ''});
                        $.cookie("myMetroId", metrosChoiseJson, { path: '/'});
                        $.cookie("myMetroId", metrosChoiseJson, { path: '/profile/'});
                        $.cookie("myMetroId", metrosChoiseJson, { path: '/category/'});
                }
            
        }
        
        window.location.reload()
    })
    
    $(document).on('click',".letterBlock .letter",function(){
        $('.letterBlock .letter').removeClass('active')
        $(this).addClass('active')
        var letter = $(this).text();
        $('.letterChoisen').text($(this).text())
            $.ajax({
                type: 'POST',
                url: '../../../site/getcitylettername',
                data: {letter:letter},
                dataType:'json',
                success: function(response){
                    $('.column').html('');
                    if(response.status == 'success'){
                        for(var key in response.cityArray){
                            var city = response.cityArray[key];
                            $('.column').append('<div class="city" data-city_id="'+city['city_id']+'">'+city['name']+'</div><br>');                            
                        }
                    }else{
                        $('.column').html('пусто');
                    }
                }
            });       
    })
    
    $(document).on('click',".column div.city",function(){
        var city_id = $(this).data('city_id');
        var city_name = $(this).text();
        if(city_id){
            $.cookie("myCityId", city_id, { path: ''});
            $.cookie("myCityId", city_id, { path: '/'});
            $.cookie("myCityId", city_id, { path: '/profile/'});
            $.cookie("myCityId", city_id, { path: '/category/'});
            
//            $.cookie('myCityId', city_id);
            $('[name=cityChoiseId]').val(city_id)
            $('.choiseCity').val(city_name)
            if(city_name.length < 7){
                $('.linkCityHeader').text(city_name)
            }else{
                $('.linkCityHeader').text(city_name.slice(0,6)+'...')
            }
            $("html, body").animate({ scrollTop: 0 }, 600);
            window.location.reload();
        }
    })
    
    $(document).on('click',".footerCity .cityF",function(){
        var city_id = $(this).data('city_id');
        var city_name = $(this).text();
        if(city_id){
            $.cookie("myCityId", city_id, { path: ''});
            $.cookie("myCityId", city_id, { path: '/'});
            $.cookie("myCityId", city_id, { path: '/profile/'});
            $.cookie("myCityId", city_id, { path: '/category/'});
            
//            $.cookie('myCityId', city_id);
            $('[name=cityChoiseId]').val(city_id)
            $('.choiseCity').val(city_name)
            if(city_name.length < 7){
                $('.linkCityHeader').text(city_name)
            }else{
                $('.linkCityHeader').text(city_name.slice(0,6)+'...')
            }
            $("html, body").animate({ scrollTop: 0 }, 600);
            window.location.reload();
        }
    })
    
    $(document).on('click',".headerSearchButton",function(e){
        e.preventDefault();
        var inputValue = $('.headerSearchText').val();
        var category_slug = $('.headerCategoryChoise').val();
        if(inputValue.length >= 2){
                $.cookie('headerSearchText',inputValue)
                $.cookie('headerCategoryChoise',category_slug)
            if(category_slug != ''){
                window.location.replace("/category/"+category_slug);
            }else{
                window.location.replace("/");
            }            
        }
    })
    
    $(document).on('click',".deleteSearchText",function(){
        $.cookie('headerSearchText',null)
        $.cookie('headerCategoryChoise',null)
        $('.headerCategoryUl li').removeClass('selected')
        $('.btn-select-value').text('Все категории');
        $('.headerCategoryChoise').val('');
        $('.headerSearchText').val('');
        window.location.reload()
    })
    
    $(window).click(function() {
        $('.signinBlock').hide();
        $('.reklamaBlock').hide();
    });
    
    $(document).on('click',".signinBlock, .registrationshow, .btn-signup, .btn-signin_mob, .signup_show",function(event){
        $('.signinBlock').show();
        $('.account-link').removeClass('active');
        $('.form-account').hide();
        event.stopPropagation();
    });
    
    $(document).on('click',".showReklama, .reklamaBlock",function(event){
        event.stopPropagation();        
    });
    
    $(document).on('click',".please_register",function(event){
        $('.account-link').addClass('active');
        $('.form-account').show();
        $("html, body").animate({ scrollTop: 0 }, 600);
        event.stopPropagation();
    });
    
    $(document).on('click',".signin",function(event){
        $('.signinBlock').show();
        $('.login_mob_input_container').hide();
    });
    
    $(document).on('submit',"#registrationForm",function(event){
        event.preventDefault();
        var contactname = $(this).find('[name=registrationContactname]').val();
        var email = $(this).find('[name=registrationEmail]').val();
        
        $.ajax({
            type: 'POST',
            url: '../../../site/registrationuser',
            data: {email:email, contactname:contactname},
            dataType:'json',
            beforeSend: function( xhr ) {
                $('.loader-email').show();
                $('.hidd-loader').show();
            },
            success: function(response){
                $('.loader-email').hide();
                $('.hidd-loader').hide();
                if(response.status == 'success'){
                    swal("Регистрация прошла успешно!", "на указанный вами адрес электронной почты отправлено данные для входа", "success");
                }else if(response.status == 'error_email'){
                    swal("Ошибка!", "адрес электронной почты уже занят", "error");
                }else{
                    swal("Ошибка!", "при регистрации возникла ошибка, попробуйте снова или обратитесь к администрации сайта", "error");
                }
            }
        });
    });
    
    
    $(document).on('submit',"#reklamaForm",function(event){
        event.preventDefault();
        var email = $(this).find('[name=reklamaEmail]').val();
        var telefon = $(this).find('[name=reklamaTelefon]').val();
        var comment = $(this).find('[name=reklamaComment]').val();
        
        $.ajax({
            type: 'POST',
            url: '../../../site/reklama',
            data: {email:email, telefon:telefon, comment:comment},
            dataType:'json',
            beforeSend: function( xhr ) {
                $('.loader-rek').show();
                $('.hidd-rek').show();
            },
            success: function(response){
                $('.loader-rek').hide();
                $('.hidd-rek').hide();
                if(response.status == 'success'){
                    swal("Отправлено!", "Мы свяжемся с вами в ближайшие время", "success");
                }
            }
        });
    });
    
    $(document).on('click',".account_mob-link",function(event){
        $('.login_mob_input_container').toggle();
        $('.logout_mob_input_container').toggle();
    });


    $('.menu_img').on('click', function () {
        if($('.hidei').length){
            $('.menu_img').addClass('showw');
            $('.menu_img').removeClass('hidei');
            $('.mobile_menublok').show();
            $('.menu_img').find('.img_mob_menue img').attr('src', '/web/img/mobile-menu-btn.png')
            $('.menu_img img').attr('src', '/web/img/mobile_mob_click.png')

        }else{
            $('.menu_img').find('.img_mob_menue img').attr('src', '/web/img/mobile_mob_click.png')
            $('.menu_img img').attr('src', '/web/img/mobile-menu-btn.png')
            $(this).removeClass('showw');
            $(this).addClass('hidei');
            $('.mobile_menublok').hide();
        }
    })

    $('.img_menu_mob').on('click', function () {
        $('.menu_img').removeClass('showw');
        $('.menu_img').addClass('hidei');
        $('.mobile_menublok').hide();
        $('.menu_img img').attr('src', '/web/img/mobile-menu-btn.png')
    })

    $(document).on('click',".signinBlock, .img_redit",function(event){
        $('.signinBlock').hide();
        $('.account-link').addClass('active');
        $('.form-account').hide();
    })
    $(document).on('click', '.changeProfileData',function(){
        $('#modalUserUpdate').modal('show')
    })

    $('.phone_java').on('click', function () {
        $('.pid').hide();
        $('.mobile_show').show();

    })

    $(document).on('click',".hide_product_filter",function () {
        $('.FilterBlock').hide();
        $('.show_product_filter').show();
        $('.hide_product_filter').hide();
    })
    $(document).on('click',".show_product_filter",function () {
        $('.FilterBlock').show();
        $('.show_product_filter').hide();
        $('.hide_product_filter').show();
    })
    
    $( ".SendHelpToEmail" ).submit(function( event ){
        event.preventDefault();
        var email = $('[name=help_email]').val();
        var text = $('[name=help_question]').val();

        $.ajax({
            type: 'POST',
            url: '../../../site/helpsend',
            data: {email:email, text:text},
            dataType:'json',
            beforeSend: function( xhr ) {
                $('.loader-help').show();
                $('.hidd-help').show();
            },
            success: function(response){
                $('.loader-help').hide();
                $('.hidd-help').hide();
                if(response.status){
                    swal("Отправлено", "Мы свяжемся с вами в ближайшие время!", "success")
                }
            }
        });        
    });
    
    $(document).on('click', '.showReklama',function(){
        $('.reklamaBlock').show();
        $("html, body").animate({ scrollTop: 0 }, 600);
    })
    $(document).on('click','.img_redit',function () {
        $('.reklamaBlock').hide();
    })




    function number_format(number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function(n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                        .toFixed(prec);
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '')
                .length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1)
                .join('0');
        }
        return s.join(dec);
    }

    $(".phone_with_ddd").mask("(999) 999-9999");
    

});




