var socket = io.connect('http://35.165.139.87:2877');
//var socket = io.connect('https://192.168.0.108:2877');
socket.on('connect', function () {
    socket.emit('login', $("#user_id").val());
    socket.on('give_message', function (data) {
        socket.emit('allUnread', $("#user_id").val());
    })
    socket.on('unreadCount', function (unreadCount) {
        if(unreadCount.msg_count){
            if(unreadCount.msg_count <= 99 ){
                $("div.unreadMsgCount > .countM").text(unreadCount.msg_count);
            }else{
                $("div.unreadMsgCount > .countM").text("99+");
            }
        }
        if (unreadCount.msg_count) {
            document.getElementById("newMSG").play();
        }
    });
    socket.emit('checkStatus', $("#userId").val());
    socket.on('reciveStatus', function (status) {
        if (status != -1) {
            $(".checkStatus").removeClass('offline');
            $(".checkStatus").addClass('online');
        } else {
            $(".checkStatus").removeClass('online');
            $(".checkStatus").addClass('offline');
        }
    })
    
});